﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 <armaturePair>j__TPar <>f__AnonymousType0`2::get_armaturePair()
// 0x00000002 <s>j__TPar <>f__AnonymousType0`2::get_s()
// 0x00000003 System.Void <>f__AnonymousType0`2::.ctor(<armaturePair>j__TPar,<s>j__TPar)
// 0x00000004 System.Boolean <>f__AnonymousType0`2::Equals(System.Object)
// 0x00000005 System.Int32 <>f__AnonymousType0`2::GetHashCode()
// 0x00000006 System.String <>f__AnonymousType0`2::ToString()
// 0x00000007 System.Single ARFeatheredPlaneMeshVisualizer::get_featheringWidth()
extern void ARFeatheredPlaneMeshVisualizer_get_featheringWidth_mD474743D2E8E4CE3078FCD70A52B70111EC99500 (void);
// 0x00000008 System.Void ARFeatheredPlaneMeshVisualizer::set_featheringWidth(System.Single)
extern void ARFeatheredPlaneMeshVisualizer_set_featheringWidth_m1974473A6C2BB2AED083691C8079E6F9A83398EC (void);
// 0x00000009 System.Void ARFeatheredPlaneMeshVisualizer::Awake()
extern void ARFeatheredPlaneMeshVisualizer_Awake_mBFDBB9E8B393BD7BA784845A0C7692848E9C61E2 (void);
// 0x0000000A System.Void ARFeatheredPlaneMeshVisualizer::OnEnable()
extern void ARFeatheredPlaneMeshVisualizer_OnEnable_m3A77938D2FE2C6A653A7E13B90DB6C9DEEB06A9A (void);
// 0x0000000B System.Void ARFeatheredPlaneMeshVisualizer::OnDisable()
extern void ARFeatheredPlaneMeshVisualizer_OnDisable_m9DE1EB0F00D7F7AC754CE4633569BB8C8687C67A (void);
// 0x0000000C System.Void ARFeatheredPlaneMeshVisualizer::ARPlane_boundaryUpdated(UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs)
extern void ARFeatheredPlaneMeshVisualizer_ARPlane_boundaryUpdated_mC88C42F35A3B243C83E9D24DCC9545F7E9E6BA28 (void);
// 0x0000000D System.Void ARFeatheredPlaneMeshVisualizer::GenerateBoundaryUVs(UnityEngine.Mesh)
extern void ARFeatheredPlaneMeshVisualizer_GenerateBoundaryUVs_mCEA3823DDAED557BA0B92214D15AB25EC75065D8 (void);
// 0x0000000E System.Void ARFeatheredPlaneMeshVisualizer::.ctor()
extern void ARFeatheredPlaneMeshVisualizer__ctor_mA9417DD97EC611603602EAE0302A80E71CF93890 (void);
// 0x0000000F System.Void ARFeatheredPlaneMeshVisualizer::.cctor()
extern void ARFeatheredPlaneMeshVisualizer__cctor_mC697317BB4FB496A10AFA8C2E73E27AF8A5538A1 (void);
// 0x00000010 UnityEngine.GameObject ReferencePointCreator::get_referencePointPrefab()
extern void ReferencePointCreator_get_referencePointPrefab_m31FB3255AF4A0E9C6E7A9B49ADF62894D1172F36 (void);
// 0x00000011 System.Void ReferencePointCreator::set_referencePointPrefab(UnityEngine.GameObject)
extern void ReferencePointCreator_set_referencePointPrefab_m55AB059B84BF9130A63CEA5B25432DF5575E556D (void);
// 0x00000012 System.Void ReferencePointCreator::RemoveAllReferencePoints()
extern void ReferencePointCreator_RemoveAllReferencePoints_mEF4D5C72B358090E4652FA705548C08851E2CD9B (void);
// 0x00000013 System.Void ReferencePointCreator::Awake()
extern void ReferencePointCreator_Awake_mBA33AA2F7ADB2A8DA9B3D2BC744C57CE87BEA02F (void);
// 0x00000014 System.Void ReferencePointCreator::Update()
extern void ReferencePointCreator_Update_mAB2F5A406EAE54CF3D8378109674BA0DBE1CC81B (void);
// 0x00000015 System.Void ReferencePointCreator::.ctor()
extern void ReferencePointCreator__ctor_mD3983A54F093C032DB88E8BC396BBD0A060C210F (void);
// 0x00000016 System.Void ReferencePointCreator::.cctor()
extern void ReferencePointCreator__cctor_mB6B005CB03A919E9A5C764505CCEA0BB17F82067 (void);
// 0x00000017 System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_m168B1E78BFA288F42D4AE0A8F1424B8D68B07993 (void);
// 0x00000018 System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_m49C4A6501BCC216F924B3C37F243D1B5B54A69FF (void);
// 0x00000019 System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m5E6DF0E37CB2E9FBBEACCB6EEE6452AB14BBE94C (void);
// 0x0000001A System.Void ChatController::.ctor()
extern void ChatController__ctor_m2C7AAB67386BA2DC6742585988B914B3FAB30013 (void);
// 0x0000001B System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_mEAC0F8D33D13DE84DCEA5D99E5162E4D28F6B54D (void);
// 0x0000001C System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m901DB807D4DFA75581306389B7A21072E98E72A0 (void);
// 0x0000001D System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_mDDD10A405C7152BEFA0ECEA0DCBD061B47C5802E (void);
// 0x0000001E System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_m630E0BFAB4D647BC38B99A70F522EF80D25F3C71 (void);
// 0x0000001F System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_m2A8770DA2E27EC52F6A6F704831B732638C76E84 (void);
// 0x00000020 System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_mEC7653F2228D8AA66F69D6B3539ED342AEE57691 (void);
// 0x00000021 System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m4E1C1BEB96F76F2EE55E6FEC45D05F2AAC5DF325 (void);
// 0x00000022 System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mBE0169BE01459AA37111A289EC422DDB0D5E3479 (void);
// 0x00000023 System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_mBF81DE006E19E49DAC3AFF685F8AF268A2FD0FFB (void);
// 0x00000024 TMPro.TMP_TextEventHandler_CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_mF70DBE3FF43B3D6E64053D37A2FADF802533E1FF (void);
// 0x00000025 System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler_CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_mDEC285B6A284CC2EC9729E3DC16E81A182890D21 (void);
// 0x00000026 TMPro.TMP_TextEventHandler_SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m395603314F8CD073897DCAB5513270C6ADD94BF4 (void);
// 0x00000027 System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler_SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_m3D4E17778B0E3CC987A3EF74515E83CE39E3C094 (void);
// 0x00000028 TMPro.TMP_TextEventHandler_WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_m415F4479934B1739658356B47DF4C2E90496AE2E (void);
// 0x00000029 System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler_WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_m2EDD56E0024792DCE7F068228B4CA5A897808F4E (void);
// 0x0000002A TMPro.TMP_TextEventHandler_LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_m8E724700CC5DF1197B103F87156576A52F62AB2B (void);
// 0x0000002B System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler_LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m067512B3F057A225AF6DD251DD7E546FFF64CD93 (void);
// 0x0000002C TMPro.TMP_TextEventHandler_LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_m221527467F0606DD3561E0FB0D7678AA8329AD5D (void);
// 0x0000002D System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler_LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_mE3CE372F9FECD727FAB3B14D46439E0534EE8AA8 (void);
// 0x0000002E System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_m67A37475531AC3EB75B43A640058AD52A605B8D9 (void);
// 0x0000002F System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mB0ABBED08D5494DFFF85D9B56D4446D96DDBDDF5 (void);
// 0x00000030 System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_mE1CAF8C68C2356069FEB1AA1B53A56E24E5CE333 (void);
// 0x00000031 System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mB429546A32DCF6C8C64E703D07F9F1CDC697B009 (void);
// 0x00000032 System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_mFBFC60A83107F26AA351246C10AB42CEB3A5A13C (void);
// 0x00000033 System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_mAB964EB5171AB07C48AC64E06C6BEC6A9C323E09 (void);
// 0x00000034 System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_m3B76D7E79C65DB9D8E09EE834252C6E33C86D3AE (void);
// 0x00000035 System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_m9E9CAD5FA36FCA342A38EBD43E609A469E49F15F (void);
// 0x00000036 System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m1C55C664BB488E25AE746B99438EEDAE5B2B8DE8 (void);
// 0x00000037 System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_m189A5951F5C0FA5FB1D0CFC461FAA1EBD7AED1AE (void);
// 0x00000038 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_m20668FA5AD3945F18B5045459057C330E0B4D1F4 (void);
// 0x00000039 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_m40EDCD3A3B6E8651A39C2220669A7689902C8B36 (void);
// 0x0000003A System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_mE8F5BC98EC6C16ECEBAD0FD78CD63E278B2DF215 (void);
// 0x0000003B System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_m7F24B3D019827130B3D5F2D3E8C3FF23425F98BE (void);
// 0x0000003C System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_m3F848191079D3EF1E3B785830D74698325CA0BB7 (void);
// 0x0000003D System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_m3323414B806F63563E680918CC90EAF766A3D1AE (void);
// 0x0000003E System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_m261B7F2CD25DC9E7144B2A2D167219A751AD9322 (void);
// 0x0000003F System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_m649EFCC5BF0F199D102083583854DE87AC5EFBDD (void);
// 0x00000040 System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m90649FDE30CC915363C5B61AA19A7DE874FF18ED (void);
// 0x00000041 System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mFDF88CB6DD4C5641A418DB08E105F9F62B897777 (void);
// 0x00000042 System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_mB07A2FD29BE4AFE284B47F2F610BDB7539F5A5DE (void);
// 0x00000043 System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m5E24687E6D82C0EBC4984D01B90769B8FD8C38B3 (void);
// 0x00000044 System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m257B81C6062A725785739AFE4C0DF84B8931EFB2 (void);
// 0x00000045 System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m9660F57BCF4F8C2154D19B6B40208466E414DAEB (void);
// 0x00000046 System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m0B63EA708A63AF6852E099FD40F7C4E18793560A (void);
// 0x00000047 System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_m8379776EEE21556D56845974B8C505AAD366B656 (void);
// 0x00000048 System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_m2E5B2D7FA6FE2F3B5516BD829EDC5522187E6359 (void);
// 0x00000049 System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_mF8175B9157B852D3EC1BAF19D168858A8782BF0D (void);
// 0x0000004A System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_m1F951082C07A983F89779737E5A6071DD7BA67EB (void);
// 0x0000004B System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_m44ACA60771EECABCB189FC78027D4ECD9726D31A (void);
// 0x0000004C System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_m57178B42FF0BB90ACA497EC1AA942CC3D4D54C32 (void);
// 0x0000004D System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_mB34C25C714FAEA4792465A981BAE46778C4F2409 (void);
// 0x0000004E System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_mDFAE260FD15CD3E704E86A25A57880A33B817BC6 (void);
// 0x0000004F System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_m0238BE0F5DF0A15743D4D4B1B64C0A86505D1B76 (void);
// 0x00000050 System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mB92D578CAC3E0A0AFB055C7FEF47601C8822A0F8 (void);
// 0x00000051 System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_m0E919E8F3C12BAFF36B17E5692FCFA5AE602B2AA (void);
// 0x00000052 System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_mC70E117C1F921453D2F448CABA234FAA17A277ED (void);
// 0x00000053 System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_mE2308836BF90B959ABE6064CD2DDDFAF224F0F4A (void);
// 0x00000054 UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_m3CE7B666BEF4CFFE9EB110C8D57D9A5F6385720B (void);
// 0x00000055 System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m8B756AF1E1C065EEA486159E6C631A585B0C3461 (void);
// 0x00000056 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m44F3CBD12A19C44A000D705FB4AB02E20432EC02 (void);
// 0x00000057 System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_mE2AAB8DF142D7BDB2C041CC7552A48745DBFDCFF (void);
// 0x00000058 System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m1593A7650860FD2A478E10EA12A2601E918DD1EC (void);
// 0x00000059 System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m313B4F7ED747AD6979D8909858D0EF182C79BBC3 (void);
// 0x0000005A System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m2540DCD733523BCBB1757724D8546AC3F1BEB16C (void);
// 0x0000005B System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_mEF10D80C419582C6944313FD100E2FD1C5AD1319 (void);
// 0x0000005C System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_mF4798814F4F86850BB9248CA192EF5B65FA3A92B (void);
// 0x0000005D System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter_FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_m19C3C5E637FB3ED2B0869E7650A1C30A3302AF53 (void);
// 0x0000005E System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_m55E3726473BA4825AC0B7B7B7EA48D0C5CE8D646 (void);
// 0x0000005F System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mAFF9E7581B7B0C93A4A7D811C978FFCEC87B3784 (void);
// 0x00000060 System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m270DBB9CC93731104E851797D6BF55EACAE9158A (void);
// 0x00000061 System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_m4394BE3A0CA37D319AA10BE200A26CFD17EEAA8F (void);
// 0x00000062 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mCBF0B6754C607CA140C405FF5B681154AC861992 (void);
// 0x00000063 System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m4C290E23BBA708FE259A5F53921B7B98480E5B08 (void);
// 0x00000064 System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mF68BE3244AFD53E84E037B39443B5B3B50336FF5 (void);
// 0x00000065 System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m23569DD32B2D3C4599B8D855AE89178C92BA25C7 (void);
// 0x00000066 System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_m4B49D7387750432FA7A15A804ABD6793422E0632 (void);
// 0x00000067 System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_m2A2D1B42F97BD424B7C61813B83FE46C91575EFB (void);
// 0x00000068 System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_mEE6FCD85F7A6FDA4CC3B51173865E53F010AB0FF (void);
// 0x00000069 System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_mF02F95A5D14806665404997F9ABAEE288A9879A0 (void);
// 0x0000006A System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_m6D15B2FC399C52D9706DD85C796BAE40CA8362D3 (void);
// 0x0000006B System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_m080D05700B1D3251085331369FCD2A131D45F963 (void);
// 0x0000006C System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_m6AB8BC86973365C192CF9EACA61459F2E0A5C88D (void);
// 0x0000006D System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m87D2FCFCEDEE1FA82DEF77A867D2DE56C3AA0973 (void);
// 0x0000006E System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_mD1C87684FD94190654176B38EE7DC960795F08E8 (void);
// 0x0000006F System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_m429F83E18507E278CA9E9B5A2AE891087ED0D830 (void);
// 0x00000070 System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m91D0E180681C5566066C366487B94A05FB376B12 (void);
// 0x00000071 System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_m80F8343FAB19617468E94CD2B35636DBB9AC2064 (void);
// 0x00000072 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_m9A938ED5B0D70633B9099F5C1B213FD50380116D (void);
// 0x00000073 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_mD481099225DF156CA7CA904AA1C81AF26A974D28 (void);
// 0x00000074 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_mE4A6507E55DD05BBC99F81212CF26F2F11179FBE (void);
// 0x00000075 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_m5E52652A02A561F2E8AB7F0C00E280C76A090F74 (void);
// 0x00000076 System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m01B9A1E989D57BE8837E99C4359BCB6DD847CB35 (void);
// 0x00000077 System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mC42D87810C72234A3360C0965CC1B7F45AB4EE26 (void);
// 0x00000078 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_mFAF9F495C66394DC36E9C6BC96C9E880C4A3B0A9 (void);
// 0x00000079 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_mC4A3331333B1DFA82B184A0701FCE26395B8D301 (void);
// 0x0000007A System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_mCA98BB5342C50F9CE247A858E1942410537E0DAF (void);
// 0x0000007B System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter_FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_mDD0EAB08CE58340555A6654BDD5BEE015E6C6ACE (void);
// 0x0000007C System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_mE3DC8B24D2819C55B66AEAEB9C9B93AFDA9C4573 (void);
// 0x0000007D System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m951573D9BF0200A4C4605E043E92BBD2EB33BA7C (void);
// 0x0000007E System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay_FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m39D0BB71DCCB67271B96F8A9082D7638E4E1A694 (void);
// 0x0000007F System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m103EF0B8818B248077CB97909BA806477DCEB8A5 (void);
// 0x00000080 System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m3501F8FA1B762D22972B9B2BAC1E20561088882B (void);
// 0x00000081 System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m2A3F19E0F9F2C72D48DDF5A4208AF18AE7769E69 (void);
// 0x00000082 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m8B985E4023A01F963A74E0FE5E8758B979FB3C3A (void);
// 0x00000083 System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_m8B1E7254BFB2D0C7D5A803AEFAFCD1B5327F79AD (void);
// 0x00000084 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m85E6334AFE22350A5715F9E45843FD865EF60C9D (void);
// 0x00000085 System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_mB6F523D582FE4789A5B95C086AA7C168A5DD5AF7 (void);
// 0x00000086 System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m1EF25B5345586DD26BB8615624358EFB21B485DB (void);
// 0x00000087 System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_mD4A85AE6FE4CD3AFF790859DEFB7E4AAF9304AE5 (void);
// 0x00000088 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_m7BF445A3B7B6A259450593775D10DE0D4BD901AD (void);
// 0x00000089 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_mD7D62A1D326528506154148148166B9196A9B903 (void);
// 0x0000008A System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_mA40DB76E1D63318E646CF2AE921084D0FDF4C3CA (void);
// 0x0000008B System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_mB40A823A322B9EFD776230600A131BAE996580C3 (void);
// 0x0000008C System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_mBFC04A0247294E62BD58CB3AC83F85AE61C3FB4F (void);
// 0x0000008D System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_mB0DEABA5CC4A6B556D76ED30A3CF08E7F0B42AFC (void);
// 0x0000008E System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_m8AB7E0B8313124F67FEDE857012B9E56397147E2 (void);
// 0x0000008F System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_m610430DD6E4FD84EBF6C499FB4415B5000109627 (void);
// 0x00000090 System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_m31920E8DD53AD295AAD8B259391A28E1A57862ED (void);
// 0x00000091 System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m189316ED7CD62EFD10B40A23E4072C2CEB5A516B (void);
// 0x00000092 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m3995DDE2D7E7CBF8087A3B61242F35E09AC94668 (void);
// 0x00000093 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m19D37F0DDC4E1D64EA67101852383862DCAAED1E (void);
// 0x00000094 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_m2CFBFF7F45A76D16C29B570E3468AFEEC2D1C443 (void);
// 0x00000095 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_mDB7F380B912148C792F857E42BFB042C6A267260 (void);
// 0x00000096 System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_mBAF42937750A7A22DB5BF09823489FDE25375816 (void);
// 0x00000097 System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m32ACAC43EDE2595CD4FFB6802D58DEBC0F65B52C (void);
// 0x00000098 System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_m63CC97434F60690EE234794C9C2AD3B25EC69486 (void);
// 0x00000099 System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mE5E221B893D3E53F3A9516082E2C4A9BE174DDF5 (void);
// 0x0000009A System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_mC977D71742279824F9DD719DD1F5CB10269BC531 (void);
// 0x0000009B System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_mE5AE5146D67DA15512283617C41F194AEDD6A4AC (void);
// 0x0000009C System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_m6B361C3B93A2CC219B98AACFC59288432EE6AC1E (void);
// 0x0000009D System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_mD5B5049BB3640662DD69EB1E14789891E8B2E720 (void);
// 0x0000009E System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m6075DA429A021C8CB3F6BE9A8B9C64127288CD19 (void);
// 0x0000009F System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m39AA373478F796E7C66763AA163D35811721F5CD (void);
// 0x000000A0 System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_mA087E96D94CB8213D28D9A601BC25ED784BB8421 (void);
// 0x000000A1 System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_mDED2AEA47D2E2EF346DE85112420F6E95D9A3CFD (void);
// 0x000000A2 System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_m0B59A798E6B193FE68F6A20E7004B223D5A2993E (void);
// 0x000000A3 System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_m238AB73BE06E33312281577CC896CEB7BB175245 (void);
// 0x000000A4 System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m41CBBA607D90D45E21C98CCDF347AE27FB50392F (void);
// 0x000000A5 System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_m7CBA45BF5135680A823536A18325ECA621EF7A1A (void);
// 0x000000A6 System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m39EBB983A4FFFF6DD1C7923C8C23FF09CFF2F6E2 (void);
// 0x000000A7 System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m8E3858FC1C976F311628466C411675E352F134A5 (void);
// 0x000000A8 System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m666FA35D389B109F01A5FC229D32664D880ADE09 (void);
// 0x000000A9 System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_mF4858E4385EAA74F5A3008C50B8AD180FCBC8517 (void);
// 0x000000AA System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_m076A6C9D71EE8B5A54CD1CEDCA5AB15160112DD3 (void);
// 0x000000AB System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m5CAAD9DFA7B4D9C561473D53CA9E3D8B78AE5606 (void);
// 0x000000AC System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_mB18FF89A84E2AA75BDD486A698955A58E47686EE (void);
// 0x000000AD System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_mCD27B81253963B3D0CD2F6BA7B161F0DFDC08114 (void);
// 0x000000AE System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_mB32AD5B7DFF20E682BA4FC82B30C87707DD3BA10 (void);
// 0x000000AF System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_m6C64C692D81F64FB7F3244C3F0E37799B159A0DE (void);
// 0x000000B0 System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mC08504F9622CC709271C09EDB7A0DF1A35E45768 (void);
// 0x000000B1 System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_mEC9842E0BC31D9D4E66FD30E6467D5A9A19034D6 (void);
// 0x000000B2 System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_mA4381FC291E17D67EA3C2292EAB8D3C959ADEA79 (void);
// 0x000000B3 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_mF6785C4DC8316E573F20A8356393946F6ABFC88C (void);
// 0x000000B4 System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m8E7AC9FF62E37EAB89F93FD0C1457555F6DCB086 (void);
// 0x000000B5 UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m2C738EA265E2B35868110EE1D8FCBD4F1D61C038 (void);
// 0x000000B6 System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_m27664A46276B3D615ECB12315F5E77C4F2AF29EE (void);
// 0x000000B7 System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_mF5BA8D140958AD2B5D2C8C5DE937E21A5D283C9F (void);
// 0x000000B8 System.Single SplineMesh.CubicBezierCurve::get_Length()
extern void CubicBezierCurve_get_Length_m5082DF3290B1CE1F6CAD6F369B230BEAAFC42DB5 (void);
// 0x000000B9 System.Void SplineMesh.CubicBezierCurve::set_Length(System.Single)
extern void CubicBezierCurve_set_Length_m9F77B2C0AA6EC07606FCB6B10FE3C6D4D2DC3B64 (void);
// 0x000000BA System.Void SplineMesh.CubicBezierCurve::.ctor(SplineMesh.SplineNode,SplineMesh.SplineNode)
extern void CubicBezierCurve__ctor_mA1AD052C90118D551E036008D38463B8F4368DA6 (void);
// 0x000000BB System.Void SplineMesh.CubicBezierCurve::ConnectStart(SplineMesh.SplineNode)
extern void CubicBezierCurve_ConnectStart_m3A40695F7ED43823D3AF5804CC8498EFDC2E7277 (void);
// 0x000000BC System.Void SplineMesh.CubicBezierCurve::ConnectEnd(SplineMesh.SplineNode)
extern void CubicBezierCurve_ConnectEnd_m1D0CB74ED3C616DEB86127455B726703F036EC2B (void);
// 0x000000BD UnityEngine.Vector3 SplineMesh.CubicBezierCurve::GetInverseDirection()
extern void CubicBezierCurve_GetInverseDirection_m3F64261A4F2FD6F2D11A2128473D571BD9AD3A61 (void);
// 0x000000BE UnityEngine.Vector3 SplineMesh.CubicBezierCurve::GetLocation(System.Single)
extern void CubicBezierCurve_GetLocation_m75A2C483E32ADB4FA13AF748923FA4A113D7E2C5 (void);
// 0x000000BF UnityEngine.Vector3 SplineMesh.CubicBezierCurve::GetTangent(System.Single)
extern void CubicBezierCurve_GetTangent_mDA3D54EF15507E2B822CE3B4E275DDE8A4AD0765 (void);
// 0x000000C0 UnityEngine.Vector3 SplineMesh.CubicBezierCurve::GetUp(System.Single)
extern void CubicBezierCurve_GetUp_mE03900461C9AAECD6A951DA44CE2802224D6BB0C (void);
// 0x000000C1 UnityEngine.Vector2 SplineMesh.CubicBezierCurve::GetScale(System.Single)
extern void CubicBezierCurve_GetScale_m8F11872104B3A4D713665F75472D75894A5C4331 (void);
// 0x000000C2 System.Single SplineMesh.CubicBezierCurve::GetRoll(System.Single)
extern void CubicBezierCurve_GetRoll_m5E1A5EF308122ECF2BB8F75691E1961B6852C420 (void);
// 0x000000C3 System.Void SplineMesh.CubicBezierCurve::ComputeSamples(System.Object,System.EventArgs)
extern void CubicBezierCurve_ComputeSamples_m435AD1667FB014EA026834922734FC0A80F5EB4F (void);
// 0x000000C4 SplineMesh.CurveSample SplineMesh.CubicBezierCurve::CreateSample(System.Single,System.Single)
extern void CubicBezierCurve_CreateSample_m19D365B323469A932D862F304D2F32392399784B (void);
// 0x000000C5 SplineMesh.CurveSample SplineMesh.CubicBezierCurve::GetSample(System.Single)
extern void CubicBezierCurve_GetSample_m7C6207AEA1DCAD5597D664703CF643B9E242505D (void);
// 0x000000C6 SplineMesh.CurveSample SplineMesh.CubicBezierCurve::GetSampleAtDistance(System.Single)
extern void CubicBezierCurve_GetSampleAtDistance_mF2519465EEA9DA43AC35D08C1BB7B9C9C4261603 (void);
// 0x000000C7 System.Void SplineMesh.CubicBezierCurve::AssertTimeInBounds(System.Single)
extern void CubicBezierCurve_AssertTimeInBounds_mDC13700258EE6F4386BA53942D0F6665937B0D00 (void);
// 0x000000C8 SplineMesh.CurveSample SplineMesh.CubicBezierCurve::GetProjectionSample(UnityEngine.Vector3)
extern void CubicBezierCurve_GetProjectionSample_m198A9E1687786CAD7A0187F0B714A1507C8D65CF (void);
// 0x000000C9 UnityEngine.Quaternion SplineMesh.CurveSample::get_Rotation()
extern void CurveSample_get_Rotation_m12EC3A372A61628C5434501F042DC94A89986DF5_AdjustorThunk (void);
// 0x000000CA System.Void SplineMesh.CurveSample::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector2,System.Single,System.Single,System.Single)
extern void CurveSample__ctor_m32B13BF83CB75A40752A0E4514CCEABB5EDE0890_AdjustorThunk (void);
// 0x000000CB System.Boolean SplineMesh.CurveSample::Equals(System.Object)
extern void CurveSample_Equals_mD9AF8E57858E6BDA77260066B4EDBB1E9C3DBD4F_AdjustorThunk (void);
// 0x000000CC System.Int32 SplineMesh.CurveSample::GetHashCode()
extern void CurveSample_GetHashCode_mA4F4E663E9E234361F72C7EE56F3D0511D148883_AdjustorThunk (void);
// 0x000000CD System.Boolean SplineMesh.CurveSample::op_Equality(SplineMesh.CurveSample,SplineMesh.CurveSample)
extern void CurveSample_op_Equality_m940288830BAD0554F536D3CCB40A78CA0D422A82 (void);
// 0x000000CE System.Boolean SplineMesh.CurveSample::op_Inequality(SplineMesh.CurveSample,SplineMesh.CurveSample)
extern void CurveSample_op_Inequality_m7139DDC0C0AA6DF5644727E7472137D0047C61BE (void);
// 0x000000CF SplineMesh.CurveSample SplineMesh.CurveSample::Lerp(SplineMesh.CurveSample,SplineMesh.CurveSample,System.Single)
extern void CurveSample_Lerp_m56950AD42D380848ABCB7EF6F803D51FCEDCF0C5 (void);
// 0x000000D0 SplineMesh.MeshVertex SplineMesh.CurveSample::GetBent(SplineMesh.MeshVertex)
extern void CurveSample_GetBent_m23AC96BC5669E1611F260AF061FC48CCF90D8226_AdjustorThunk (void);
// 0x000000D1 System.Boolean SplineMesh.Spline::get_IsLoop()
extern void Spline_get_IsLoop_mAAD8B8DAA2FD7F8F962B517F648FDAE3E4CBEAD0 (void);
// 0x000000D2 System.Void SplineMesh.Spline::set_IsLoop(System.Boolean)
extern void Spline_set_IsLoop_mEC84E3A178EDC68ADC4516C223CAEC88D291965E (void);
// 0x000000D3 System.Void SplineMesh.Spline::add_NodeListChanged(SplineMesh.ListChangeHandler`1<SplineMesh.SplineNode>)
extern void Spline_add_NodeListChanged_mC9AA7EF3FAA5C0B3A894305693CF6291A07008E2 (void);
// 0x000000D4 System.Void SplineMesh.Spline::remove_NodeListChanged(SplineMesh.ListChangeHandler`1<SplineMesh.SplineNode>)
extern void Spline_remove_NodeListChanged_m3E310440C66EBD7E541AF6D2556726033B54CEF6 (void);
// 0x000000D5 System.Void SplineMesh.Spline::Reset()
extern void Spline_Reset_m7A03F8AFA702BFE44A504D380BBFDD91799526B1 (void);
// 0x000000D6 System.Void SplineMesh.Spline::OnEnable()
extern void Spline_OnEnable_mE711C675006F38DD0D364873858E80DFABC073C0 (void);
// 0x000000D7 System.Collections.ObjectModel.ReadOnlyCollection`1<SplineMesh.CubicBezierCurve> SplineMesh.Spline::GetCurves()
extern void Spline_GetCurves_mA237EF8A322C0BF1C0D588E75E6BA409ADE1D58C (void);
// 0x000000D8 System.Void SplineMesh.Spline::RaiseNodeListChanged(SplineMesh.ListChangedEventArgs`1<SplineMesh.SplineNode>)
extern void Spline_RaiseNodeListChanged_m58B1DE7242D4A1CB32D11F141565FD5640746055 (void);
// 0x000000D9 System.Void SplineMesh.Spline::UpdateAfterCurveChanged()
extern void Spline_UpdateAfterCurveChanged_mBC8C3CBF000CDCF6721303DF815443A2DBA45C1D (void);
// 0x000000DA SplineMesh.CurveSample SplineMesh.Spline::GetSample(System.Single)
extern void Spline_GetSample_m79F3DF84C7247150AC0B1D8AA65B1F7E0DBF5332 (void);
// 0x000000DB SplineMesh.CubicBezierCurve SplineMesh.Spline::GetCurve(System.Single)
extern void Spline_GetCurve_m549F6369E3D73C304A4566D2135F824E62EB261B (void);
// 0x000000DC System.Int32 SplineMesh.Spline::GetNodeIndexForTime(System.Single)
extern void Spline_GetNodeIndexForTime_mA725AF3A95AF01F4398FFA3729E7310643C5BCE3 (void);
// 0x000000DD System.Void SplineMesh.Spline::RefreshCurves()
extern void Spline_RefreshCurves_mDB7CF3FCDF9DC0F2413BED2B832486FD800B1E7B (void);
// 0x000000DE SplineMesh.CurveSample SplineMesh.Spline::GetSampleAtDistance(System.Single)
extern void Spline_GetSampleAtDistance_m535736A11F122E1B2B02A7BC4BAD7C645EAC0D8C (void);
// 0x000000DF System.Void SplineMesh.Spline::AddNode(SplineMesh.SplineNode)
extern void Spline_AddNode_m1CB08761B4DEA22154D217FC83C64F9AF4D9D097 (void);
// 0x000000E0 System.Void SplineMesh.Spline::InsertNode(System.Int32,SplineMesh.SplineNode)
extern void Spline_InsertNode_m90EC00FCACDCA969C8F6926894994850DB22B30D (void);
// 0x000000E1 System.Void SplineMesh.Spline::RemoveNode(SplineMesh.SplineNode)
extern void Spline_RemoveNode_m92989CB5808908E06A3284D12E96653178A9D9A7 (void);
// 0x000000E2 System.Void SplineMesh.Spline::updateLoopBinding()
extern void Spline_updateLoopBinding_m0F220B0863DF6C8C5A40059EAB4F1DE85D392928 (void);
// 0x000000E3 System.Void SplineMesh.Spline::StartNodeChanged(System.Object,System.EventArgs)
extern void Spline_StartNodeChanged_m6508019AD84AEA54811674F2A6D63D0EB45F95F1 (void);
// 0x000000E4 System.Void SplineMesh.Spline::EndNodeChanged(System.Object,System.EventArgs)
extern void Spline_EndNodeChanged_mD69A191E94F3AD32190D1B04523170AB422941AB (void);
// 0x000000E5 SplineMesh.CurveSample SplineMesh.Spline::GetProjectionSample(UnityEngine.Vector3)
extern void Spline_GetProjectionSample_m671FF1697E9A6D955F743769B06D4802BEFFA5BD (void);
// 0x000000E6 System.Void SplineMesh.Spline::.ctor()
extern void Spline__ctor_m0EB49BF62FF595A502FEC2AD730BE6E318AD91FA (void);
// 0x000000E7 System.Void SplineMesh.ListChangedEventArgs`1::.ctor()
// 0x000000E8 System.Void SplineMesh.ListChangeHandler`1::.ctor(System.Object,System.IntPtr)
// 0x000000E9 System.Void SplineMesh.ListChangeHandler`1::Invoke(System.Object,SplineMesh.ListChangedEventArgs`1<T2>)
// 0x000000EA System.IAsyncResult SplineMesh.ListChangeHandler`1::BeginInvoke(System.Object,SplineMesh.ListChangedEventArgs`1<T2>,System.AsyncCallback,System.Object)
// 0x000000EB System.Void SplineMesh.ListChangeHandler`1::EndInvoke(System.IAsyncResult)
// 0x000000EC UnityEngine.Vector3 SplineMesh.SplineNode::get_Position()
extern void SplineNode_get_Position_mB9FB3BC4D2BC5ADDA2CF438B2710720EE1FE304C (void);
// 0x000000ED System.Void SplineMesh.SplineNode::set_Position(UnityEngine.Vector3)
extern void SplineNode_set_Position_m45768ABD2E18AD917C144AE373D4C1460376AED8 (void);
// 0x000000EE UnityEngine.Vector3 SplineMesh.SplineNode::get_Direction()
extern void SplineNode_get_Direction_m24AFBFE0574D9F9093E7637D9102CBA52CDE94F1 (void);
// 0x000000EF System.Void SplineMesh.SplineNode::set_Direction(UnityEngine.Vector3)
extern void SplineNode_set_Direction_m761EBC8150EF28F6156BF49ABCD5C27EB8C42855 (void);
// 0x000000F0 UnityEngine.Vector3 SplineMesh.SplineNode::get_Up()
extern void SplineNode_get_Up_mA4C435B54CDEE190B67ECAC1145548FECEE0C64A (void);
// 0x000000F1 System.Void SplineMesh.SplineNode::set_Up(UnityEngine.Vector3)
extern void SplineNode_set_Up_mFD16FE44DE72C4E5980F0391022E72D277828CF7 (void);
// 0x000000F2 UnityEngine.Vector2 SplineMesh.SplineNode::get_Scale()
extern void SplineNode_get_Scale_m12AA98174CA7D6955485B99F722E25AD7A3C3131 (void);
// 0x000000F3 System.Void SplineMesh.SplineNode::set_Scale(UnityEngine.Vector2)
extern void SplineNode_set_Scale_m46DD6EB07AE6989003C9C1D8E053A28656EC0ACD (void);
// 0x000000F4 System.Single SplineMesh.SplineNode::get_Roll()
extern void SplineNode_get_Roll_m1C3A5F2AEF7A4A8E2E0B04FEB5A6398942CA0433 (void);
// 0x000000F5 System.Void SplineMesh.SplineNode::set_Roll(System.Single)
extern void SplineNode_set_Roll_m02F3E2960C9A91FC6353019F0C53127A759ED8CA (void);
// 0x000000F6 System.Void SplineMesh.SplineNode::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern void SplineNode__ctor_m98587A573F31D62FEB60E895FF06CE53EE05C908 (void);
// 0x000000F7 System.Void SplineMesh.SplineNode::add_Changed(System.EventHandler)
extern void SplineNode_add_Changed_m2685DBE22A2B7F097335F2539D2E8CDD77E326C8 (void);
// 0x000000F8 System.Void SplineMesh.SplineNode::remove_Changed(System.EventHandler)
extern void SplineNode_remove_Changed_m065E3B27E2E2AC3C066C31322F1F11F218D3EDC4 (void);
// 0x000000F9 SplineMesh.Spline SplineMesh.SplineSmoother::get_Spline()
extern void SplineSmoother_get_Spline_mFA1CAC789DC283F9FB321B612AFF1D09A13FF5C8 (void);
// 0x000000FA System.Void SplineMesh.SplineSmoother::OnValidate()
extern void SplineSmoother_OnValidate_mE0C1412A8F1EC2F565A286DB9822AC29A9A7FC2E (void);
// 0x000000FB System.Void SplineMesh.SplineSmoother::OnEnable()
extern void SplineSmoother_OnEnable_m23C92077FD6B31A40ABE47B8A7B3D6D07AC5058C (void);
// 0x000000FC System.Void SplineMesh.SplineSmoother::OnDisable()
extern void SplineSmoother_OnDisable_m10149F042FD4CCFCD3492EEAD6B229AFC8360156 (void);
// 0x000000FD System.Void SplineMesh.SplineSmoother::Spline_NodeListChanged(System.Object,SplineMesh.ListChangedEventArgs`1<SplineMesh.SplineNode>)
extern void SplineSmoother_Spline_NodeListChanged_mEDC04AFCB1F6D81ADF08B0D017861564C9E00680 (void);
// 0x000000FE System.Void SplineMesh.SplineSmoother::OnNodeChanged(System.Object,System.EventArgs)
extern void SplineSmoother_OnNodeChanged_mF4EB1967C0AEBE004B7F3901BEBE484F303DD182 (void);
// 0x000000FF System.Void SplineMesh.SplineSmoother::SmoothNode(SplineMesh.SplineNode)
extern void SplineSmoother_SmoothNode_mB5E122971A3696999409B17889402EA49628B7EA (void);
// 0x00000100 System.Void SplineMesh.SplineSmoother::SmoothAll()
extern void SplineSmoother_SmoothAll_mDB1CC0BAEAD3B5AA900F0BBEF3F87197DEBBD2EC (void);
// 0x00000101 System.Void SplineMesh.SplineSmoother::.ctor()
extern void SplineSmoother__ctor_mD376935A6685D1DE97CECB9258874A2257FCC55D (void);
// 0x00000102 System.Void SplineMesh.ExampleContortAlong::OnEnable()
extern void ExampleContortAlong_OnEnable_mF7838AACD660A55CCC12A792FC14DC1A834CA2B8 (void);
// 0x00000103 System.Void SplineMesh.ExampleContortAlong::OnDisable()
extern void ExampleContortAlong_OnDisable_m2EFBC93678B148943C78E432C6C304E885436B5C (void);
// 0x00000104 System.Void SplineMesh.ExampleContortAlong::OnValidate()
extern void ExampleContortAlong_OnValidate_m83C5680DEE2B5A531BE4D0C23C747BA9CEC3B459 (void);
// 0x00000105 System.Void SplineMesh.ExampleContortAlong::EditorUpdate()
extern void ExampleContortAlong_EditorUpdate_mB0A413C91A345A3ADCDAAFDCBD86286986948260 (void);
// 0x00000106 System.Void SplineMesh.ExampleContortAlong::Contort()
extern void ExampleContortAlong_Contort_mD5F99BD4E6A561A77935C97D57ADB1B715D8DD72 (void);
// 0x00000107 System.Void SplineMesh.ExampleContortAlong::Init()
extern void ExampleContortAlong_Init_m87C79455B2793F550B823A8554F93DB63884AB35 (void);
// 0x00000108 System.Void SplineMesh.ExampleContortAlong::.ctor()
extern void ExampleContortAlong__ctor_m193171B52C7ADCD961826D3ECDBF2CD599A435CA (void);
// 0x00000109 System.Void SplineMesh.ExampleFollowSpline::OnEnable()
extern void ExampleFollowSpline_OnEnable_mA5AFC04210DB9B4738AC5B40AC9CA1BCD1A9F2AA (void);
// 0x0000010A System.Void SplineMesh.ExampleFollowSpline::OnDisable()
extern void ExampleFollowSpline_OnDisable_m725141669D5227B8DB31E0203017CFF688C07764 (void);
// 0x0000010B System.Void SplineMesh.ExampleFollowSpline::EditorUpdate()
extern void ExampleFollowSpline_EditorUpdate_mA5CB3FB5E6ECF17E1F4212116CB24A982B07896D (void);
// 0x0000010C System.Void SplineMesh.ExampleFollowSpline::PlaceFollower()
extern void ExampleFollowSpline_PlaceFollower_m30518F2A328F6FE403BF8553A54F44F84F971F42 (void);
// 0x0000010D System.Void SplineMesh.ExampleFollowSpline::.ctor()
extern void ExampleFollowSpline__ctor_m4E732FAA51376EC90D0B0B90152A01DE68A7F129 (void);
// 0x0000010E System.Void SplineMesh.ExampleGrowingRoot::OnEnable()
extern void ExampleGrowingRoot_OnEnable_mA2B0E195ECD65E9BC5A79ED9D619EAC5815F1D02 (void);
// 0x0000010F System.Void SplineMesh.ExampleGrowingRoot::OnDisable()
extern void ExampleGrowingRoot_OnDisable_m4239E494883008764E08EBA383C38D7A9A848B54 (void);
// 0x00000110 System.Void SplineMesh.ExampleGrowingRoot::OnValidate()
extern void ExampleGrowingRoot_OnValidate_mD4FAFBA4EF1B34A76B6EADBBC82DBBD72C4B42E0 (void);
// 0x00000111 System.Void SplineMesh.ExampleGrowingRoot::EditorUpdate()
extern void ExampleGrowingRoot_EditorUpdate_m7D4E386AE3016E97B0C9FD8A345C1EF3D845EA0D (void);
// 0x00000112 System.Void SplineMesh.ExampleGrowingRoot::Contort()
extern void ExampleGrowingRoot_Contort_mB06C25DA7C6282FB3EFAD8F1040811447F375DCB (void);
// 0x00000113 System.Void SplineMesh.ExampleGrowingRoot::Init()
extern void ExampleGrowingRoot_Init_m3EB07714EAF0E83E197B2CFA8101D2386ECB70D0 (void);
// 0x00000114 System.Void SplineMesh.ExampleGrowingRoot::.ctor()
extern void ExampleGrowingRoot__ctor_m577BFA03E45E998263B19DCB04B9D0BE0AF70A2A (void);
// 0x00000115 System.Void SplineMesh.ExampleSower::OnEnable()
extern void ExampleSower_OnEnable_m303C3E9A5E9406EB1752ECCA8F307F68DB796371 (void);
// 0x00000116 System.Void SplineMesh.ExampleSower::OnValidate()
extern void ExampleSower_OnValidate_mD2BA43503B8861163356F1C1E7C57D25808121D2 (void);
// 0x00000117 System.Void SplineMesh.ExampleSower::Update()
extern void ExampleSower_Update_m9C7C8A45AE2C14C3B96686D342FDFFEEAD78CA54 (void);
// 0x00000118 System.Void SplineMesh.ExampleSower::Sow()
extern void ExampleSower_Sow_mA1E4758F839D3A568CF197E7311CB039648AC10A (void);
// 0x00000119 System.Void SplineMesh.ExampleSower::.ctor()
extern void ExampleSower__ctor_mF0FAE098AB6AA19C4D715D926E3FD0A5BBB32036 (void);
// 0x0000011A System.Void SplineMesh.ExampleSower::<OnEnable>b__12_0(System.Object,SplineMesh.ListChangedEventArgs`1<SplineMesh.SplineNode>)
extern void ExampleSower_U3COnEnableU3Eb__12_0_mEF96ADA9A75BB7DE3BF0C48C1400594577636F4E (void);
// 0x0000011B System.Void SplineMesh.ExampleSower::<OnEnable>b__12_1()
extern void ExampleSower_U3COnEnableU3Eb__12_1_mC7897F657EDF6D56900D73FC70CBD80EBA176E0D (void);
// 0x0000011C System.Void SplineMesh.ExampleSower::<OnEnable>b__12_2()
extern void ExampleSower_U3COnEnableU3Eb__12_2_m9D5BBC7CB2F4679B5A200C66F429E6DB0580D4AD (void);
// 0x0000011D System.Void SplineMesh.ExampleTentacle::OnEnable()
extern void ExampleTentacle_OnEnable_m567A6EE78E936F003AF60484AC530C994745A252 (void);
// 0x0000011E System.Void SplineMesh.ExampleTentacle::OnValidate()
extern void ExampleTentacle_OnValidate_m438BF1A86936AE7BE709FB26DDF29751CEEC953A (void);
// 0x0000011F System.Void SplineMesh.ExampleTentacle::.ctor()
extern void ExampleTentacle__ctor_m6D097D31766A4E1981D3F0CD7B6CD5DA9469B190 (void);
// 0x00000120 System.Void SplineMesh.ExampleTrack::OnEnable()
extern void ExampleTrack_OnEnable_m9C3E79495F2C9088C0321E3173F3B6F39BF64FE5 (void);
// 0x00000121 System.Void SplineMesh.ExampleTrack::OnValidate()
extern void ExampleTrack_OnValidate_mFE7799F982E2D57DE0D8FF873F91E3E1D1D17EBB (void);
// 0x00000122 System.Void SplineMesh.ExampleTrack::Update()
extern void ExampleTrack_Update_m9C5DF30B69B9D3C45FEA75A8557357D616F23357 (void);
// 0x00000123 System.Void SplineMesh.ExampleTrack::CreateMeshes()
extern void ExampleTrack_CreateMeshes_m036E99CDD7ED2D00A160AE65A3C8BA687CF67C5A (void);
// 0x00000124 System.Void SplineMesh.ExampleTrack::.ctor()
extern void ExampleTrack__ctor_mD1F9484DEF697C914F8753745F277AE93904CE19 (void);
// 0x00000125 System.Void SplineMesh.ExampleTrack::<OnEnable>b__5_0(System.Object,SplineMesh.ListChangedEventArgs`1<SplineMesh.SplineNode>)
extern void ExampleTrack_U3COnEnableU3Eb__5_0_m511F1FB402B2EE562D71916F0CFCE6610FDE2701 (void);
// 0x00000126 System.Void SplineMesh.TrackSegment::.ctor()
extern void TrackSegment__ctor_m8985138C5E471DDDC635AABB43432BFA83C4287F (void);
// 0x00000127 System.Void SplineMesh.TransformedMesh::.ctor()
extern void TransformedMesh__ctor_mEBAEA1EBF0563FBDCA307AD3250FD279B0DA6FBC (void);
// 0x00000128 UnityEngine.GameObject SplineMesh.RopeBuilder::get_Generated()
extern void RopeBuilder_get_Generated_mC61A6581BA0400735181B216CF7C53E17AE37191 (void);
// 0x00000129 System.Void SplineMesh.RopeBuilder::OnEnable()
extern void RopeBuilder_OnEnable_m19D5BEDB506DEA7770F18D56459D6A63C1662E40 (void);
// 0x0000012A System.Void SplineMesh.RopeBuilder::OnValidate()
extern void RopeBuilder_OnValidate_mE8EB8870F70BEE530169190B93B9F79CFF1818EE (void);
// 0x0000012B System.Void SplineMesh.RopeBuilder::Update()
extern void RopeBuilder_Update_m884098CF2C8BAC17226FCB02980A2A945C5E5F95 (void);
// 0x0000012C System.Void SplineMesh.RopeBuilder::UpdateNodes()
extern void RopeBuilder_UpdateNodes_mBE69AAAEEE3B0B3C5A2F52C6A4BAFBB46E9070A2 (void);
// 0x0000012D System.Void SplineMesh.RopeBuilder::UpdateSpline()
extern void RopeBuilder_UpdateSpline_m2AC85FDC731590170C071B0DF7D736F2F6FC5C8B (void);
// 0x0000012E System.Void SplineMesh.RopeBuilder::Generate()
extern void RopeBuilder_Generate_m77D55939264F4046DF36CFF8D6C0E06507996517 (void);
// 0x0000012F System.Void SplineMesh.RopeBuilder::.ctor()
extern void RopeBuilder__ctor_m256F48ABCA41FBBC96BD623C4E5485CD4CDDB281 (void);
// 0x00000130 System.Collections.Generic.List`1<SplineMesh.ExtrusionSegment_Vertex> SplineMesh.ExtrusionSegment::get_ShapeVertices()
extern void ExtrusionSegment_get_ShapeVertices_m6D69711D6B557E0F346677C5EDD419DA1D3DE5B6 (void);
// 0x00000131 System.Void SplineMesh.ExtrusionSegment::set_ShapeVertices(System.Collections.Generic.List`1<SplineMesh.ExtrusionSegment_Vertex>)
extern void ExtrusionSegment_set_ShapeVertices_mDA6295EA450418E65F2373F8B8BFBEC615CDC354 (void);
// 0x00000132 System.Single SplineMesh.ExtrusionSegment::get_TextureScale()
extern void ExtrusionSegment_get_TextureScale_m9D14AC0E3A7101405A2306DCED3A6145996FAB88 (void);
// 0x00000133 System.Void SplineMesh.ExtrusionSegment::set_TextureScale(System.Single)
extern void ExtrusionSegment_set_TextureScale_mDDA6DFA5D54946E18539DF1CADAFC0BBC945C6F0 (void);
// 0x00000134 System.Single SplineMesh.ExtrusionSegment::get_TextureOffset()
extern void ExtrusionSegment_get_TextureOffset_m8599921134CFD72F3214D091F607561133EE04FE (void);
// 0x00000135 System.Void SplineMesh.ExtrusionSegment::set_TextureOffset(System.Single)
extern void ExtrusionSegment_set_TextureOffset_m7A5F79228C898A835570F2550F1AFC342594681C (void);
// 0x00000136 System.Single SplineMesh.ExtrusionSegment::get_SampleSpacing()
extern void ExtrusionSegment_get_SampleSpacing_m470E3B6FC5849B793E0196B2290F6A8B87A76E0C (void);
// 0x00000137 System.Void SplineMesh.ExtrusionSegment::set_SampleSpacing(System.Single)
extern void ExtrusionSegment_set_SampleSpacing_m8F468B88AB0811A899AFDF276AFF20B66FE00AD6 (void);
// 0x00000138 System.Void SplineMesh.ExtrusionSegment::OnEnable()
extern void ExtrusionSegment_OnEnable_m06512DAEBAED61A0E9F52842B348D5E501DA60EE (void);
// 0x00000139 System.Void SplineMesh.ExtrusionSegment::SetInterval(SplineMesh.CubicBezierCurve)
extern void ExtrusionSegment_SetInterval_mACF749D9F47F43EB85C080D7D6411F8DD83B634F (void);
// 0x0000013A System.Void SplineMesh.ExtrusionSegment::SetInterval(SplineMesh.Spline,System.Single,System.Single)
extern void ExtrusionSegment_SetInterval_m18E24BE29F2CF0667AEED6C96F07C42D57C04957 (void);
// 0x0000013B System.Void SplineMesh.ExtrusionSegment::SetDirty()
extern void ExtrusionSegment_SetDirty_m370D7B2092E11A335C753ACA7FADCDFB8BFA2221 (void);
// 0x0000013C System.Void SplineMesh.ExtrusionSegment::Update()
extern void ExtrusionSegment_Update_m3F4748B31D25C4BC6C0805DBF423B1E4711DA945 (void);
// 0x0000013D System.Void SplineMesh.ExtrusionSegment::ComputeIfNeeded()
extern void ExtrusionSegment_ComputeIfNeeded_m767F1A1D1E618CBBE708362269D12F1A2DB8BDA5 (void);
// 0x0000013E System.Collections.Generic.List`1<SplineMesh.CurveSample> SplineMesh.ExtrusionSegment::GetPath()
extern void ExtrusionSegment_GetPath_mFF4A51F851DD783BF23C6251E49327B24685490D (void);
// 0x0000013F System.Void SplineMesh.ExtrusionSegment::Compute()
extern void ExtrusionSegment_Compute_m9DD76117647220A7E08F2A5ECB658D365ECBCFF5 (void);
// 0x00000140 System.Void SplineMesh.ExtrusionSegment::.ctor()
extern void ExtrusionSegment__ctor_m330C2931D19CF58020F3056D1D6A4C80BF19B14F (void);
// 0x00000141 SplineMesh.SourceMesh SplineMesh.MeshBender::get_Source()
extern void MeshBender_get_Source_mAA711E24B40BC85423E9B4E0C0F4AA3C9CD5BD1B (void);
// 0x00000142 System.Void SplineMesh.MeshBender::set_Source(SplineMesh.SourceMesh)
extern void MeshBender_set_Source_mABA7B90704F20FD0E0DB098C8CF3FC4B1D393735 (void);
// 0x00000143 SplineMesh.MeshBender_FillingMode SplineMesh.MeshBender::get_Mode()
extern void MeshBender_get_Mode_m2D5CBFF8E88ECC0FCEE73884611AE8CBB6C9314A (void);
// 0x00000144 System.Void SplineMesh.MeshBender::set_Mode(SplineMesh.MeshBender_FillingMode)
extern void MeshBender_set_Mode_m9EE6173509C438B0A16E67743D46B555483BEE11 (void);
// 0x00000145 System.Void SplineMesh.MeshBender::SetInterval(SplineMesh.CubicBezierCurve)
extern void MeshBender_SetInterval_mD2A0DD0CC2688940B27A339BA18CA3CDAB3E0D0F (void);
// 0x00000146 System.Void SplineMesh.MeshBender::SetInterval(SplineMesh.Spline,System.Single,System.Single)
extern void MeshBender_SetInterval_m48EEB022D916BF0D75381ACE858FF476161AE7B9 (void);
// 0x00000147 System.Void SplineMesh.MeshBender::OnEnable()
extern void MeshBender_OnEnable_mD8AF660FE0022E1DA4023CF2C4E2A0CA86113D8D (void);
// 0x00000148 System.Void SplineMesh.MeshBender::Update()
extern void MeshBender_Update_m8B3980983FBB89C4B4A47A6182ADD7CD45DC96A6 (void);
// 0x00000149 System.Void SplineMesh.MeshBender::ComputeIfNeeded()
extern void MeshBender_ComputeIfNeeded_mE844401FDF7F0E04F84AB4BCEEE7745EA0F6F7AC (void);
// 0x0000014A System.Void SplineMesh.MeshBender::SetDirty()
extern void MeshBender_SetDirty_mB6DC4CDBE3BF713F629CC37CD80611130915BD79 (void);
// 0x0000014B System.Void SplineMesh.MeshBender::Compute()
extern void MeshBender_Compute_m9B5D232898974B2CD184F01ED7FFA58B53F4F36A (void);
// 0x0000014C System.Void SplineMesh.MeshBender::OnDestroy()
extern void MeshBender_OnDestroy_m8680893AFA02F731E5C2B5C2D64025146DEE3EDE (void);
// 0x0000014D System.Void SplineMesh.MeshBender::FillOnce()
extern void MeshBender_FillOnce_m56F5A59AFA162DDE7F9E3259F260C7B8CE638220 (void);
// 0x0000014E System.Void SplineMesh.MeshBender::FillRepeat()
extern void MeshBender_FillRepeat_m17CBAA349B1D0F1E9088A5238D7098B282519073 (void);
// 0x0000014F System.Void SplineMesh.MeshBender::FillStretch()
extern void MeshBender_FillStretch_m7AF6BAD2D623F3FCACF02A68D9297DE682F3434D (void);
// 0x00000150 System.Void SplineMesh.MeshBender::.ctor()
extern void MeshBender__ctor_m4EBF0737151F2682E67EB95477CB8F87602D0485 (void);
// 0x00000151 System.Void SplineMesh.MeshVertex::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector2)
extern void MeshVertex__ctor_m68DAFC04C45FD91053423850F4DB5FC9A28081A6 (void);
// 0x00000152 System.Void SplineMesh.MeshVertex::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern void MeshVertex__ctor_m38F8CF8B756D057812B5017D806A838EBCFA0194 (void);
// 0x00000153 UnityEngine.Mesh SplineMesh.SourceMesh::get_Mesh()
extern void SourceMesh_get_Mesh_mD5822953A429B22C9849A3E1B62715BAE6AE8883_AdjustorThunk (void);
// 0x00000154 System.Collections.Generic.List`1<SplineMesh.MeshVertex> SplineMesh.SourceMesh::get_Vertices()
extern void SourceMesh_get_Vertices_m85732F747D7CE434323D6E01532F65C95654D922_AdjustorThunk (void);
// 0x00000155 System.Int32[] SplineMesh.SourceMesh::get_Triangles()
extern void SourceMesh_get_Triangles_m3019F09D075CEFD5329BC932C81669AA72BC762C_AdjustorThunk (void);
// 0x00000156 System.Single SplineMesh.SourceMesh::get_MinX()
extern void SourceMesh_get_MinX_m8AEE8617C2C634C6878BDD80C7273114649CC40D_AdjustorThunk (void);
// 0x00000157 System.Single SplineMesh.SourceMesh::get_Length()
extern void SourceMesh_get_Length_mE0F51EB74C3E5C269B56021D3D6AC2FA735D74E3_AdjustorThunk (void);
// 0x00000158 System.Void SplineMesh.SourceMesh::.ctor(UnityEngine.Mesh)
extern void SourceMesh__ctor_m8EB6398E50FAEDF21BAD1BC329FC679CF616255E_AdjustorThunk (void);
// 0x00000159 System.Void SplineMesh.SourceMesh::.ctor(SplineMesh.SourceMesh)
extern void SourceMesh__ctor_mE93F93EBBC5DCCC772FD6DABD52320646B6ADA8C_AdjustorThunk (void);
// 0x0000015A SplineMesh.SourceMesh SplineMesh.SourceMesh::Build(UnityEngine.Mesh)
extern void SourceMesh_Build_m9311EA59CFAC0C51687872884386A13C0D9D46A6 (void);
// 0x0000015B SplineMesh.SourceMesh SplineMesh.SourceMesh::Translate(UnityEngine.Vector3)
extern void SourceMesh_Translate_m1697B3DE22A47D6D3226A61F2EECCF9242C5CF13_AdjustorThunk (void);
// 0x0000015C SplineMesh.SourceMesh SplineMesh.SourceMesh::Translate(System.Single,System.Single,System.Single)
extern void SourceMesh_Translate_m9662F3A26E0A594567327CB6B8CF8AFC3A106E98_AdjustorThunk (void);
// 0x0000015D SplineMesh.SourceMesh SplineMesh.SourceMesh::Rotate(UnityEngine.Quaternion)
extern void SourceMesh_Rotate_m64F45A9A69E80E0B99689D74192A18C7DE3173B5_AdjustorThunk (void);
// 0x0000015E SplineMesh.SourceMesh SplineMesh.SourceMesh::Scale(UnityEngine.Vector3)
extern void SourceMesh_Scale_m8B7D9146967546B25A75DACAF56BCDC420B20ED4_AdjustorThunk (void);
// 0x0000015F SplineMesh.SourceMesh SplineMesh.SourceMesh::Scale(System.Single,System.Single,System.Single)
extern void SourceMesh_Scale_m57CE92D4BF731C236A706ADADE04FDB58022F3BC_AdjustorThunk (void);
// 0x00000160 System.Void SplineMesh.SourceMesh::BuildData()
extern void SourceMesh_BuildData_mBF02CC17C119CFBB58B3FC44AE6116EEBC3A3341_AdjustorThunk (void);
// 0x00000161 System.Boolean SplineMesh.SourceMesh::Equals(System.Object)
extern void SourceMesh_Equals_mA2239E3B5CCFE04F905CFDF726D4616B6217A578_AdjustorThunk (void);
// 0x00000162 System.Int32 SplineMesh.SourceMesh::GetHashCode()
extern void SourceMesh_GetHashCode_m1E9A1EC18A819FBED34524BF7974E0E3AD574A6D_AdjustorThunk (void);
// 0x00000163 System.Boolean SplineMesh.SourceMesh::op_Equality(SplineMesh.SourceMesh,SplineMesh.SourceMesh)
extern void SourceMesh_op_Equality_m1E6D6A4A26EA51C05CB94E5BBE54010EA0BF9CE1 (void);
// 0x00000164 System.Boolean SplineMesh.SourceMesh::op_Inequality(SplineMesh.SourceMesh,SplineMesh.SourceMesh)
extern void SourceMesh_op_Inequality_m876C903730A5EC74A394E80F0AF4DF4782FBB6A9 (void);
// 0x00000165 System.Void SplineMesh.SplineExtrusion::Reset()
extern void SplineExtrusion_Reset_mDEC016069336429A5C988784FB7A9D2481706420 (void);
// 0x00000166 System.Void SplineMesh.SplineExtrusion::OnValidate()
extern void SplineExtrusion_OnValidate_m8CBFD50673612819636F2F725754FD7A1AF1319C (void);
// 0x00000167 System.Void SplineMesh.SplineExtrusion::OnEnable()
extern void SplineExtrusion_OnEnable_m83A7E883C689AB047D971AE6F19FAC4BDA4B62F5 (void);
// 0x00000168 System.Void SplineMesh.SplineExtrusion::Update()
extern void SplineExtrusion_Update_m0B361F04E531DD7F3D918F6DDC0DEDE6D4ABD7E2 (void);
// 0x00000169 System.Void SplineMesh.SplineExtrusion::GenerateMesh()
extern void SplineExtrusion_GenerateMesh_m705A24F42E1AACEEE72EDF865608C1C42BA37B15 (void);
// 0x0000016A System.Void SplineMesh.SplineExtrusion::SetToUpdate()
extern void SplineExtrusion_SetToUpdate_mD5B3DDB09A047C5BAF6B0EB472F4E8FE602B1146 (void);
// 0x0000016B System.Void SplineMesh.SplineExtrusion::.ctor()
extern void SplineExtrusion__ctor_m8399C72C29D606FA8BB89DB1D389FA312CD59A06 (void);
// 0x0000016C System.Void SplineMesh.SplineExtrusion::<OnEnable>b__9_0(System.Object,SplineMesh.ListChangedEventArgs`1<SplineMesh.SplineNode>)
extern void SplineExtrusion_U3COnEnableU3Eb__9_0_m36074849F20961576F532AB472F7E82328BCC9AD (void);
// 0x0000016D System.Void SplineMesh.SplineMeshTiling::OnEnable()
extern void SplineMeshTiling_OnEnable_m06602898315E06DD639577393BEC9D75CE7A15DC (void);
// 0x0000016E System.Void SplineMesh.SplineMeshTiling::OnValidate()
extern void SplineMeshTiling_OnValidate_m451E0F84CA9067281E28E3497D5BAA8863B40733 (void);
// 0x0000016F System.Void SplineMesh.SplineMeshTiling::Update()
extern void SplineMeshTiling_Update_m156EA4837039685AD1A817246F7E35FBB2576ECF (void);
// 0x00000170 System.Void SplineMesh.SplineMeshTiling::CreateMeshes()
extern void SplineMeshTiling_CreateMeshes_m0647CEA47D66D624A9247B953DC4CCA69C913CDB (void);
// 0x00000171 UnityEngine.GameObject SplineMesh.SplineMeshTiling::FindOrCreate(System.String)
extern void SplineMeshTiling_FindOrCreate_mEAE901F00DE893A38D6877F9BCE9E53EF454DAE5 (void);
// 0x00000172 System.Void SplineMesh.SplineMeshTiling::.ctor()
extern void SplineMeshTiling__ctor_m108A5B74A7CEF25BF434DAA44D40B45F242F8673 (void);
// 0x00000173 System.Void SplineMesh.SplineMeshTiling::<OnEnable>b__13_0(System.Object,SplineMesh.ListChangedEventArgs`1<SplineMesh.SplineNode>)
extern void SplineMeshTiling_U3COnEnableU3Eb__13_0_m29C4ADD8CEB4E49C669E36800C05A0C3E138E8A6 (void);
// 0x00000174 System.Boolean SplineMesh.CameraUtility::IsOnScreen(UnityEngine.Vector3)
extern void CameraUtility_IsOnScreen_m9DA3BC15A360DC4500BF36F63A273E6670D6A6E5 (void);
// 0x00000175 System.Int32[] SplineMesh.MeshUtility::GetReversedTriangles(UnityEngine.Mesh)
extern void MeshUtility_GetReversedTriangles_mB206A681F7CDA3F30C19DEE6046D9E2943C6E21F (void);
// 0x00000176 System.Void SplineMesh.MeshUtility::Update(UnityEngine.Mesh,UnityEngine.Mesh,System.Collections.Generic.IEnumerable`1<System.Int32>,System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>,System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>,System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>,System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>,System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>,System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>,System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>,System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>,System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>,System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>)
extern void MeshUtility_Update_m293B74E825E7809BB9DF84C1E7193767DE4989F2 (void);
// 0x00000177 System.Void SplineMesh.MeshUtility::.ctor()
extern void MeshUtility__ctor_mF62C47BD567C1DEEFC1933A55407FE8FEF6105D5 (void);
// 0x00000178 UnityEngine.GameObject SplineMesh.UOUtility::Create(System.String,UnityEngine.GameObject,System.Type[])
extern void UOUtility_Create_m79A06FD68FD2DF6A4FBF179B77FD676919D2B118 (void);
// 0x00000179 UnityEngine.GameObject SplineMesh.UOUtility::Instantiate(UnityEngine.GameObject,UnityEngine.Transform)
extern void UOUtility_Instantiate_mC3E3C4012590C8982944FC570A72737C2A9BD276 (void);
// 0x0000017A System.Void SplineMesh.UOUtility::Destroy(UnityEngine.GameObject)
extern void UOUtility_Destroy_mC8A244AE4C70CCEA82F9C3250CB15018911BE9AD (void);
// 0x0000017B System.Void SplineMesh.UOUtility::Destroy(UnityEngine.Component)
extern void UOUtility_Destroy_m39BE6A8E0CF388BE124F9FDBEFF519C468CC1E73 (void);
// 0x0000017C System.Void SplineMesh.UOUtility::DestroyChildren(UnityEngine.GameObject)
extern void UOUtility_DestroyChildren_m4A9667ABF2363D3461F8EF4C6E65D82EB825B022 (void);
// 0x0000017D System.Void Service.Singletons.GlobalDataController::Awake()
extern void GlobalDataController_Awake_mAC99E23BD358EBE17E35531388E5A6C742006902 (void);
// 0x0000017E System.Void Service.Singletons.GlobalDataController::.ctor()
extern void GlobalDataController__ctor_m19B00F456AD9EABA1A6BE11ADCA8E4A4020BDE4F (void);
// 0x0000017F T Service.Singletons.Singleton`1::get_Instance()
// 0x00000180 System.Void Service.Singletons.Singleton`1::.ctor()
// 0x00000181 System.String Service.Rest.RequestHeader::get_Key()
extern void RequestHeader_get_Key_m3A2B4A1CDFB2B7F9A8AC9EFCCED4E2026F224B4F (void);
// 0x00000182 System.Void Service.Rest.RequestHeader::set_Key(System.String)
extern void RequestHeader_set_Key_mC722B90800295D0D2F8D7A111CD15EF6AF2EE94E (void);
// 0x00000183 System.String Service.Rest.RequestHeader::get_Value()
extern void RequestHeader_get_Value_mBE7C0AFC2B54F388655D6B2ACD06F29DA81F1DFF (void);
// 0x00000184 System.Void Service.Rest.RequestHeader::set_Value(System.String)
extern void RequestHeader_set_Value_mE1C87A6D85D786A8FCA77342258B10E0D5327C44 (void);
// 0x00000185 System.Void Service.Rest.RequestHeader::.ctor()
extern void RequestHeader__ctor_mF2C37FD3744852423F9AC2BB0892DBEDDE333554 (void);
// 0x00000186 System.Int64 Service.Rest.Response::get_StatusCode()
extern void Response_get_StatusCode_m4AFEDEB66072BEEA17D379AAA3098142095B154B (void);
// 0x00000187 System.Void Service.Rest.Response::set_StatusCode(System.Int64)
extern void Response_set_StatusCode_m0174486BB7B6D527995007E4F8D986682CF3E3E4 (void);
// 0x00000188 System.String Service.Rest.Response::get_Error()
extern void Response_get_Error_m6B827CE1C5216DC68CD02B2D14CC4BCF734AEBEE (void);
// 0x00000189 System.Void Service.Rest.Response::set_Error(System.String)
extern void Response_set_Error_m61FA9D6F0B3448EE4CA3EE997C2C07548F7C0738 (void);
// 0x0000018A System.String Service.Rest.Response::get_Data()
extern void Response_get_Data_m533C8EECADD1590D8D91E034FE35684EA0A12B3C (void);
// 0x0000018B System.Void Service.Rest.Response::set_Data(System.String)
extern void Response_set_Data_m889420365BC4745949E086BBF51D9BC4C9F80054 (void);
// 0x0000018C System.Collections.Generic.Dictionary`2<System.String,System.String> Service.Rest.Response::get_Headers()
extern void Response_get_Headers_mCC3E0D3F6ED69D447BF101368EB26CF5486A9402 (void);
// 0x0000018D System.Void Service.Rest.Response::set_Headers(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void Response_set_Headers_m5FB48E7AD57BFC340E1420F46BB7C367AA298450 (void);
// 0x0000018E System.Void Service.Rest.Response::.ctor()
extern void Response__ctor_mA3F7F1B3A1B6633E46C8845BEE8B8665F77549C3 (void);
// 0x0000018F System.Collections.IEnumerator Service.Rest.RestWebClient::HttpGet(System.String,System.Action`1<Service.Rest.Response>)
extern void RestWebClient_HttpGet_m927816995B02693B5AF7D25D437533589D477813 (void);
// 0x00000190 System.Collections.IEnumerator Service.Rest.RestWebClient::HttpDelete(System.String,System.Action`1<Service.Rest.Response>)
extern void RestWebClient_HttpDelete_mF84B5E07CE36E24ECD0AACBC058B324E823A47C8 (void);
// 0x00000191 System.Collections.IEnumerator Service.Rest.RestWebClient::HttpPost(System.String,System.String,System.Action`1<Service.Rest.Response>,System.Collections.Generic.IEnumerable`1<Service.Rest.RequestHeader>)
extern void RestWebClient_HttpPost_m804535ABC9F95CC87ED4CB3F25D0BDE3F7263656 (void);
// 0x00000192 System.Collections.IEnumerator Service.Rest.RestWebClient::HttpPut(System.String,System.String,System.Action`1<Service.Rest.Response>,System.Collections.Generic.IEnumerable`1<Service.Rest.RequestHeader>)
extern void RestWebClient_HttpPut_m6792D259CEB2BB8ED7207CAAB0EDC0F35FD50CEC (void);
// 0x00000193 System.Collections.IEnumerator Service.Rest.RestWebClient::HttpHead(System.String,System.Action`1<Service.Rest.Response>)
extern void RestWebClient_HttpHead_m5F32AC030F72C4FC04D76C091505C0996A126119 (void);
// 0x00000194 System.Void Service.Rest.RestWebClient::.ctor()
extern void RestWebClient__ctor_mB223998642156DF065213E4913438D0DD84E387F (void);
// 0x00000195 System.Void Service.Models.ArmatureData::.ctor()
extern void ArmatureData__ctor_mE9305C84DB20D08F32729136813D50039BD48C70 (void);
// 0x00000196 System.Void Service.Models.ArmatureDataCollection::.ctor()
extern void ArmatureDataCollection__ctor_mB024E4452A4F274EA0A68A1AE0AE547CEE232EE1 (void);
// 0x00000197 System.Void Service.Models.ItemData::.ctor()
extern void ItemData__ctor_m29EFD39FFB2B4A1BEF42CDE5DE78AF32D03D7117 (void);
// 0x00000198 System.Void Service.Models.PipeData::.ctor()
extern void PipeData__ctor_mF0AE9CBAA970A085FF23AC52204BAF381DB43AD1 (void);
// 0x00000199 System.Void Service.Models.PipeDataCollection::.ctor()
extern void PipeDataCollection__ctor_m2152F3F1CD4BB5808478CF6A082BA9C95BF45124 (void);
// 0x0000019A System.Void Scenes.ChooseArmatures::Start()
extern void ChooseArmatures_Start_m411049CCA2C5677EA297C7E589BB33EC6B152E1B (void);
// 0x0000019B System.Void Scenes.ChooseArmatures::OnRequestComplete(Service.Rest.Response)
extern void ChooseArmatures_OnRequestComplete_mD3CCDC931D907E111A0A9A84901AE7B4A366545B (void);
// 0x0000019C System.Void Scenes.ChooseArmatures::OnPlaceArmatureButtonClicked()
extern void ChooseArmatures_OnPlaceArmatureButtonClicked_m6E9AE0C202245A64CAB828F8D39CA1C3F9A81442 (void);
// 0x0000019D System.Void Scenes.ChooseArmatures::OnAdoptArmatureToggleClicked(System.Boolean)
extern void ChooseArmatures_OnAdoptArmatureToggleClicked_mFCE677C3414E0548EABFB8BFCF76DD5D7FFA2A29 (void);
// 0x0000019E System.Void Scenes.ChooseArmatures::ArmatureOneDropdownValueChanged(TMPro.TMP_Dropdown)
extern void ChooseArmatures_ArmatureOneDropdownValueChanged_mC51FEF91F26A4BA593D4BE149823289A8B53D4FA (void);
// 0x0000019F System.Void Scenes.ChooseArmatures::ArmatureTwoDropdownValueChanged(TMPro.TMP_Dropdown)
extern void ChooseArmatures_ArmatureTwoDropdownValueChanged_m1E76804734D5FD8BA88D86118488941EF670E953 (void);
// 0x000001A0 System.Void Scenes.ChooseArmatures::AddEventListener()
extern void ChooseArmatures_AddEventListener_mAB53D5ECCCA49855DAF517330F98497C2892E4BD (void);
// 0x000001A1 System.Void Scenes.ChooseArmatures::FillDropDownWithData(TMPro.TMP_Dropdown)
extern void ChooseArmatures_FillDropDownWithData_m5D5C779F40BEC42FCC3DBC4E102DAE4B41E9738A (void);
// 0x000001A2 System.Void Scenes.ChooseArmatures::DataCheck()
extern void ChooseArmatures_DataCheck_m5710E3CC80A068DB61D3D0D86892AF8EE9B05D48 (void);
// 0x000001A3 System.Void Scenes.ChooseArmatures::.ctor()
extern void ChooseArmatures__ctor_m5E65C06EE9E7EC399025FA6BC8556212D58956BE (void);
// 0x000001A4 System.Void Scenes.ChooseArmatures::<Start>b__10_0(Service.Rest.Response)
extern void ChooseArmatures_U3CStartU3Eb__10_0_m581EC5F0A0F4C41E347BB166E81773912F6BFE9D (void);
// 0x000001A5 System.Boolean Scenes.ChooseArmatures::<OnRequestComplete>b__11_0(Service.Models.ArmatureData)
extern void ChooseArmatures_U3COnRequestCompleteU3Eb__11_0_m603E06009E8B69BC473C1488D71FB4296A65C7A3 (void);
// 0x000001A6 System.Boolean Scenes.ChooseArmatures::<OnAdoptArmatureToggleClicked>b__13_0(Service.Models.ArmatureData)
extern void ChooseArmatures_U3COnAdoptArmatureToggleClickedU3Eb__13_0_m4E05E1D6FA098BD7A93CD36337BF2CEE4FA38658 (void);
// 0x000001A7 System.Void Scenes.ChooseArmatures::<AddEventListener>b__16_0(System.Int32)
extern void ChooseArmatures_U3CAddEventListenerU3Eb__16_0_m28DBBECD92E9DEB8F542AD958E35A6BBB4FDF117 (void);
// 0x000001A8 System.Void Scenes.ChooseArmatures::<AddEventListener>b__16_1(System.Int32)
extern void ChooseArmatures_U3CAddEventListenerU3Eb__16_1_m93CF1C44334C20114513308D8249939F46D1D0E3 (void);
// 0x000001A9 System.Void Scenes.ChoosePipe::Awake()
extern void ChoosePipe_Awake_mAAED78B05A5917C3E024AE3CF9E75A5DC60CC08E (void);
// 0x000001AA System.Void Scenes.ChoosePipe::ResetConfiguration()
extern void ChoosePipe_ResetConfiguration_m7CE295103DC51B5719B53C77997EEA421489E1B9 (void);
// 0x000001AB System.Void Scenes.ChoosePipe::CategoryDropdownValueChanged(TMPro.TMP_Dropdown)
extern void ChoosePipe_CategoryDropdownValueChanged_m9D2D405C6C9C846BD26E2979DE0F5ADD561A3804 (void);
// 0x000001AC System.Void Scenes.ChoosePipe::PipeTypeDropdownValueChanged(TMPro.TMP_Dropdown)
extern void ChoosePipe_PipeTypeDropdownValueChanged_mA2E0F4B245693F26758333F20E7C37C8E96B456D (void);
// 0x000001AD System.Void Scenes.ChoosePipe::OnRequestComplete(Service.Rest.Response)
extern void ChoosePipe_OnRequestComplete_m1E0E51E56ED6471C9F7A4275910AB3D122F3A822 (void);
// 0x000001AE System.Void Scenes.ChoosePipe::AddEventListener()
extern void ChoosePipe_AddEventListener_m03C1BD855D372A155FF90969FDA562893E47C5A6 (void);
// 0x000001AF System.Void Scenes.ChoosePipe::OnPlacePipeButtonClicked()
extern void ChoosePipe_OnPlacePipeButtonClicked_mBE70A741E913D1D14829E5D0639D0619B6B5EA82 (void);
// 0x000001B0 System.Void Scenes.ChoosePipe::.ctor()
extern void ChoosePipe__ctor_mAD7E37B4448A60E4C4070E6E92D21D16BAF2AB51 (void);
// 0x000001B1 System.Void Scenes.ChoosePipe::<CategoryDropdownValueChanged>b__11_0(Service.Rest.Response)
extern void ChoosePipe_U3CCategoryDropdownValueChangedU3Eb__11_0_m847D8470A75F4D5F62678F5CA7ADA402C2BABC38 (void);
// 0x000001B2 System.Boolean Scenes.ChoosePipe::<OnRequestComplete>b__13_0(Service.Models.PipeData)
extern void ChoosePipe_U3COnRequestCompleteU3Eb__13_0_mBE4679A5351937DC9E86EEA471D279B083DF37CD (void);
// 0x000001B3 System.Void Scenes.ChoosePipe::<AddEventListener>b__14_0(System.Int32)
extern void ChoosePipe_U3CAddEventListenerU3Eb__14_0_mEC750E2BFC7148B65EC0BF6EE849A7FA430D47DC (void);
// 0x000001B4 System.Void Scenes.ChoosePipe::<AddEventListener>b__14_1(System.Int32)
extern void ChoosePipe_U3CAddEventListenerU3Eb__14_1_mF86E2C6D22C0B42E1C0DE621357ED5297D611D3A (void);
// 0x000001B5 System.Void Scenes.Error::Update()
extern void Error_Update_m882CC7E0B84B9591B05823AB88E2244BBEE57674 (void);
// 0x000001B6 System.Void Scenes.Error::.ctor()
extern void Error__ctor_m8BE2636542E3622585AA12AC27C485BC736E31ED (void);
// 0x000001B7 System.Void Scenes.OrderOverview::Start()
extern void OrderOverview_Start_m0EB29791580D029C6EABFF19377F07A1DFA3F752 (void);
// 0x000001B8 System.Void Scenes.OrderOverview::DataCheck()
extern void OrderOverview_DataCheck_m4469E5A063B7CEF435DED7F722ADF236AE87D324 (void);
// 0x000001B9 System.Void Scenes.OrderOverview::InitTextFields()
extern void OrderOverview_InitTextFields_m97AFC9DCF1C0280CC61E4905E823C0A813211D2B (void);
// 0x000001BA System.Void Scenes.OrderOverview::AddListeners()
extern void OrderOverview_AddListeners_m0B1E0ED4839FFDC9D05CDAFE7268ECE25EFA461C (void);
// 0x000001BB System.Void Scenes.OrderOverview::LengthValueChanged(TMPro.TMP_InputField)
extern void OrderOverview_LengthValueChanged_mAD182FED1D9D18DAD789C0C4862D6BA7CC9128BC (void);
// 0x000001BC System.Void Scenes.OrderOverview::AngleValueChanged(TMPro.TMP_InputField)
extern void OrderOverview_AngleValueChanged_mDBAEA85AAC16E0473FABBADC9D3F03728A96CC6C (void);
// 0x000001BD System.Boolean Scenes.OrderOverview::IsAngled()
extern void OrderOverview_IsAngled_m8E1AE6B43C777CE178D3AF6F2266ECDB11D5598C (void);
// 0x000001BE System.Void Scenes.OrderOverview::OnRequestComplete(Service.Rest.Response)
extern void OrderOverview_OnRequestComplete_mA23FF8DAA8D95D702EEA18FFF9087ADCB5CCF69E (void);
// 0x000001BF System.Boolean Scenes.OrderOverview::IsAvailable()
extern void OrderOverview_IsAvailable_mBC83D8C43A4B6E34123B17C2B3D2A820CD33EB3F (void);
// 0x000001C0 System.Double Scenes.OrderOverview::CalculateGrossPrice()
extern void OrderOverview_CalculateGrossPrice_m8A79FE6D28C50823B145D1999F48144E44A50497 (void);
// 0x000001C1 System.Double Scenes.OrderOverview::CalculateNetPrice()
extern void OrderOverview_CalculateNetPrice_mFC6CC3DF5459A3C66CAD21A1A96891CE36617F9E (void);
// 0x000001C2 System.Collections.Generic.List`1<Service.Models.ItemData> Scenes.OrderOverview::ParseXml(System.String)
extern void OrderOverview_ParseXml_m1D84A1BF66749D77F0880997DD8616E00C1A3495 (void);
// 0x000001C3 System.Void Scenes.OrderOverview::.ctor()
extern void OrderOverview__ctor_mF5D5324828ED94E9E92F176E8AD99BDAF52D5041 (void);
// 0x000001C4 System.Void Scenes.OrderOverview::<DataCheck>b__16_0(Service.Rest.Response)
extern void OrderOverview_U3CDataCheckU3Eb__16_0_m18450BCC626FE31417EC9A03E9B80D7C1B5CC614 (void);
// 0x000001C5 System.Void Scenes.OrderOverview::<AddListeners>b__18_0(System.String)
extern void OrderOverview_U3CAddListenersU3Eb__18_0_m3F22AA6237DB3BD6AA796EB88FDF39425E3EA839 (void);
// 0x000001C6 System.Void Scenes.OrderOverview::<AddListeners>b__18_1(System.String)
extern void OrderOverview_U3CAddListenersU3Eb__18_1_mCC20ED68290726A1D464FBC3699F99F2D6A3A581 (void);
// 0x000001C7 System.Void Scenes.PlaceObjectsInAR.PlacementController::Start()
extern void PlacementController_Start_m4420C505AA8D9289FEADC6D6DA1A2E44826DF8B1 (void);
// 0x000001C8 System.Void Scenes.PlaceObjectsInAR.PlacementController::Update()
extern void PlacementController_Update_m506C92280E3D90949D2DBD3DA5F58A50771FCF61 (void);
// 0x000001C9 System.Void Scenes.PlaceObjectsInAR.PlacementController::NegateCanvasVisibility()
extern void PlacementController_NegateCanvasVisibility_mA14CD8300E6A4F2EA4340061FEC69C8D4F2AA4CB (void);
// 0x000001CA System.Void Scenes.PlaceObjectsInAR.PlacementController::DataCheck()
extern void PlacementController_DataCheck_mC5A943B4CE29584A5876EB16C69D9975A1D45627 (void);
// 0x000001CB System.Void Scenes.PlaceObjectsInAR.PlacementController::.ctor()
extern void PlacementController__ctor_mE7D8E5E75398D80B9113BA447CFEC03D116B2B24 (void);
// 0x000001CC System.Void Scenes.PlaceObjectsInAR.Pipe.PlacePipe::Start()
extern void PlacePipe_Start_mCB4C514447D2BC5D90CD6C8DFB2BEAB992C97AAB (void);
// 0x000001CD System.Void Scenes.PlaceObjectsInAR.Pipe.PlacePipe::Update()
extern void PlacePipe_Update_m0F08EAA6966D129C82D4408E34DF7F45CA92D2B4 (void);
// 0x000001CE System.Void Scenes.PlaceObjectsInAR.Pipe.PlacePipe::Raycasting()
extern void PlacePipe_Raycasting_mEFBE4A4C72B1AE367B293DAC79D08BF525752813 (void);
// 0x000001CF System.Void Scenes.PlaceObjectsInAR.Pipe.PlacePipe::Add()
extern void PlacePipe_Add_m2DCB9A00D1152856F808DFAAACBED087A467066B (void);
// 0x000001D0 System.Void Scenes.PlaceObjectsInAR.Pipe.PlacePipe::RemoveLast()
extern void PlacePipe_RemoveLast_m34262F3BB0913C0B4AF5A3C06DDBAF6A7C8E465D (void);
// 0x000001D1 System.Void Scenes.PlaceObjectsInAR.Pipe.PlacePipe::DrawPipe()
extern void PlacePipe_DrawPipe_mB24AC37A65645CD95A21D3EA5B337E2CF72D7455 (void);
// 0x000001D2 System.Void Scenes.PlaceObjectsInAR.Pipe.PlacePipe::UpdateButtons()
extern void PlacePipe_UpdateButtons_m6E95510F0302A03BDA15F0BA78C81B7A1E376E7C (void);
// 0x000001D3 System.Void Scenes.PlaceObjectsInAR.Pipe.PlacePipe::UpdateTextField()
extern void PlacePipe_UpdateTextField_mDD75A75DC3D8DF88036400C1EF400CBD51FFAE87 (void);
// 0x000001D4 System.Void Scenes.PlaceObjectsInAR.Pipe.PlacePipe::ResetScene()
extern void PlacePipe_ResetScene_m0662F6D1A24FAED0668C4C4909F017E1819490BB (void);
// 0x000001D5 System.Void Scenes.PlaceObjectsInAR.Pipe.PlacePipe::Finish()
extern void PlacePipe_Finish_mC8714AB0AD4A216264D434F16E94821D776C6163 (void);
// 0x000001D6 System.Void Scenes.PlaceObjectsInAR.Pipe.PlacePipe::.ctor()
extern void PlacePipe__ctor_mACD9DC8F612DE8958832572345E0EA2D73DA09EA (void);
// 0x000001D7 System.Void Scenes.PlaceObjectsInAR.Pipe.PlacePipe::.cctor()
extern void PlacePipe__cctor_m274AD8DCC794FFB213BA6C7CC116A593408F0599 (void);
// 0x000001D8 System.Void Scenes.PlaceObjectsInAR.Armature.PlaceArmatures::Start()
extern void PlaceArmatures_Start_m65DA4BEFA56D2D36EA0E6810148FAE3CD5966AE8 (void);
// 0x000001D9 System.Void Scenes.PlaceObjectsInAR.Armature.PlaceArmatures::Update()
extern void PlaceArmatures_Update_mF900C2E2693C7ABCC8EE4C03ACD4823A5B2645AD (void);
// 0x000001DA System.Void Scenes.PlaceObjectsInAR.Armature.PlaceArmatures::SetArmaturesOnPosition()
extern void PlaceArmatures_SetArmaturesOnPosition_m0A8B7C5B5745DECCCA5B4B56DE9D316D24F5A4BD (void);
// 0x000001DB System.Void Scenes.PlaceObjectsInAR.Armature.PlaceArmatures::CheckAngled()
extern void PlaceArmatures_CheckAngled_mC896B38DD7F6EFE3CF37FB7C4CA910F813076E78 (void);
// 0x000001DC System.Void Scenes.PlaceObjectsInAR.Armature.PlaceArmatures::InitializeTexts()
extern void PlaceArmatures_InitializeTexts_mAD4468F9B7D6B4367E983DCE2D81B5ECB1E46618 (void);
// 0x000001DD System.Void Scenes.PlaceObjectsInAR.Armature.PlaceArmatures::SelectObject(UnityEngine.GameObject,UnityEngine.GameObject)
extern void PlaceArmatures_SelectObject_m72806760DC2AC7E68E4B0648B3CE2EB23552839D (void);
// 0x000001DE System.Void Scenes.PlaceObjectsInAR.Armature.PlaceArmatures::Deselect()
extern void PlaceArmatures_Deselect_mB27E85C6B94391558FA64EB457383F3CDFFD57C6 (void);
// 0x000001DF System.Void Scenes.PlaceObjectsInAR.Armature.PlaceArmatures::SetObjectVisualizer(UnityEngine.GameObject,UnityEngine.GameObject)
extern void PlaceArmatures_SetObjectVisualizer_m2607E122E08F39DFF68DA5DD1E8AB367E7A38CB1 (void);
// 0x000001E0 System.Void Scenes.PlaceObjectsInAR.Armature.PlaceArmatures::RotateObject(System.Single,System.Single)
extern void PlaceArmatures_RotateObject_mDE7B762D0E0D060C10CCEFE0C56B4CBE06F919FF (void);
// 0x000001E1 System.Void Scenes.PlaceObjectsInAR.Armature.PlaceArmatures::UpdateAngleValue()
extern void PlaceArmatures_UpdateAngleValue_mBA3632EDB5FFDB764C8832CD38733DEC9A5D4632 (void);
// 0x000001E2 System.Void Scenes.PlaceObjectsInAR.Armature.PlaceArmatures::ChangePipeMaterial(UnityEngine.Material)
extern void PlaceArmatures_ChangePipeMaterial_m39DE00385C2DDA294E7BCE31E3A99AAACD60BB31 (void);
// 0x000001E3 System.Void Scenes.PlaceObjectsInAR.Armature.PlaceArmatures::ChangeArmatureMaterial(UnityEngine.GameObject,UnityEngine.Material)
extern void PlaceArmatures_ChangeArmatureMaterial_m74D6A1B67F99E95A0CFF0000AEE00E32A85F6EB9 (void);
// 0x000001E4 System.Void Scenes.PlaceObjectsInAR.Armature.PlaceArmatures::Finish()
extern void PlaceArmatures_Finish_m7EF3100CE5BDC954493A98070A52E94E39A945E9 (void);
// 0x000001E5 System.Void Scenes.PlaceObjectsInAR.Armature.PlaceArmatures::.ctor()
extern void PlaceArmatures__ctor_mC1CDD280A12F690D7F0D10F66105F6BE9924358C (void);
// 0x000001E6 System.Void Loading.LoadingProgress::Start()
extern void LoadingProgress_Start_m89EAC7EFCD1E51CE32D9C31E9AC320840747533C (void);
// 0x000001E7 System.Void Loading.LoadingProgress::Update()
extern void LoadingProgress_Update_m38F1D29CA5E253A7362F7383CB2C69761FF400D9 (void);
// 0x000001E8 System.Void Loading.LoadingProgress::.ctor()
extern void LoadingProgress__ctor_mF133A4E04E677D7338C4E5E833DF13EA18CF9104 (void);
// 0x000001E9 System.Void Loading.SceneLoader::Load(Loading.SceneLoader_Scene)
extern void SceneLoader_Load_m4356F5746D09B88AE072993683CF457E2DF6580D (void);
// 0x000001EA System.Void Loading.SceneLoader::LoadSceneAdaptive(Loading.SceneLoader_Scene)
extern void SceneLoader_LoadSceneAdaptive_m4413745F03F34942419CD0EFFAFE2F4CC699F4E6 (void);
// 0x000001EB System.Collections.IEnumerator Loading.SceneLoader::SetActive(System.String)
extern void SceneLoader_SetActive_m2A5B7D715F6916212EC27BD102527F9E189DBE1C (void);
// 0x000001EC System.Void Loading.SceneLoader::UnLoadScene(Loading.SceneLoader_Scene)
extern void SceneLoader_UnLoadScene_m356A6C86F3F8825E28F40F78122BEDEEC48F7799 (void);
// 0x000001ED System.Collections.IEnumerator Loading.SceneLoader::LoadSceneAsync(Loading.SceneLoader_Scene)
extern void SceneLoader_LoadSceneAsync_mA7B1EB4E8A6C428BD42C74AA97573219FC6F1EB1 (void);
// 0x000001EE System.Single Loading.SceneLoader::GetLoadingProgress()
extern void SceneLoader_GetLoadingProgress_mCE591F3C8E279E39B40B7D6DD60AB33F9EDEE31C (void);
// 0x000001EF Loading.SceneLoader_Scene Loading.SceneLoader::FindScene(System.String)
extern void SceneLoader_FindScene_m778C5CCC46E9BEBA487C4EA32F6DB7DCA8CF58BC (void);
// 0x000001F0 System.Void Loading.SceneLoader::SceneLoaderCallback()
extern void SceneLoader_SceneLoaderCallback_m1777D34AAB6327B512DDEF49E2A228E5A1E43DCA (void);
// 0x000001F1 System.Void Loading.SceneLoaderCallback::Update()
extern void SceneLoaderCallback_Update_mA0B08178E51AB738F9567F55081B56D50EE630C8 (void);
// 0x000001F2 System.Void Loading.SceneLoaderCallback::.ctor()
extern void SceneLoaderCallback__ctor_m41083CDC93B7561E8F47CE927B3D21DBEE209291 (void);
// 0x000001F3 System.Void InputHandler.ButtonHandler::LoadScene(System.String)
extern void ButtonHandler_LoadScene_m9194A4B6B1990C84E5BC9349EB80ACDB2074E1DE (void);
// 0x000001F4 System.Void InputHandler.ButtonHandler::.ctor()
extern void ButtonHandler__ctor_m2FE0B4B9A17406C58B3DA2D88249F784FE50EC5D (void);
// 0x000001F5 System.Void InputHandler.ButtonSwitcher::Start()
extern void ButtonSwitcher_Start_m7F11F558F06D1A169E981AD650A8AB8FC1058EA2 (void);
// 0x000001F6 System.Void InputHandler.ButtonSwitcher::Update()
extern void ButtonSwitcher_Update_m3806F33A98679AE2AC045491E747697033A95892 (void);
// 0x000001F7 System.Void InputHandler.ButtonSwitcher::MoveCanvas(System.Single)
extern void ButtonSwitcher_MoveCanvas_mB95A921EEE0F25CBA1970675BF52394EFAAD296F (void);
// 0x000001F8 System.Void InputHandler.ButtonSwitcher::CheckPlacementMethod()
extern void ButtonSwitcher_CheckPlacementMethod_m9197A1BAAE48F35DA13BA108EA48EC5FD6329851 (void);
// 0x000001F9 System.Void InputHandler.ButtonSwitcher::.ctor()
extern void ButtonSwitcher__ctor_mBA1EA68B10F5BB8130A2C1E68690986E409C11C0 (void);
// 0x000001FA System.Void InputHandler.ButtonSwitcher::.cctor()
extern void ButtonSwitcher__cctor_m50BC7200C20DD3E67E41592FF116A8F6E7296AEC (void);
// 0x000001FB System.Void InputHandler.SwipeGesture::Start()
extern void SwipeGesture_Start_mD3A7D399C033A145F74E843929C3252CE533BB46 (void);
// 0x000001FC System.Void InputHandler.SwipeGesture::Update()
extern void SwipeGesture_Update_m15CB5B36D9C8F88DF209C9F9EB8471E1A9F7B7E9 (void);
// 0x000001FD System.Void InputHandler.SwipeGesture::.ctor()
extern void SwipeGesture__ctor_mF056D731568CC4418941F4CA6196B7A5A98F6EA2 (void);
// 0x000001FE System.Void Helper.Create3DPipe::set_WayPoints(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void Create3DPipe_set_WayPoints_m50E6F11C5BED4EA7B0BF3140498CCA284F09C526 (void);
// 0x000001FF System.Void Helper.Create3DPipe::DrawPipeFromWaypoints(SplineMesh.Spline)
extern void Create3DPipe_DrawPipeFromWaypoints_m8F23C3DCC5216B883BDB48CD2B36D60E78479657 (void);
// 0x00000200 System.Void Helper.Create3DPipe::.ctor()
extern void Create3DPipe__ctor_m12A8AA279C1BECB267B521A518CAACE4529EE673 (void);
// 0x00000201 System.Void Helper.FindArmatureModel::.ctor()
extern void FindArmatureModel__ctor_m290A3FFA26F9B60A94760E76DC60D1C8F87E76B1 (void);
// 0x00000202 UnityEngine.GameObject Helper.FindArmatureModel::GetArmatureDependingOnName(System.String)
extern void FindArmatureModel_GetArmatureDependingOnName_mE41B088012B3CB556CF8820AE2217F65AEC9E536 (void);
// 0x00000203 System.Void Helper.Redirect::Update()
extern void Redirect_Update_m7BCCA10904C2FCA79BEAD291F2C5D3B4AABF52D1 (void);
// 0x00000204 System.Void Helper.Redirect::.ctor()
extern void Redirect__ctor_m2A77D47045DBD6A24CC314E64449DFA1CE9CA7D2 (void);
// 0x00000205 System.Void EnvMapAnimator_<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m8B0264798939C569742263D32E0054DBAB9AE6FF (void);
// 0x00000206 System.Void EnvMapAnimator_<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m3EFE2ADAD412045F666CFA1C8C9FF53AF92CBD75 (void);
// 0x00000207 System.Boolean EnvMapAnimator_<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m84F94A5CD6012300AC80698CDCA870A0A146E226 (void);
// 0x00000208 System.Object EnvMapAnimator_<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m129CB3E5CAFFA1D19D4988182EEF116F2086A637 (void);
// 0x00000209 System.Void EnvMapAnimator_<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m345E255900454CC505A8AAE3BF6AEF3C06467EAB (void);
// 0x0000020A System.Object EnvMapAnimator_<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m5920F51DCC2DC7C8BC98EE95D6CD4D7784997272 (void);
// 0x0000020B System.Void TMPro.TMP_TextEventHandler_CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_mE2C306B8090F90261252C94D26AB5085580B11D5 (void);
// 0x0000020C System.Void TMPro.TMP_TextEventHandler_SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m9D9F101CB717ACD5449336DFFF70F86AE32BB6EC (void);
// 0x0000020D System.Void TMPro.TMP_TextEventHandler_WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_mFD7F2937426D4AA1A8CBB13F62C3CC1D2061AD1E (void);
// 0x0000020E System.Void TMPro.TMP_TextEventHandler_LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_mA23AFEC8E11183CF472044FA72B07AD28ED6E675 (void);
// 0x0000020F System.Void TMPro.TMP_TextEventHandler_LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m02CC491DBE4B2FF05A8FD4285813215ED3D323E5 (void);
// 0x00000210 System.Void TMPro.Examples.Benchmark01_<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m328932E4B6124311CD738F2F84F69BC149209129 (void);
// 0x00000211 System.Void TMPro.Examples.Benchmark01_<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m209F531CE6ED7F07649497DD15817C1D7C1880A1 (void);
// 0x00000212 System.Boolean TMPro.Examples.Benchmark01_<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mD927C85D41034011055A7CA3AFFAF4E10464F65D (void);
// 0x00000213 System.Object TMPro.Examples.Benchmark01_<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m18BA91C8A20CBD6976D52E335563D9B42C1AE9A8 (void);
// 0x00000214 System.Void TMPro.Examples.Benchmark01_<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m5A67B5BDE759A157229E6CF24E653B79B2AC0200 (void);
// 0x00000215 System.Object TMPro.Examples.Benchmark01_<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA521C1EFA357A9F4F4CAA68A4D0B85468764323C (void);
// 0x00000216 System.Void TMPro.Examples.Benchmark01_UGUI_<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m9A8C7C0644996520AD443A4F7CA527BF05C54C3C (void);
// 0x00000217 System.Void TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m94D1420C08F57F2901E2499D36778BB8F1C76932 (void);
// 0x00000218 System.Boolean TMPro.Examples.Benchmark01_UGUI_<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_m31AF957FFAEEED4BE0F39A1185C6112C4EB6F7AA (void);
// 0x00000219 System.Object TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39B535B222104759319A54A6D7E2E81482A1F71E (void);
// 0x0000021A System.Void TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mC2B435140D045B6A20FB105E0E2CBD625218CA74 (void);
// 0x0000021B System.Object TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m9A98CCB7604AAD93919CAE48955C6A6CB8C38790 (void);
// 0x0000021C System.Void TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_mB5F6ED6FCDA5BEAD56E22B64283D7A4D7F7EAE71 (void);
// 0x0000021D System.Void TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m29AAE9560CA4EEB4A548A68ACA085EC9E4CB8EA5 (void);
// 0x0000021E System.Boolean TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_m4F2D37B672E95820F49489611196CDE334736157 (void);
// 0x0000021F System.Object TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m21D2B0A0B0CADF520D05FE4948F1DE94CF119630 (void);
// 0x00000220 System.Void TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_m1E942C0FD32005FDBB182CF646FD2312BA273BC7 (void);
// 0x00000221 System.Object TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mFC3602799F1D07BB002093DFB879FC759384FDD3 (void);
// 0x00000222 System.Void TMPro.Examples.SkewTextExample_<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_mA03118DB0FD3BF160500E127D1FACDAF45313047 (void);
// 0x00000223 System.Void TMPro.Examples.SkewTextExample_<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m9D7F6A90DA911D77EE72A2824FF9690CED05FBC8 (void);
// 0x00000224 System.Boolean TMPro.Examples.SkewTextExample_<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_m7FE0DD003507BAD92E35CC5DACE5D043ADD766ED (void);
// 0x00000225 System.Object TMPro.Examples.SkewTextExample_<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m349F81ECD49FF12E4009E2E56DB81974D68C6DAD (void);
// 0x00000226 System.Void TMPro.Examples.SkewTextExample_<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_m3122CE754238FB7815F7ABE8E7DFAF3AB7B03278 (void);
// 0x00000227 System.Object TMPro.Examples.SkewTextExample_<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m79BF250C4ADC29ACF11370E2B5BD4FFD78709565 (void);
// 0x00000228 System.Void TMPro.Examples.TeleType_<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m8231909D78A27061165C450481E233339F300046 (void);
// 0x00000229 System.Void TMPro.Examples.TeleType_<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m6886DB5D83361607B72BBCCB7D484B9C0BFE1981 (void);
// 0x0000022A System.Boolean TMPro.Examples.TeleType_<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m1CD1306C9E074D3F941AC906A48D3CA97C148774 (void);
// 0x0000022B System.Object TMPro.Examples.TeleType_<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE50AC5FA9F27773C51DD3E4188A748BA0A513F8A (void);
// 0x0000022C System.Void TMPro.Examples.TeleType_<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m5D6EE5C4B2C20A433129D8BFD13DFC82681346A2 (void);
// 0x0000022D System.Object TMPro.Examples.TeleType_<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m1FA5600514131056D8198F8442F37A4A22A9F065 (void);
// 0x0000022E System.Void TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_m48510711FC78DFEA9CF4603E1E75F4DF7C5F1489 (void);
// 0x0000022F System.Void TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m5B88486625A74566DF3FC7BFB4CE327A58C57ED4 (void);
// 0x00000230 System.Boolean TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_mE45076151810A7C1F83802B7754DE92E812EABAB (void);
// 0x00000231 System.Object TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C330834C7113C8468CC1A09417B7C521CAE833B (void);
// 0x00000232 System.Void TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_m25B719FFD0CAB1DFF2853FF47A4EE2032176E287 (void);
// 0x00000233 System.Object TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9A540E1B18E93F749F4BFD4C8597AEC9F2C199F7 (void);
// 0x00000234 System.Void TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_mDF41BA5FE3D53FEC3CB8214FCA7853A1142DE70C (void);
// 0x00000235 System.Void TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m46827499CD3657AF468926B6302D2340ED975965 (void);
// 0x00000236 System.Boolean TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_mE0CB1189CFAD7F7B3E74438A4528D9BFAABB48DE (void);
// 0x00000237 System.Object TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4A3E941DF6C67BC9ACEFEAA09D11167B3F3A38EC (void);
// 0x00000238 System.Void TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_m967B427886233CDB5DFFEA323F02A89CE9330CC8 (void);
// 0x00000239 System.Object TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m0C1B2941BEC04593993127F6D9DCDBA6FAE7CC20 (void);
// 0x0000023A System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12__ctor_m13B7271203EDC80E649C1CE40F09A93BDA2633DF (void);
// 0x0000023B System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_IDisposable_Dispose_m3D1611AA38746EF0827F5260DADCC361DD56DF0C (void);
// 0x0000023C System.Boolean TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_MoveNext_m03111B7039F928512A7A53F8DA9C04671AA8D7EE (void);
// 0x0000023D System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55B08E77035437C2B75332F29214C33214417414 (void);
// 0x0000023E System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_Reset_m6737F2D695AB295240F15C5B0B4F24A59106BFDA (void);
// 0x0000023F System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_get_Current_m6BD4D2442BDDDFB6859CFE646182580A0A1E130A (void);
// 0x00000240 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__13__ctor_m7E4C3B87E56A7B23D725D653E52ADE02554EAE3E (void);
// 0x00000241 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_IDisposable_Dispose_m485A7C4CF3496858A72CBA647B29BC610F39FE39 (void);
// 0x00000242 System.Boolean TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_MoveNext_m6A3C88B1149D12B58E6E580BC04622F553ED1424 (void);
// 0x00000243 System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2EE54B2AC8AAE44BACF8EE8954A6D824045CFC55 (void);
// 0x00000244 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_Reset_m8524C9700DEF2DE7A28BBFDB938FE159985E86EE (void);
// 0x00000245 System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_get_Current_m2293C6A327D4D1CCC1ACBC90DBE00DC1C6F39EBE (void);
// 0x00000246 System.Void TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m3D7543ED636AFCD2C59E834668568DB2A4005F6A (void);
// 0x00000247 System.Void TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m3F7092E831D4CFDACC5B6254958DFEC2D313D0EE (void);
// 0x00000248 System.Boolean TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_mC0D42DAE0A614F2B91AF1F9A2F8C0AF471CA0AE4 (void);
// 0x00000249 System.Object TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6597EE639379454CADA3823A1B955FEFBAF894BD (void);
// 0x0000024A System.Void TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m9943586181F26EEE58A42138CE0489DDF07EA359 (void);
// 0x0000024B System.Object TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m2D4E4AA5EEB4F07F283E61018685199A4C2D56BD (void);
// 0x0000024C System.Void TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_mC74A801C40038DA74D856FACFBAD12F3BC3E11E7 (void);
// 0x0000024D System.Void TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m07C61223FC885322B6066E81CB130879661D5A72 (void);
// 0x0000024E System.Boolean TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m04CD6FB321DE3AD8D5890766C1F2CAAE4112EDF2 (void);
// 0x0000024F System.Object TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35F5F559A34D3F9BE1E5DD636FEAF517164A6B07 (void);
// 0x00000250 System.Void TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m325AE901312C158848B79B302EBA7BE847C93D49 (void);
// 0x00000251 System.Object TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_mA688DA41E2C04FF9774F607794C114057FA055C6 (void);
// 0x00000252 System.Void TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m89953000A887F8C0931B0E98B484FBAAC37748C5 (void);
// 0x00000253 System.Void TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m9AFBB2A87D38A1358F9EB09D617075D72DEED19B (void);
// 0x00000254 System.Boolean TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m9F607EF7DBDFFC4FB2307B0EC4C7F33EEE63BBE8 (void);
// 0x00000255 System.Object TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF47F7A3AB51F9BC1B8F74E10FD82B29C1B223DCD (void);
// 0x00000256 System.Void TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m99B988724C4F53F7F8334C5F633C8B1603185ADD (void);
// 0x00000257 System.Object TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_mE9127628FC1726149DA7C8FE95A7D4CFB1EE1655 (void);
// 0x00000258 System.Void TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_mBA04B89258FA2EF09266E1766AB0B815E521897A (void);
// 0x00000259 System.Void TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mE001B767DE85B4B3B86A0C080B9FC00381340A1C (void);
// 0x0000025A System.Boolean TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m611487BEE2BB82A9BFF5EA2157BDCA610F87876D (void);
// 0x0000025B System.Object TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A54A1BF63433F860822B43ABA9FAC6A4124409C (void);
// 0x0000025C System.Void TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m48F09B740CBEEC27459498932572D2869A1A4CBE (void);
// 0x0000025D System.Object TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mA915AC55E6DEE343235545FC1FE6F6CA5611DF3C (void);
// 0x0000025E System.Void TMPro.Examples.VertexZoom_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m1C2F2204ADD6E4BA14E14CF255F520B7E2464941 (void);
// 0x0000025F System.Int32 TMPro.Examples.VertexZoom_<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m673C7031DB1882DEEFB53F179E3C2FB13FB6CA5A (void);
// 0x00000260 System.Void TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m2F5D29C1CA797C0BCEC16C8B5D96D1CF5B07F6F3 (void);
// 0x00000261 System.Void TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m1A43A8EA2FB689EE2B39D8A624580594374905B9 (void);
// 0x00000262 System.Boolean TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_mCA826F12F72BDBB79F9B50DC9CBC6E7F80B2110F (void);
// 0x00000263 System.Object TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF0E754E9557C03F892EFA19C0307AECD6BA8C4D (void);
// 0x00000264 System.Void TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m002A7C6C8AE61BE6CD6FD0B2173C75DBF47BCC56 (void);
// 0x00000265 System.Object TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mE91B03C99FCCBC8ED4E37649C5364E83D047B053 (void);
// 0x00000266 System.Void TMPro.Examples.WarpTextExample_<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m845C9410F3856EF25585F59C425200EEFCEFB3C0 (void);
// 0x00000267 System.Void TMPro.Examples.WarpTextExample_<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m63AC2AE8BC0FCF89812A33CAF150E9D1B56BAE6A (void);
// 0x00000268 System.Boolean TMPro.Examples.WarpTextExample_<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_m98D3E999A69E233C7AD5F357A0D0623D731DCDAA (void);
// 0x00000269 System.Object TMPro.Examples.WarpTextExample_<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3971C0D86C5903812972245A6F872D101ACB5189 (void);
// 0x0000026A System.Void TMPro.Examples.WarpTextExample_<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_mF17827612AC7C91DE879114D1D6428450B9504D0 (void);
// 0x0000026B System.Object TMPro.Examples.WarpTextExample_<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m98A13A358D4E71D9612F68731A7AE11A3DD080DE (void);
// 0x0000026C System.Void SplineMesh.ExampleTrack_<>c::.cctor()
extern void U3CU3Ec__cctor_m7C968439A45D026920C6C5D5EA65AAEA83606E34 (void);
// 0x0000026D System.Void SplineMesh.ExampleTrack_<>c::.ctor()
extern void U3CU3Ec__ctor_m696942DE4DFB5A7467CB313B1CD8799AE0305B7C (void);
// 0x0000026E UnityEngine.GameObject SplineMesh.ExampleTrack_<>c::<CreateMeshes>b__8_0(UnityEngine.Transform)
extern void U3CU3Ec_U3CCreateMeshesU3Eb__8_0_m4D637995C58AD7110AD8C8B21CD55B89D2E2BC7E (void);
// 0x0000026F System.Void SplineMesh.ExtrusionSegment_Vertex::.ctor(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void Vertex__ctor_m3FB3A2AD740159C83FD82DB3DCFF580697881CEA (void);
// 0x00000270 System.Void SplineMesh.ExtrusionSegment_Vertex::.ctor(SplineMesh.ExtrusionSegment_Vertex)
extern void Vertex__ctor_mA4F00945EA013CC81D49C6E27F703ED1C10F3D7A (void);
// 0x00000271 System.Void SplineMesh.ExtrusionSegment_<>c::.cctor()
extern void U3CU3Ec__cctor_mF54855846ADBDCD8620E152C3BA4B1AFFD5E9E0F (void);
// 0x00000272 System.Void SplineMesh.ExtrusionSegment_<>c::.ctor()
extern void U3CU3Ec__ctor_m787A4B7E3084867184B5514748E2BD62DCA9205A (void);
// 0x00000273 UnityEngine.Vector3 SplineMesh.ExtrusionSegment_<>c::<Compute>b__31_0(SplineMesh.MeshVertex)
extern void U3CU3Ec_U3CComputeU3Eb__31_0_mC45343E4EF8EC7D90B72F38CA404AB86D11A0E84 (void);
// 0x00000274 UnityEngine.Vector3 SplineMesh.ExtrusionSegment_<>c::<Compute>b__31_1(SplineMesh.MeshVertex)
extern void U3CU3Ec_U3CComputeU3Eb__31_1_mC03DC6F65E6A3B445E3328079587C23076951BBE (void);
// 0x00000275 UnityEngine.Vector2 SplineMesh.ExtrusionSegment_<>c::<Compute>b__31_2(SplineMesh.MeshVertex)
extern void U3CU3Ec_U3CComputeU3Eb__31_2_mD24885FCBDCAC928EB1C5F5DDEB962D25848A1BA (void);
// 0x00000276 System.Void SplineMesh.MeshBender_<>c::.cctor()
extern void U3CU3Ec__cctor_m6DBE3096D5D93F61ED3EFABD0E0928E35ECE6352 (void);
// 0x00000277 System.Void SplineMesh.MeshBender_<>c::.ctor()
extern void U3CU3Ec__ctor_m131D8F81BC128201C8B49F1FA539D2204F099C45 (void);
// 0x00000278 UnityEngine.Vector3 SplineMesh.MeshBender_<>c::<FillOnce>b__25_0(SplineMesh.MeshVertex)
extern void U3CU3Ec_U3CFillOnceU3Eb__25_0_mC0FC995AB7363510A1CE19C2FAAD8F629C2D0970 (void);
// 0x00000279 UnityEngine.Vector3 SplineMesh.MeshBender_<>c::<FillOnce>b__25_1(SplineMesh.MeshVertex)
extern void U3CU3Ec_U3CFillOnceU3Eb__25_1_mDBDE40CEFF9892DCAB0A4F8613373DCD46153AFC (void);
// 0x0000027A UnityEngine.Vector3 SplineMesh.MeshBender_<>c::<FillRepeat>b__26_0(SplineMesh.MeshVertex)
extern void U3CU3Ec_U3CFillRepeatU3Eb__26_0_m2E5A74EB91BF04A38755EAF1E9D7FF73C2598079 (void);
// 0x0000027B UnityEngine.Vector3 SplineMesh.MeshBender_<>c::<FillRepeat>b__26_1(SplineMesh.MeshVertex)
extern void U3CU3Ec_U3CFillRepeatU3Eb__26_1_m414F9F219164338E88CCC5F249DA887A0BB71639 (void);
// 0x0000027C UnityEngine.Vector3 SplineMesh.MeshBender_<>c::<FillStretch>b__27_0(SplineMesh.MeshVertex)
extern void U3CU3Ec_U3CFillStretchU3Eb__27_0_m044AE6F02BEB6874F2E161AC4B528E6BC9B0F26C (void);
// 0x0000027D UnityEngine.Vector3 SplineMesh.MeshBender_<>c::<FillStretch>b__27_1(SplineMesh.MeshVertex)
extern void U3CU3Ec_U3CFillStretchU3Eb__27_1_mE62BEE4B7056B3A23AA994D2CE7DECBC81E9521C (void);
// 0x0000027E System.Void SplineMesh.SplineMeshTiling_<>c::.cctor()
extern void U3CU3Ec__cctor_m6D4552D449AF496E6984E9F5C4BC8F85F258ADC9 (void);
// 0x0000027F System.Void SplineMesh.SplineMeshTiling_<>c::.ctor()
extern void U3CU3Ec__ctor_mF27C1536AD315F65371F655EF08330E1FC806955 (void);
// 0x00000280 UnityEngine.GameObject SplineMesh.SplineMeshTiling_<>c::<CreateMeshes>b__16_0(UnityEngine.Transform)
extern void U3CU3Ec_U3CCreateMeshesU3Eb__16_0_mC746B56628831C50327E26DC568649EB0B5DC799 (void);
// 0x00000281 System.Void Service.Rest.RestWebClient_<HttpGet>d__1::.ctor(System.Int32)
extern void U3CHttpGetU3Ed__1__ctor_m57D54108B5D6778C7D0C398D073C8F6EF2CC4DF7 (void);
// 0x00000282 System.Void Service.Rest.RestWebClient_<HttpGet>d__1::System.IDisposable.Dispose()
extern void U3CHttpGetU3Ed__1_System_IDisposable_Dispose_m49664829AB4E380552E7D5EE0BD65E3DC313A210 (void);
// 0x00000283 System.Boolean Service.Rest.RestWebClient_<HttpGet>d__1::MoveNext()
extern void U3CHttpGetU3Ed__1_MoveNext_mF39BBEA2F4457E8671DC8E306899F97867FA0ABA (void);
// 0x00000284 System.Void Service.Rest.RestWebClient_<HttpGet>d__1::<>m__Finally1()
extern void U3CHttpGetU3Ed__1_U3CU3Em__Finally1_m07935C8658851E12B3A1DC2A92641D5C47273309 (void);
// 0x00000285 System.Object Service.Rest.RestWebClient_<HttpGet>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHttpGetU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC8AFD58E65080D05C735D4CBA1636B05D287664B (void);
// 0x00000286 System.Void Service.Rest.RestWebClient_<HttpGet>d__1::System.Collections.IEnumerator.Reset()
extern void U3CHttpGetU3Ed__1_System_Collections_IEnumerator_Reset_m4CED69761DFE7BDFC1651467750D5D596B294E4D (void);
// 0x00000287 System.Object Service.Rest.RestWebClient_<HttpGet>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CHttpGetU3Ed__1_System_Collections_IEnumerator_get_Current_m2C32722CD63DCFEF8DFCCE0BF179C5F14045F306 (void);
// 0x00000288 System.Void Service.Rest.RestWebClient_<HttpDelete>d__2::.ctor(System.Int32)
extern void U3CHttpDeleteU3Ed__2__ctor_mDF4467E0D713CBC9F6B586FFC01D56D55BB86F2A (void);
// 0x00000289 System.Void Service.Rest.RestWebClient_<HttpDelete>d__2::System.IDisposable.Dispose()
extern void U3CHttpDeleteU3Ed__2_System_IDisposable_Dispose_m388748DB132AD4CB3126992A5BC9FD3A7433B704 (void);
// 0x0000028A System.Boolean Service.Rest.RestWebClient_<HttpDelete>d__2::MoveNext()
extern void U3CHttpDeleteU3Ed__2_MoveNext_mA32A60E0E49984D7E37CF1B484DDE8AD02E54918 (void);
// 0x0000028B System.Void Service.Rest.RestWebClient_<HttpDelete>d__2::<>m__Finally1()
extern void U3CHttpDeleteU3Ed__2_U3CU3Em__Finally1_mBF6925D255EA08802C17D9C47FA1F704EC0976EF (void);
// 0x0000028C System.Object Service.Rest.RestWebClient_<HttpDelete>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHttpDeleteU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m63C103756875EAD5C410F846345E98A39F33A9A6 (void);
// 0x0000028D System.Void Service.Rest.RestWebClient_<HttpDelete>d__2::System.Collections.IEnumerator.Reset()
extern void U3CHttpDeleteU3Ed__2_System_Collections_IEnumerator_Reset_mB26CB445F177724DD6988E41F4DDFFF08F7A4732 (void);
// 0x0000028E System.Object Service.Rest.RestWebClient_<HttpDelete>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CHttpDeleteU3Ed__2_System_Collections_IEnumerator_get_Current_m07C4AA79AD28CC69714DC58818BE0A361409FCF7 (void);
// 0x0000028F System.Void Service.Rest.RestWebClient_<HttpPost>d__3::.ctor(System.Int32)
extern void U3CHttpPostU3Ed__3__ctor_m7D509067A83191FFE91CF09B515278678A88BE53 (void);
// 0x00000290 System.Void Service.Rest.RestWebClient_<HttpPost>d__3::System.IDisposable.Dispose()
extern void U3CHttpPostU3Ed__3_System_IDisposable_Dispose_mADE1BD09F179274CAD120B0BD3E73F8F454FD53A (void);
// 0x00000291 System.Boolean Service.Rest.RestWebClient_<HttpPost>d__3::MoveNext()
extern void U3CHttpPostU3Ed__3_MoveNext_m662DA8DC365F0A396A19B5CF0CCD6FA12E67E6D0 (void);
// 0x00000292 System.Void Service.Rest.RestWebClient_<HttpPost>d__3::<>m__Finally1()
extern void U3CHttpPostU3Ed__3_U3CU3Em__Finally1_mB9CFF4DFEC8475E6454F0DF85D7740271C03D5A6 (void);
// 0x00000293 System.Object Service.Rest.RestWebClient_<HttpPost>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHttpPostU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m91767642FBE9F9692217E14DEE61ACE8537D8418 (void);
// 0x00000294 System.Void Service.Rest.RestWebClient_<HttpPost>d__3::System.Collections.IEnumerator.Reset()
extern void U3CHttpPostU3Ed__3_System_Collections_IEnumerator_Reset_m2C7B761948466DF4646737A6CD67C509BD3AEB2F (void);
// 0x00000295 System.Object Service.Rest.RestWebClient_<HttpPost>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CHttpPostU3Ed__3_System_Collections_IEnumerator_get_Current_mA5D0E1D452C35DE206D6A53BA619A2A7FA4958F9 (void);
// 0x00000296 System.Void Service.Rest.RestWebClient_<HttpPut>d__4::.ctor(System.Int32)
extern void U3CHttpPutU3Ed__4__ctor_m2F50B7D77CE5A37976C5D63CC7D15E738F89B625 (void);
// 0x00000297 System.Void Service.Rest.RestWebClient_<HttpPut>d__4::System.IDisposable.Dispose()
extern void U3CHttpPutU3Ed__4_System_IDisposable_Dispose_m24150DE19A56E505C30DE38145F514B83EE8382B (void);
// 0x00000298 System.Boolean Service.Rest.RestWebClient_<HttpPut>d__4::MoveNext()
extern void U3CHttpPutU3Ed__4_MoveNext_mBD244ABDD5CCFC7D808DF6B01FA4883558A49CDF (void);
// 0x00000299 System.Void Service.Rest.RestWebClient_<HttpPut>d__4::<>m__Finally1()
extern void U3CHttpPutU3Ed__4_U3CU3Em__Finally1_mE001D9F071D634B01C6ED5CB190E709F0C82F008 (void);
// 0x0000029A System.Object Service.Rest.RestWebClient_<HttpPut>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHttpPutU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m699CB4EE20DC7442553E05DA5FD6C1B6B4F22546 (void);
// 0x0000029B System.Void Service.Rest.RestWebClient_<HttpPut>d__4::System.Collections.IEnumerator.Reset()
extern void U3CHttpPutU3Ed__4_System_Collections_IEnumerator_Reset_mB2AD79998C84296E056B941A8F163FBE6AFF20B7 (void);
// 0x0000029C System.Object Service.Rest.RestWebClient_<HttpPut>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CHttpPutU3Ed__4_System_Collections_IEnumerator_get_Current_m8B0E55472EE5B3E056C2C9AD5421F472C16E0333 (void);
// 0x0000029D System.Void Service.Rest.RestWebClient_<HttpHead>d__5::.ctor(System.Int32)
extern void U3CHttpHeadU3Ed__5__ctor_mA4D217EC377A3600BD1D0FE57A96F329DE8681C3 (void);
// 0x0000029E System.Void Service.Rest.RestWebClient_<HttpHead>d__5::System.IDisposable.Dispose()
extern void U3CHttpHeadU3Ed__5_System_IDisposable_Dispose_mAE0A40AF567CEBDB8CB083570763C35123508B8A (void);
// 0x0000029F System.Boolean Service.Rest.RestWebClient_<HttpHead>d__5::MoveNext()
extern void U3CHttpHeadU3Ed__5_MoveNext_m2C4BFA52CB068854B947278F6A98C98D8E05F7DA (void);
// 0x000002A0 System.Void Service.Rest.RestWebClient_<HttpHead>d__5::<>m__Finally1()
extern void U3CHttpHeadU3Ed__5_U3CU3Em__Finally1_mE5D51E1314AD7FE24C8472CEDAE4213DD0A2ACA0 (void);
// 0x000002A1 System.Object Service.Rest.RestWebClient_<HttpHead>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHttpHeadU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m18F1FD8934F2A5A96ECA978FD0DF490990CF5CC8 (void);
// 0x000002A2 System.Void Service.Rest.RestWebClient_<HttpHead>d__5::System.Collections.IEnumerator.Reset()
extern void U3CHttpHeadU3Ed__5_System_Collections_IEnumerator_Reset_m47B2858FEACBA7688A514E223DABE2269C0183D4 (void);
// 0x000002A3 System.Object Service.Rest.RestWebClient_<HttpHead>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CHttpHeadU3Ed__5_System_Collections_IEnumerator_get_Current_m4D321DA89DA0BAAF06ED049F36ACB4429EB3DDA6 (void);
// 0x000002A4 System.Void Scenes.ChooseArmatures_<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_mCC5BC6D39808F7814535121AEE5ABCE05119DFD7 (void);
// 0x000002A5 System.Boolean Scenes.ChooseArmatures_<>c__DisplayClass14_0::<ArmatureOneDropdownValueChanged>b__0(Service.Models.ArmatureData)
extern void U3CU3Ec__DisplayClass14_0_U3CArmatureOneDropdownValueChangedU3Eb__0_m1FF82FDB237DBE3C20F37F7A011416A28E3821C8 (void);
// 0x000002A6 System.Void Scenes.ChooseArmatures_<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_mCC735845FF56CC8F219F0356E6EA525EDCF06620 (void);
// 0x000002A7 System.Boolean Scenes.ChooseArmatures_<>c__DisplayClass15_0::<ArmatureTwoDropdownValueChanged>b__0(Service.Models.ArmatureData)
extern void U3CU3Ec__DisplayClass15_0_U3CArmatureTwoDropdownValueChangedU3Eb__0_m87A76DA791982B20F4B17FD988BFDE693CA6C9BB (void);
// 0x000002A8 System.Void Scenes.ChooseArmatures_<>c::.cctor()
extern void U3CU3Ec__cctor_mCD32F4052D5DCA2488F83ADE1F77EA74282E7484 (void);
// 0x000002A9 System.Void Scenes.ChooseArmatures_<>c::.ctor()
extern void U3CU3Ec__ctor_m0704454B4060DF5148CB28AABCD257ED778CE2C1 (void);
// 0x000002AA System.Int32 Scenes.ChooseArmatures_<>c::<FillDropDownWithData>b__17_0(Service.Models.ArmatureData,Service.Models.ArmatureData)
extern void U3CU3Ec_U3CFillDropDownWithDataU3Eb__17_0_m323B51AAC9BACE24415F71E0EBEAB644A90A9A36 (void);
// 0x000002AB System.String Scenes.ChooseArmatures_<>c::<FillDropDownWithData>b__17_1(Service.Models.ArmatureData)
extern void U3CU3Ec_U3CFillDropDownWithDataU3Eb__17_1_mFAAD20D0F7F748329A597385D7AA4612ECDE42FE (void);
// 0x000002AC System.Void Scenes.ChoosePipe_<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m06A70BF29D05DCA5B848DFD8E9FE7DCA0F5F8C59 (void);
// 0x000002AD System.Boolean Scenes.ChoosePipe_<>c__DisplayClass12_0::<PipeTypeDropdownValueChanged>b__0(Service.Models.PipeData)
extern void U3CU3Ec__DisplayClass12_0_U3CPipeTypeDropdownValueChangedU3Eb__0_m4A677BE2097122742A6F3C6E15510D541417931E (void);
// 0x000002AE System.Void Scenes.ChoosePipe_<>c::.cctor()
extern void U3CU3Ec__cctor_m8738A4DA0168457CAFD0FDD08BC5384F67104ABC (void);
// 0x000002AF System.Void Scenes.ChoosePipe_<>c::.ctor()
extern void U3CU3Ec__ctor_mADF95EB19A64F136EFA3469B04094B38A90BD2AA (void);
// 0x000002B0 System.String Scenes.ChoosePipe_<>c::<OnRequestComplete>b__13_1(Service.Models.PipeData)
extern void U3CU3Ec_U3COnRequestCompleteU3Eb__13_1_m0744AF2BE14B54C32D3D8347D4A99BAB61AE51A9 (void);
// 0x000002B1 System.Int32 Scenes.ChoosePipe_<>c::<OnRequestComplete>b__13_2(Service.Models.PipeData,Service.Models.PipeData)
extern void U3CU3Ec_U3COnRequestCompleteU3Eb__13_2_m6E03B7C3DE1FD8F94CA23CF8983AAA8E97926D26 (void);
// 0x000002B2 System.String Scenes.ChoosePipe_<>c::<OnRequestComplete>b__13_3(Service.Models.PipeData)
extern void U3CU3Ec_U3COnRequestCompleteU3Eb__13_3_m7416AE4D4F7A69CE6EEEC838D7D2D264E78B318B (void);
// 0x000002B3 System.Void Scenes.PlaceObjectsInAR.Pipe.PlacePipe_<>c::.cctor()
extern void U3CU3Ec__cctor_mD4F25E26E4EB3A33C54B1549C95592EF319A9560 (void);
// 0x000002B4 System.Void Scenes.PlaceObjectsInAR.Pipe.PlacePipe_<>c::.ctor()
extern void U3CU3Ec__ctor_mEFDF2424AFF97BF99B58BBBF0427AA4C80CB0C6F (void);
// 0x000002B5 UnityEngine.Vector3 Scenes.PlaceObjectsInAR.Pipe.PlacePipe_<>c::<DrawPipe>b__24_0(UnityEngine.GameObject)
extern void U3CU3Ec_U3CDrawPipeU3Eb__24_0_mC39F2087C3CF55D79D1C51A1C5FD467FFAD6EDDB (void);
// 0x000002B6 System.Void Loading.SceneLoader_LoadingMonoBehaviour::.ctor()
extern void LoadingMonoBehaviour__ctor_m3DCBD199B32CBBE0610F74E6C22643BFD0DF2539 (void);
// 0x000002B7 System.Void Loading.SceneLoader_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m74FAA460B03BB06E2961735B3435DB380F13EB44 (void);
// 0x000002B8 System.Void Loading.SceneLoader_<>c__DisplayClass4_0::<Load>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CLoadU3Eb__0_mA2971131149CCFEC0E01CA07A6A512C94FEA4829 (void);
// 0x000002B9 System.Void Loading.SceneLoader_<SetActive>d__6::.ctor(System.Int32)
extern void U3CSetActiveU3Ed__6__ctor_m81ACD5EE7022F82CFA0A0FD0956421B57B9C4B7D (void);
// 0x000002BA System.Void Loading.SceneLoader_<SetActive>d__6::System.IDisposable.Dispose()
extern void U3CSetActiveU3Ed__6_System_IDisposable_Dispose_mAEB6CF32BD97C3042986C0D647F2B341F62DB8AC (void);
// 0x000002BB System.Boolean Loading.SceneLoader_<SetActive>d__6::MoveNext()
extern void U3CSetActiveU3Ed__6_MoveNext_m37B35356F15F93B6AF4692600E53BDF039F22F80 (void);
// 0x000002BC System.Object Loading.SceneLoader_<SetActive>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetActiveU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m38A6639AA4F908C4367B876D0C9CBF047E1B48DD (void);
// 0x000002BD System.Void Loading.SceneLoader_<SetActive>d__6::System.Collections.IEnumerator.Reset()
extern void U3CSetActiveU3Ed__6_System_Collections_IEnumerator_Reset_m02B300ACA6ED1F151062AB5C3CFB6F09FC32FF86 (void);
// 0x000002BE System.Object Loading.SceneLoader_<SetActive>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CSetActiveU3Ed__6_System_Collections_IEnumerator_get_Current_m7B67385E706B00A1FEB3BD44824288448E2D071D (void);
// 0x000002BF System.Void Loading.SceneLoader_<LoadSceneAsync>d__8::.ctor(System.Int32)
extern void U3CLoadSceneAsyncU3Ed__8__ctor_m6047B94C7A8011CE80C28FE67F1DE1FDD6B13D7F (void);
// 0x000002C0 System.Void Loading.SceneLoader_<LoadSceneAsync>d__8::System.IDisposable.Dispose()
extern void U3CLoadSceneAsyncU3Ed__8_System_IDisposable_Dispose_m91FFE31DDB26C503FD5790094CB3F2200EDACD93 (void);
// 0x000002C1 System.Boolean Loading.SceneLoader_<LoadSceneAsync>d__8::MoveNext()
extern void U3CLoadSceneAsyncU3Ed__8_MoveNext_m1DED49F10A337A0C12435EBFD8560931083EF52C (void);
// 0x000002C2 System.Object Loading.SceneLoader_<LoadSceneAsync>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadSceneAsyncU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7CC1BC0C97A47A7F4E3ABA728FA9DC6FB098DD20 (void);
// 0x000002C3 System.Void Loading.SceneLoader_<LoadSceneAsync>d__8::System.Collections.IEnumerator.Reset()
extern void U3CLoadSceneAsyncU3Ed__8_System_Collections_IEnumerator_Reset_m8CF831F3429473C0CD2195F27B02F5A8FE2B3129 (void);
// 0x000002C4 System.Object Loading.SceneLoader_<LoadSceneAsync>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CLoadSceneAsyncU3Ed__8_System_Collections_IEnumerator_get_Current_m885A63CBCB0B2254290645F2B81F05EACC950C88 (void);
// 0x000002C5 System.Void Helper.FindArmatureModel_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m7BD16E395EE70B11119E37B755EB1FB45B438079 (void);
// 0x000002C6 System.Boolean Helper.FindArmatureModel_<>c__DisplayClass4_0::<GetArmatureDependingOnName>b__2(<>f__AnonymousType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String[]>,System.String>)
extern void U3CU3Ec__DisplayClass4_0_U3CGetArmatureDependingOnNameU3Eb__2_m57FCBDAB61AAEBE628247A8F021B4AA45F33522B (void);
// 0x000002C7 System.Void Helper.FindArmatureModel_<>c::.cctor()
extern void U3CU3Ec__cctor_mF7F04136E6109C63ACE6A52FF0DA47226B80ABFC (void);
// 0x000002C8 System.Void Helper.FindArmatureModel_<>c::.ctor()
extern void U3CU3Ec__ctor_m2EA6A9C7CA87FC516C96AE04E9FF589D7BB9FF90 (void);
// 0x000002C9 System.Collections.Generic.IEnumerable`1<System.String> Helper.FindArmatureModel_<>c::<GetArmatureDependingOnName>b__4_0(System.Collections.Generic.KeyValuePair`2<System.String,System.String[]>)
extern void U3CU3Ec_U3CGetArmatureDependingOnNameU3Eb__4_0_mE89C1AFFC0964CF1FB52A96F0DF92B15C5442FB4 (void);
// 0x000002CA <>f__AnonymousType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String[]>,System.String> Helper.FindArmatureModel_<>c::<GetArmatureDependingOnName>b__4_1(System.Collections.Generic.KeyValuePair`2<System.String,System.String[]>,System.String)
extern void U3CU3Ec_U3CGetArmatureDependingOnNameU3Eb__4_1_mDE313AE457A61CB8075BAEF4FFD84E652C7454F6 (void);
// 0x000002CB System.Collections.Generic.KeyValuePair`2<System.String,System.String[]> Helper.FindArmatureModel_<>c::<GetArmatureDependingOnName>b__4_3(<>f__AnonymousType0`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String[]>,System.String>)
extern void U3CU3Ec_U3CGetArmatureDependingOnNameU3Eb__4_3_m90F7645184DD9284517BB089D7481A8B06561462 (void);
static Il2CppMethodPointer s_methodPointers[715] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ARFeatheredPlaneMeshVisualizer_get_featheringWidth_mD474743D2E8E4CE3078FCD70A52B70111EC99500,
	ARFeatheredPlaneMeshVisualizer_set_featheringWidth_m1974473A6C2BB2AED083691C8079E6F9A83398EC,
	ARFeatheredPlaneMeshVisualizer_Awake_mBFDBB9E8B393BD7BA784845A0C7692848E9C61E2,
	ARFeatheredPlaneMeshVisualizer_OnEnable_m3A77938D2FE2C6A653A7E13B90DB6C9DEEB06A9A,
	ARFeatheredPlaneMeshVisualizer_OnDisable_m9DE1EB0F00D7F7AC754CE4633569BB8C8687C67A,
	ARFeatheredPlaneMeshVisualizer_ARPlane_boundaryUpdated_mC88C42F35A3B243C83E9D24DCC9545F7E9E6BA28,
	ARFeatheredPlaneMeshVisualizer_GenerateBoundaryUVs_mCEA3823DDAED557BA0B92214D15AB25EC75065D8,
	ARFeatheredPlaneMeshVisualizer__ctor_mA9417DD97EC611603602EAE0302A80E71CF93890,
	ARFeatheredPlaneMeshVisualizer__cctor_mC697317BB4FB496A10AFA8C2E73E27AF8A5538A1,
	ReferencePointCreator_get_referencePointPrefab_m31FB3255AF4A0E9C6E7A9B49ADF62894D1172F36,
	ReferencePointCreator_set_referencePointPrefab_m55AB059B84BF9130A63CEA5B25432DF5575E556D,
	ReferencePointCreator_RemoveAllReferencePoints_mEF4D5C72B358090E4652FA705548C08851E2CD9B,
	ReferencePointCreator_Awake_mBA33AA2F7ADB2A8DA9B3D2BC744C57CE87BEA02F,
	ReferencePointCreator_Update_mAB2F5A406EAE54CF3D8378109674BA0DBE1CC81B,
	ReferencePointCreator__ctor_mD3983A54F093C032DB88E8BC396BBD0A060C210F,
	ReferencePointCreator__cctor_mB6B005CB03A919E9A5C764505CCEA0BB17F82067,
	ChatController_OnEnable_m168B1E78BFA288F42D4AE0A8F1424B8D68B07993,
	ChatController_OnDisable_m49C4A6501BCC216F924B3C37F243D1B5B54A69FF,
	ChatController_AddToChatOutput_m5E6DF0E37CB2E9FBBEACCB6EEE6452AB14BBE94C,
	ChatController__ctor_m2C7AAB67386BA2DC6742585988B914B3FAB30013,
	DropdownSample_OnButtonClick_mEAC0F8D33D13DE84DCEA5D99E5162E4D28F6B54D,
	DropdownSample__ctor_m901DB807D4DFA75581306389B7A21072E98E72A0,
	EnvMapAnimator_Awake_mDDD10A405C7152BEFA0ECEA0DCBD061B47C5802E,
	EnvMapAnimator_Start_m630E0BFAB4D647BC38B99A70F522EF80D25F3C71,
	EnvMapAnimator__ctor_m2A8770DA2E27EC52F6A6F704831B732638C76E84,
	TMP_DigitValidator_Validate_mEC7653F2228D8AA66F69D6B3539ED342AEE57691,
	TMP_DigitValidator__ctor_m4E1C1BEB96F76F2EE55E6FEC45D05F2AAC5DF325,
	TMP_PhoneNumberValidator_Validate_mBE0169BE01459AA37111A289EC422DDB0D5E3479,
	TMP_PhoneNumberValidator__ctor_mBF81DE006E19E49DAC3AFF685F8AF268A2FD0FFB,
	TMP_TextEventHandler_get_onCharacterSelection_mF70DBE3FF43B3D6E64053D37A2FADF802533E1FF,
	TMP_TextEventHandler_set_onCharacterSelection_mDEC285B6A284CC2EC9729E3DC16E81A182890D21,
	TMP_TextEventHandler_get_onSpriteSelection_m395603314F8CD073897DCAB5513270C6ADD94BF4,
	TMP_TextEventHandler_set_onSpriteSelection_m3D4E17778B0E3CC987A3EF74515E83CE39E3C094,
	TMP_TextEventHandler_get_onWordSelection_m415F4479934B1739658356B47DF4C2E90496AE2E,
	TMP_TextEventHandler_set_onWordSelection_m2EDD56E0024792DCE7F068228B4CA5A897808F4E,
	TMP_TextEventHandler_get_onLineSelection_m8E724700CC5DF1197B103F87156576A52F62AB2B,
	TMP_TextEventHandler_set_onLineSelection_m067512B3F057A225AF6DD251DD7E546FFF64CD93,
	TMP_TextEventHandler_get_onLinkSelection_m221527467F0606DD3561E0FB0D7678AA8329AD5D,
	TMP_TextEventHandler_set_onLinkSelection_mE3CE372F9FECD727FAB3B14D46439E0534EE8AA8,
	TMP_TextEventHandler_Awake_m67A37475531AC3EB75B43A640058AD52A605B8D9,
	TMP_TextEventHandler_LateUpdate_mB0ABBED08D5494DFFF85D9B56D4446D96DDBDDF5,
	TMP_TextEventHandler_OnPointerEnter_mE1CAF8C68C2356069FEB1AA1B53A56E24E5CE333,
	TMP_TextEventHandler_OnPointerExit_mB429546A32DCF6C8C64E703D07F9F1CDC697B009,
	TMP_TextEventHandler_SendOnCharacterSelection_mFBFC60A83107F26AA351246C10AB42CEB3A5A13C,
	TMP_TextEventHandler_SendOnSpriteSelection_mAB964EB5171AB07C48AC64E06C6BEC6A9C323E09,
	TMP_TextEventHandler_SendOnWordSelection_m3B76D7E79C65DB9D8E09EE834252C6E33C86D3AE,
	TMP_TextEventHandler_SendOnLineSelection_m9E9CAD5FA36FCA342A38EBD43E609A469E49F15F,
	TMP_TextEventHandler_SendOnLinkSelection_m1C55C664BB488E25AE746B99438EEDAE5B2B8DE8,
	TMP_TextEventHandler__ctor_m189A5951F5C0FA5FB1D0CFC461FAA1EBD7AED1AE,
	Benchmark01_Start_m20668FA5AD3945F18B5045459057C330E0B4D1F4,
	Benchmark01__ctor_m40EDCD3A3B6E8651A39C2220669A7689902C8B36,
	Benchmark01_UGUI_Start_mE8F5BC98EC6C16ECEBAD0FD78CD63E278B2DF215,
	Benchmark01_UGUI__ctor_m7F24B3D019827130B3D5F2D3E8C3FF23425F98BE,
	Benchmark02_Start_m3F848191079D3EF1E3B785830D74698325CA0BB7,
	Benchmark02__ctor_m3323414B806F63563E680918CC90EAF766A3D1AE,
	Benchmark03_Awake_m261B7F2CD25DC9E7144B2A2D167219A751AD9322,
	Benchmark03_Start_m649EFCC5BF0F199D102083583854DE87AC5EFBDD,
	Benchmark03__ctor_m90649FDE30CC915363C5B61AA19A7DE874FF18ED,
	Benchmark04_Start_mFDF88CB6DD4C5641A418DB08E105F9F62B897777,
	Benchmark04__ctor_mB07A2FD29BE4AFE284B47F2F610BDB7539F5A5DE,
	CameraController_Awake_m5E24687E6D82C0EBC4984D01B90769B8FD8C38B3,
	CameraController_Start_m257B81C6062A725785739AFE4C0DF84B8931EFB2,
	CameraController_LateUpdate_m9660F57BCF4F8C2154D19B6B40208466E414DAEB,
	CameraController_GetPlayerInput_m0B63EA708A63AF6852E099FD40F7C4E18793560A,
	CameraController__ctor_m8379776EEE21556D56845974B8C505AAD366B656,
	ObjectSpin_Awake_m2E5B2D7FA6FE2F3B5516BD829EDC5522187E6359,
	ObjectSpin_Update_mF8175B9157B852D3EC1BAF19D168858A8782BF0D,
	ObjectSpin__ctor_m1F951082C07A983F89779737E5A6071DD7BA67EB,
	ShaderPropAnimator_Awake_m44ACA60771EECABCB189FC78027D4ECD9726D31A,
	ShaderPropAnimator_Start_m57178B42FF0BB90ACA497EC1AA942CC3D4D54C32,
	ShaderPropAnimator_AnimateProperties_mB34C25C714FAEA4792465A981BAE46778C4F2409,
	ShaderPropAnimator__ctor_mDFAE260FD15CD3E704E86A25A57880A33B817BC6,
	SimpleScript_Start_m0238BE0F5DF0A15743D4D4B1B64C0A86505D1B76,
	SimpleScript_Update_mB92D578CAC3E0A0AFB055C7FEF47601C8822A0F8,
	SimpleScript__ctor_m0E919E8F3C12BAFF36B17E5692FCFA5AE602B2AA,
	SkewTextExample_Awake_mC70E117C1F921453D2F448CABA234FAA17A277ED,
	SkewTextExample_Start_mE2308836BF90B959ABE6064CD2DDDFAF224F0F4A,
	SkewTextExample_CopyAnimationCurve_m3CE7B666BEF4CFFE9EB110C8D57D9A5F6385720B,
	SkewTextExample_WarpText_m8B756AF1E1C065EEA486159E6C631A585B0C3461,
	SkewTextExample__ctor_m44F3CBD12A19C44A000D705FB4AB02E20432EC02,
	TMP_ExampleScript_01_Awake_mE2AAB8DF142D7BDB2C041CC7552A48745DBFDCFF,
	TMP_ExampleScript_01_Update_m1593A7650860FD2A478E10EA12A2601E918DD1EC,
	TMP_ExampleScript_01__ctor_m313B4F7ED747AD6979D8909858D0EF182C79BBC3,
	TMP_FrameRateCounter_Awake_m2540DCD733523BCBB1757724D8546AC3F1BEB16C,
	TMP_FrameRateCounter_Start_mEF10D80C419582C6944313FD100E2FD1C5AD1319,
	TMP_FrameRateCounter_Update_mF4798814F4F86850BB9248CA192EF5B65FA3A92B,
	TMP_FrameRateCounter_Set_FrameCounter_Position_m19C3C5E637FB3ED2B0869E7650A1C30A3302AF53,
	TMP_FrameRateCounter__ctor_m55E3726473BA4825AC0B7B7B7EA48D0C5CE8D646,
	TMP_TextEventCheck_OnEnable_mAFF9E7581B7B0C93A4A7D811C978FFCEC87B3784,
	TMP_TextEventCheck_OnDisable_m270DBB9CC93731104E851797D6BF55EACAE9158A,
	TMP_TextEventCheck_OnCharacterSelection_m4394BE3A0CA37D319AA10BE200A26CFD17EEAA8F,
	TMP_TextEventCheck_OnSpriteSelection_mCBF0B6754C607CA140C405FF5B681154AC861992,
	TMP_TextEventCheck_OnWordSelection_m4C290E23BBA708FE259A5F53921B7B98480E5B08,
	TMP_TextEventCheck_OnLineSelection_mF68BE3244AFD53E84E037B39443B5B3B50336FF5,
	TMP_TextEventCheck_OnLinkSelection_m23569DD32B2D3C4599B8D855AE89178C92BA25C7,
	TMP_TextEventCheck__ctor_m4B49D7387750432FA7A15A804ABD6793422E0632,
	TMP_TextInfoDebugTool__ctor_m2A2D1B42F97BD424B7C61813B83FE46C91575EFB,
	TMP_TextSelector_A_Awake_mEE6FCD85F7A6FDA4CC3B51173865E53F010AB0FF,
	TMP_TextSelector_A_LateUpdate_mF02F95A5D14806665404997F9ABAEE288A9879A0,
	TMP_TextSelector_A_OnPointerEnter_m6D15B2FC399C52D9706DD85C796BAE40CA8362D3,
	TMP_TextSelector_A_OnPointerExit_m080D05700B1D3251085331369FCD2A131D45F963,
	TMP_TextSelector_A__ctor_m6AB8BC86973365C192CF9EACA61459F2E0A5C88D,
	TMP_TextSelector_B_Awake_m87D2FCFCEDEE1FA82DEF77A867D2DE56C3AA0973,
	TMP_TextSelector_B_OnEnable_mD1C87684FD94190654176B38EE7DC960795F08E8,
	TMP_TextSelector_B_OnDisable_m429F83E18507E278CA9E9B5A2AE891087ED0D830,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m91D0E180681C5566066C366487B94A05FB376B12,
	TMP_TextSelector_B_LateUpdate_m80F8343FAB19617468E94CD2B35636DBB9AC2064,
	TMP_TextSelector_B_OnPointerEnter_m9A938ED5B0D70633B9099F5C1B213FD50380116D,
	TMP_TextSelector_B_OnPointerExit_mD481099225DF156CA7CA904AA1C81AF26A974D28,
	TMP_TextSelector_B_OnPointerClick_mE4A6507E55DD05BBC99F81212CF26F2F11179FBE,
	TMP_TextSelector_B_OnPointerUp_m5E52652A02A561F2E8AB7F0C00E280C76A090F74,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m01B9A1E989D57BE8837E99C4359BCB6DD847CB35,
	TMP_TextSelector_B__ctor_mC42D87810C72234A3360C0965CC1B7F45AB4EE26,
	TMP_UiFrameRateCounter_Awake_mFAF9F495C66394DC36E9C6BC96C9E880C4A3B0A9,
	TMP_UiFrameRateCounter_Start_mC4A3331333B1DFA82B184A0701FCE26395B8D301,
	TMP_UiFrameRateCounter_Update_mCA98BB5342C50F9CE247A858E1942410537E0DAF,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_mDD0EAB08CE58340555A6654BDD5BEE015E6C6ACE,
	TMP_UiFrameRateCounter__ctor_mE3DC8B24D2819C55B66AEAEB9C9B93AFDA9C4573,
	TMPro_InstructionOverlay_Awake_m951573D9BF0200A4C4605E043E92BBD2EB33BA7C,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m39D0BB71DCCB67271B96F8A9082D7638E4E1A694,
	TMPro_InstructionOverlay__ctor_m103EF0B8818B248077CB97909BA806477DCEB8A5,
	TeleType_Awake_m3501F8FA1B762D22972B9B2BAC1E20561088882B,
	TeleType_Start_m2A3F19E0F9F2C72D48DDF5A4208AF18AE7769E69,
	TeleType__ctor_m8B985E4023A01F963A74E0FE5E8758B979FB3C3A,
	TextConsoleSimulator_Awake_m8B1E7254BFB2D0C7D5A803AEFAFCD1B5327F79AD,
	TextConsoleSimulator_Start_m85E6334AFE22350A5715F9E45843FD865EF60C9D,
	TextConsoleSimulator_OnEnable_mB6F523D582FE4789A5B95C086AA7C168A5DD5AF7,
	TextConsoleSimulator_OnDisable_m1EF25B5345586DD26BB8615624358EFB21B485DB,
	TextConsoleSimulator_ON_TEXT_CHANGED_mD4A85AE6FE4CD3AFF790859DEFB7E4AAF9304AE5,
	TextConsoleSimulator_RevealCharacters_m7BF445A3B7B6A259450593775D10DE0D4BD901AD,
	TextConsoleSimulator_RevealWords_mD7D62A1D326528506154148148166B9196A9B903,
	TextConsoleSimulator__ctor_mA40DB76E1D63318E646CF2AE921084D0FDF4C3CA,
	TextMeshProFloatingText_Awake_mB40A823A322B9EFD776230600A131BAE996580C3,
	TextMeshProFloatingText_Start_mBFC04A0247294E62BD58CB3AC83F85AE61C3FB4F,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_mB0DEABA5CC4A6B556D76ED30A3CF08E7F0B42AFC,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_m8AB7E0B8313124F67FEDE857012B9E56397147E2,
	TextMeshProFloatingText__ctor_m610430DD6E4FD84EBF6C499FB4415B5000109627,
	TextMeshSpawner_Awake_m31920E8DD53AD295AAD8B259391A28E1A57862ED,
	TextMeshSpawner_Start_m189316ED7CD62EFD10B40A23E4072C2CEB5A516B,
	TextMeshSpawner__ctor_m3995DDE2D7E7CBF8087A3B61242F35E09AC94668,
	VertexColorCycler_Awake_m19D37F0DDC4E1D64EA67101852383862DCAAED1E,
	VertexColorCycler_Start_m2CFBFF7F45A76D16C29B570E3468AFEEC2D1C443,
	VertexColorCycler_AnimateVertexColors_mDB7F380B912148C792F857E42BFB042C6A267260,
	VertexColorCycler__ctor_mBAF42937750A7A22DB5BF09823489FDE25375816,
	VertexJitter_Awake_m32ACAC43EDE2595CD4FFB6802D58DEBC0F65B52C,
	VertexJitter_OnEnable_m63CC97434F60690EE234794C9C2AD3B25EC69486,
	VertexJitter_OnDisable_mE5E221B893D3E53F3A9516082E2C4A9BE174DDF5,
	VertexJitter_Start_mC977D71742279824F9DD719DD1F5CB10269BC531,
	VertexJitter_ON_TEXT_CHANGED_mE5AE5146D67DA15512283617C41F194AEDD6A4AC,
	VertexJitter_AnimateVertexColors_m6B361C3B93A2CC219B98AACFC59288432EE6AC1E,
	VertexJitter__ctor_mD5B5049BB3640662DD69EB1E14789891E8B2E720,
	VertexShakeA_Awake_m6075DA429A021C8CB3F6BE9A8B9C64127288CD19,
	VertexShakeA_OnEnable_m39AA373478F796E7C66763AA163D35811721F5CD,
	VertexShakeA_OnDisable_mA087E96D94CB8213D28D9A601BC25ED784BB8421,
	VertexShakeA_Start_mDED2AEA47D2E2EF346DE85112420F6E95D9A3CFD,
	VertexShakeA_ON_TEXT_CHANGED_m0B59A798E6B193FE68F6A20E7004B223D5A2993E,
	VertexShakeA_AnimateVertexColors_m238AB73BE06E33312281577CC896CEB7BB175245,
	VertexShakeA__ctor_m41CBBA607D90D45E21C98CCDF347AE27FB50392F,
	VertexShakeB_Awake_m7CBA45BF5135680A823536A18325ECA621EF7A1A,
	VertexShakeB_OnEnable_m39EBB983A4FFFF6DD1C7923C8C23FF09CFF2F6E2,
	VertexShakeB_OnDisable_m8E3858FC1C976F311628466C411675E352F134A5,
	VertexShakeB_Start_m666FA35D389B109F01A5FC229D32664D880ADE09,
	VertexShakeB_ON_TEXT_CHANGED_mF4858E4385EAA74F5A3008C50B8AD180FCBC8517,
	VertexShakeB_AnimateVertexColors_m076A6C9D71EE8B5A54CD1CEDCA5AB15160112DD3,
	VertexShakeB__ctor_m5CAAD9DFA7B4D9C561473D53CA9E3D8B78AE5606,
	VertexZoom_Awake_mB18FF89A84E2AA75BDD486A698955A58E47686EE,
	VertexZoom_OnEnable_mCD27B81253963B3D0CD2F6BA7B161F0DFDC08114,
	VertexZoom_OnDisable_mB32AD5B7DFF20E682BA4FC82B30C87707DD3BA10,
	VertexZoom_Start_m6C64C692D81F64FB7F3244C3F0E37799B159A0DE,
	VertexZoom_ON_TEXT_CHANGED_mC08504F9622CC709271C09EDB7A0DF1A35E45768,
	VertexZoom_AnimateVertexColors_mEC9842E0BC31D9D4E66FD30E6467D5A9A19034D6,
	VertexZoom__ctor_mA4381FC291E17D67EA3C2292EAB8D3C959ADEA79,
	WarpTextExample_Awake_mF6785C4DC8316E573F20A8356393946F6ABFC88C,
	WarpTextExample_Start_m8E7AC9FF62E37EAB89F93FD0C1457555F6DCB086,
	WarpTextExample_CopyAnimationCurve_m2C738EA265E2B35868110EE1D8FCBD4F1D61C038,
	WarpTextExample_WarpText_m27664A46276B3D615ECB12315F5E77C4F2AF29EE,
	WarpTextExample__ctor_mF5BA8D140958AD2B5D2C8C5DE937E21A5D283C9F,
	CubicBezierCurve_get_Length_m5082DF3290B1CE1F6CAD6F369B230BEAAFC42DB5,
	CubicBezierCurve_set_Length_m9F77B2C0AA6EC07606FCB6B10FE3C6D4D2DC3B64,
	CubicBezierCurve__ctor_mA1AD052C90118D551E036008D38463B8F4368DA6,
	CubicBezierCurve_ConnectStart_m3A40695F7ED43823D3AF5804CC8498EFDC2E7277,
	CubicBezierCurve_ConnectEnd_m1D0CB74ED3C616DEB86127455B726703F036EC2B,
	CubicBezierCurve_GetInverseDirection_m3F64261A4F2FD6F2D11A2128473D571BD9AD3A61,
	CubicBezierCurve_GetLocation_m75A2C483E32ADB4FA13AF748923FA4A113D7E2C5,
	CubicBezierCurve_GetTangent_mDA3D54EF15507E2B822CE3B4E275DDE8A4AD0765,
	CubicBezierCurve_GetUp_mE03900461C9AAECD6A951DA44CE2802224D6BB0C,
	CubicBezierCurve_GetScale_m8F11872104B3A4D713665F75472D75894A5C4331,
	CubicBezierCurve_GetRoll_m5E1A5EF308122ECF2BB8F75691E1961B6852C420,
	CubicBezierCurve_ComputeSamples_m435AD1667FB014EA026834922734FC0A80F5EB4F,
	CubicBezierCurve_CreateSample_m19D365B323469A932D862F304D2F32392399784B,
	CubicBezierCurve_GetSample_m7C6207AEA1DCAD5597D664703CF643B9E242505D,
	CubicBezierCurve_GetSampleAtDistance_mF2519465EEA9DA43AC35D08C1BB7B9C9C4261603,
	CubicBezierCurve_AssertTimeInBounds_mDC13700258EE6F4386BA53942D0F6665937B0D00,
	CubicBezierCurve_GetProjectionSample_m198A9E1687786CAD7A0187F0B714A1507C8D65CF,
	CurveSample_get_Rotation_m12EC3A372A61628C5434501F042DC94A89986DF5_AdjustorThunk,
	CurveSample__ctor_m32B13BF83CB75A40752A0E4514CCEABB5EDE0890_AdjustorThunk,
	CurveSample_Equals_mD9AF8E57858E6BDA77260066B4EDBB1E9C3DBD4F_AdjustorThunk,
	CurveSample_GetHashCode_mA4F4E663E9E234361F72C7EE56F3D0511D148883_AdjustorThunk,
	CurveSample_op_Equality_m940288830BAD0554F536D3CCB40A78CA0D422A82,
	CurveSample_op_Inequality_m7139DDC0C0AA6DF5644727E7472137D0047C61BE,
	CurveSample_Lerp_m56950AD42D380848ABCB7EF6F803D51FCEDCF0C5,
	CurveSample_GetBent_m23AC96BC5669E1611F260AF061FC48CCF90D8226_AdjustorThunk,
	Spline_get_IsLoop_mAAD8B8DAA2FD7F8F962B517F648FDAE3E4CBEAD0,
	Spline_set_IsLoop_mEC84E3A178EDC68ADC4516C223CAEC88D291965E,
	Spline_add_NodeListChanged_mC9AA7EF3FAA5C0B3A894305693CF6291A07008E2,
	Spline_remove_NodeListChanged_m3E310440C66EBD7E541AF6D2556726033B54CEF6,
	Spline_Reset_m7A03F8AFA702BFE44A504D380BBFDD91799526B1,
	Spline_OnEnable_mE711C675006F38DD0D364873858E80DFABC073C0,
	Spline_GetCurves_mA237EF8A322C0BF1C0D588E75E6BA409ADE1D58C,
	Spline_RaiseNodeListChanged_m58B1DE7242D4A1CB32D11F141565FD5640746055,
	Spline_UpdateAfterCurveChanged_mBC8C3CBF000CDCF6721303DF815443A2DBA45C1D,
	Spline_GetSample_m79F3DF84C7247150AC0B1D8AA65B1F7E0DBF5332,
	Spline_GetCurve_m549F6369E3D73C304A4566D2135F824E62EB261B,
	Spline_GetNodeIndexForTime_mA725AF3A95AF01F4398FFA3729E7310643C5BCE3,
	Spline_RefreshCurves_mDB7CF3FCDF9DC0F2413BED2B832486FD800B1E7B,
	Spline_GetSampleAtDistance_m535736A11F122E1B2B02A7BC4BAD7C645EAC0D8C,
	Spline_AddNode_m1CB08761B4DEA22154D217FC83C64F9AF4D9D097,
	Spline_InsertNode_m90EC00FCACDCA969C8F6926894994850DB22B30D,
	Spline_RemoveNode_m92989CB5808908E06A3284D12E96653178A9D9A7,
	Spline_updateLoopBinding_m0F220B0863DF6C8C5A40059EAB4F1DE85D392928,
	Spline_StartNodeChanged_m6508019AD84AEA54811674F2A6D63D0EB45F95F1,
	Spline_EndNodeChanged_mD69A191E94F3AD32190D1B04523170AB422941AB,
	Spline_GetProjectionSample_m671FF1697E9A6D955F743769B06D4802BEFFA5BD,
	Spline__ctor_m0EB49BF62FF595A502FEC2AD730BE6E318AD91FA,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SplineNode_get_Position_mB9FB3BC4D2BC5ADDA2CF438B2710720EE1FE304C,
	SplineNode_set_Position_m45768ABD2E18AD917C144AE373D4C1460376AED8,
	SplineNode_get_Direction_m24AFBFE0574D9F9093E7637D9102CBA52CDE94F1,
	SplineNode_set_Direction_m761EBC8150EF28F6156BF49ABCD5C27EB8C42855,
	SplineNode_get_Up_mA4C435B54CDEE190B67ECAC1145548FECEE0C64A,
	SplineNode_set_Up_mFD16FE44DE72C4E5980F0391022E72D277828CF7,
	SplineNode_get_Scale_m12AA98174CA7D6955485B99F722E25AD7A3C3131,
	SplineNode_set_Scale_m46DD6EB07AE6989003C9C1D8E053A28656EC0ACD,
	SplineNode_get_Roll_m1C3A5F2AEF7A4A8E2E0B04FEB5A6398942CA0433,
	SplineNode_set_Roll_m02F3E2960C9A91FC6353019F0C53127A759ED8CA,
	SplineNode__ctor_m98587A573F31D62FEB60E895FF06CE53EE05C908,
	SplineNode_add_Changed_m2685DBE22A2B7F097335F2539D2E8CDD77E326C8,
	SplineNode_remove_Changed_m065E3B27E2E2AC3C066C31322F1F11F218D3EDC4,
	SplineSmoother_get_Spline_mFA1CAC789DC283F9FB321B612AFF1D09A13FF5C8,
	SplineSmoother_OnValidate_mE0C1412A8F1EC2F565A286DB9822AC29A9A7FC2E,
	SplineSmoother_OnEnable_m23C92077FD6B31A40ABE47B8A7B3D6D07AC5058C,
	SplineSmoother_OnDisable_m10149F042FD4CCFCD3492EEAD6B229AFC8360156,
	SplineSmoother_Spline_NodeListChanged_mEDC04AFCB1F6D81ADF08B0D017861564C9E00680,
	SplineSmoother_OnNodeChanged_mF4EB1967C0AEBE004B7F3901BEBE484F303DD182,
	SplineSmoother_SmoothNode_mB5E122971A3696999409B17889402EA49628B7EA,
	SplineSmoother_SmoothAll_mDB1CC0BAEAD3B5AA900F0BBEF3F87197DEBBD2EC,
	SplineSmoother__ctor_mD376935A6685D1DE97CECB9258874A2257FCC55D,
	ExampleContortAlong_OnEnable_mF7838AACD660A55CCC12A792FC14DC1A834CA2B8,
	ExampleContortAlong_OnDisable_m2EFBC93678B148943C78E432C6C304E885436B5C,
	ExampleContortAlong_OnValidate_m83C5680DEE2B5A531BE4D0C23C747BA9CEC3B459,
	ExampleContortAlong_EditorUpdate_mB0A413C91A345A3ADCDAAFDCBD86286986948260,
	ExampleContortAlong_Contort_mD5F99BD4E6A561A77935C97D57ADB1B715D8DD72,
	ExampleContortAlong_Init_m87C79455B2793F550B823A8554F93DB63884AB35,
	ExampleContortAlong__ctor_m193171B52C7ADCD961826D3ECDBF2CD599A435CA,
	ExampleFollowSpline_OnEnable_mA5AFC04210DB9B4738AC5B40AC9CA1BCD1A9F2AA,
	ExampleFollowSpline_OnDisable_m725141669D5227B8DB31E0203017CFF688C07764,
	ExampleFollowSpline_EditorUpdate_mA5CB3FB5E6ECF17E1F4212116CB24A982B07896D,
	ExampleFollowSpline_PlaceFollower_m30518F2A328F6FE403BF8553A54F44F84F971F42,
	ExampleFollowSpline__ctor_m4E732FAA51376EC90D0B0B90152A01DE68A7F129,
	ExampleGrowingRoot_OnEnable_mA2B0E195ECD65E9BC5A79ED9D619EAC5815F1D02,
	ExampleGrowingRoot_OnDisable_m4239E494883008764E08EBA383C38D7A9A848B54,
	ExampleGrowingRoot_OnValidate_mD4FAFBA4EF1B34A76B6EADBBC82DBBD72C4B42E0,
	ExampleGrowingRoot_EditorUpdate_m7D4E386AE3016E97B0C9FD8A345C1EF3D845EA0D,
	ExampleGrowingRoot_Contort_mB06C25DA7C6282FB3EFAD8F1040811447F375DCB,
	ExampleGrowingRoot_Init_m3EB07714EAF0E83E197B2CFA8101D2386ECB70D0,
	ExampleGrowingRoot__ctor_m577BFA03E45E998263B19DCB04B9D0BE0AF70A2A,
	ExampleSower_OnEnable_m303C3E9A5E9406EB1752ECCA8F307F68DB796371,
	ExampleSower_OnValidate_mD2BA43503B8861163356F1C1E7C57D25808121D2,
	ExampleSower_Update_m9C7C8A45AE2C14C3B96686D342FDFFEEAD78CA54,
	ExampleSower_Sow_mA1E4758F839D3A568CF197E7311CB039648AC10A,
	ExampleSower__ctor_mF0FAE098AB6AA19C4D715D926E3FD0A5BBB32036,
	ExampleSower_U3COnEnableU3Eb__12_0_mEF96ADA9A75BB7DE3BF0C48C1400594577636F4E,
	ExampleSower_U3COnEnableU3Eb__12_1_mC7897F657EDF6D56900D73FC70CBD80EBA176E0D,
	ExampleSower_U3COnEnableU3Eb__12_2_m9D5BBC7CB2F4679B5A200C66F429E6DB0580D4AD,
	ExampleTentacle_OnEnable_m567A6EE78E936F003AF60484AC530C994745A252,
	ExampleTentacle_OnValidate_m438BF1A86936AE7BE709FB26DDF29751CEEC953A,
	ExampleTentacle__ctor_m6D097D31766A4E1981D3F0CD7B6CD5DA9469B190,
	ExampleTrack_OnEnable_m9C3E79495F2C9088C0321E3173F3B6F39BF64FE5,
	ExampleTrack_OnValidate_mFE7799F982E2D57DE0D8FF873F91E3E1D1D17EBB,
	ExampleTrack_Update_m9C5DF30B69B9D3C45FEA75A8557357D616F23357,
	ExampleTrack_CreateMeshes_m036E99CDD7ED2D00A160AE65A3C8BA687CF67C5A,
	ExampleTrack__ctor_mD1F9484DEF697C914F8753745F277AE93904CE19,
	ExampleTrack_U3COnEnableU3Eb__5_0_m511F1FB402B2EE562D71916F0CFCE6610FDE2701,
	TrackSegment__ctor_m8985138C5E471DDDC635AABB43432BFA83C4287F,
	TransformedMesh__ctor_mEBAEA1EBF0563FBDCA307AD3250FD279B0DA6FBC,
	RopeBuilder_get_Generated_mC61A6581BA0400735181B216CF7C53E17AE37191,
	RopeBuilder_OnEnable_m19D5BEDB506DEA7770F18D56459D6A63C1662E40,
	RopeBuilder_OnValidate_mE8EB8870F70BEE530169190B93B9F79CFF1818EE,
	RopeBuilder_Update_m884098CF2C8BAC17226FCB02980A2A945C5E5F95,
	RopeBuilder_UpdateNodes_mBE69AAAEEE3B0B3C5A2F52C6A4BAFBB46E9070A2,
	RopeBuilder_UpdateSpline_m2AC85FDC731590170C071B0DF7D736F2F6FC5C8B,
	RopeBuilder_Generate_m77D55939264F4046DF36CFF8D6C0E06507996517,
	RopeBuilder__ctor_m256F48ABCA41FBBC96BD623C4E5485CD4CDDB281,
	ExtrusionSegment_get_ShapeVertices_m6D69711D6B557E0F346677C5EDD419DA1D3DE5B6,
	ExtrusionSegment_set_ShapeVertices_mDA6295EA450418E65F2373F8B8BFBEC615CDC354,
	ExtrusionSegment_get_TextureScale_m9D14AC0E3A7101405A2306DCED3A6145996FAB88,
	ExtrusionSegment_set_TextureScale_mDDA6DFA5D54946E18539DF1CADAFC0BBC945C6F0,
	ExtrusionSegment_get_TextureOffset_m8599921134CFD72F3214D091F607561133EE04FE,
	ExtrusionSegment_set_TextureOffset_m7A5F79228C898A835570F2550F1AFC342594681C,
	ExtrusionSegment_get_SampleSpacing_m470E3B6FC5849B793E0196B2290F6A8B87A76E0C,
	ExtrusionSegment_set_SampleSpacing_m8F468B88AB0811A899AFDF276AFF20B66FE00AD6,
	ExtrusionSegment_OnEnable_m06512DAEBAED61A0E9F52842B348D5E501DA60EE,
	ExtrusionSegment_SetInterval_mACF749D9F47F43EB85C080D7D6411F8DD83B634F,
	ExtrusionSegment_SetInterval_m18E24BE29F2CF0667AEED6C96F07C42D57C04957,
	ExtrusionSegment_SetDirty_m370D7B2092E11A335C753ACA7FADCDFB8BFA2221,
	ExtrusionSegment_Update_m3F4748B31D25C4BC6C0805DBF423B1E4711DA945,
	ExtrusionSegment_ComputeIfNeeded_m767F1A1D1E618CBBE708362269D12F1A2DB8BDA5,
	ExtrusionSegment_GetPath_mFF4A51F851DD783BF23C6251E49327B24685490D,
	ExtrusionSegment_Compute_m9DD76117647220A7E08F2A5ECB658D365ECBCFF5,
	ExtrusionSegment__ctor_m330C2931D19CF58020F3056D1D6A4C80BF19B14F,
	MeshBender_get_Source_mAA711E24B40BC85423E9B4E0C0F4AA3C9CD5BD1B,
	MeshBender_set_Source_mABA7B90704F20FD0E0DB098C8CF3FC4B1D393735,
	MeshBender_get_Mode_m2D5CBFF8E88ECC0FCEE73884611AE8CBB6C9314A,
	MeshBender_set_Mode_m9EE6173509C438B0A16E67743D46B555483BEE11,
	MeshBender_SetInterval_mD2A0DD0CC2688940B27A339BA18CA3CDAB3E0D0F,
	MeshBender_SetInterval_m48EEB022D916BF0D75381ACE858FF476161AE7B9,
	MeshBender_OnEnable_mD8AF660FE0022E1DA4023CF2C4E2A0CA86113D8D,
	MeshBender_Update_m8B3980983FBB89C4B4A47A6182ADD7CD45DC96A6,
	MeshBender_ComputeIfNeeded_mE844401FDF7F0E04F84AB4BCEEE7745EA0F6F7AC,
	MeshBender_SetDirty_mB6DC4CDBE3BF713F629CC37CD80611130915BD79,
	MeshBender_Compute_m9B5D232898974B2CD184F01ED7FFA58B53F4F36A,
	MeshBender_OnDestroy_m8680893AFA02F731E5C2B5C2D64025146DEE3EDE,
	MeshBender_FillOnce_m56F5A59AFA162DDE7F9E3259F260C7B8CE638220,
	MeshBender_FillRepeat_m17CBAA349B1D0F1E9088A5238D7098B282519073,
	MeshBender_FillStretch_m7AF6BAD2D623F3FCACF02A68D9297DE682F3434D,
	MeshBender__ctor_m4EBF0737151F2682E67EB95477CB8F87602D0485,
	MeshVertex__ctor_m68DAFC04C45FD91053423850F4DB5FC9A28081A6,
	MeshVertex__ctor_m38F8CF8B756D057812B5017D806A838EBCFA0194,
	SourceMesh_get_Mesh_mD5822953A429B22C9849A3E1B62715BAE6AE8883_AdjustorThunk,
	SourceMesh_get_Vertices_m85732F747D7CE434323D6E01532F65C95654D922_AdjustorThunk,
	SourceMesh_get_Triangles_m3019F09D075CEFD5329BC932C81669AA72BC762C_AdjustorThunk,
	SourceMesh_get_MinX_m8AEE8617C2C634C6878BDD80C7273114649CC40D_AdjustorThunk,
	SourceMesh_get_Length_mE0F51EB74C3E5C269B56021D3D6AC2FA735D74E3_AdjustorThunk,
	SourceMesh__ctor_m8EB6398E50FAEDF21BAD1BC329FC679CF616255E_AdjustorThunk,
	SourceMesh__ctor_mE93F93EBBC5DCCC772FD6DABD52320646B6ADA8C_AdjustorThunk,
	SourceMesh_Build_m9311EA59CFAC0C51687872884386A13C0D9D46A6,
	SourceMesh_Translate_m1697B3DE22A47D6D3226A61F2EECCF9242C5CF13_AdjustorThunk,
	SourceMesh_Translate_m9662F3A26E0A594567327CB6B8CF8AFC3A106E98_AdjustorThunk,
	SourceMesh_Rotate_m64F45A9A69E80E0B99689D74192A18C7DE3173B5_AdjustorThunk,
	SourceMesh_Scale_m8B7D9146967546B25A75DACAF56BCDC420B20ED4_AdjustorThunk,
	SourceMesh_Scale_m57CE92D4BF731C236A706ADADE04FDB58022F3BC_AdjustorThunk,
	SourceMesh_BuildData_mBF02CC17C119CFBB58B3FC44AE6116EEBC3A3341_AdjustorThunk,
	SourceMesh_Equals_mA2239E3B5CCFE04F905CFDF726D4616B6217A578_AdjustorThunk,
	SourceMesh_GetHashCode_m1E9A1EC18A819FBED34524BF7974E0E3AD574A6D_AdjustorThunk,
	SourceMesh_op_Equality_m1E6D6A4A26EA51C05CB94E5BBE54010EA0BF9CE1,
	SourceMesh_op_Inequality_m876C903730A5EC74A394E80F0AF4DF4782FBB6A9,
	SplineExtrusion_Reset_mDEC016069336429A5C988784FB7A9D2481706420,
	SplineExtrusion_OnValidate_m8CBFD50673612819636F2F725754FD7A1AF1319C,
	SplineExtrusion_OnEnable_m83A7E883C689AB047D971AE6F19FAC4BDA4B62F5,
	SplineExtrusion_Update_m0B361F04E531DD7F3D918F6DDC0DEDE6D4ABD7E2,
	SplineExtrusion_GenerateMesh_m705A24F42E1AACEEE72EDF865608C1C42BA37B15,
	SplineExtrusion_SetToUpdate_mD5B3DDB09A047C5BAF6B0EB472F4E8FE602B1146,
	SplineExtrusion__ctor_m8399C72C29D606FA8BB89DB1D389FA312CD59A06,
	SplineExtrusion_U3COnEnableU3Eb__9_0_m36074849F20961576F532AB472F7E82328BCC9AD,
	SplineMeshTiling_OnEnable_m06602898315E06DD639577393BEC9D75CE7A15DC,
	SplineMeshTiling_OnValidate_m451E0F84CA9067281E28E3497D5BAA8863B40733,
	SplineMeshTiling_Update_m156EA4837039685AD1A817246F7E35FBB2576ECF,
	SplineMeshTiling_CreateMeshes_m0647CEA47D66D624A9247B953DC4CCA69C913CDB,
	SplineMeshTiling_FindOrCreate_mEAE901F00DE893A38D6877F9BCE9E53EF454DAE5,
	SplineMeshTiling__ctor_m108A5B74A7CEF25BF434DAA44D40B45F242F8673,
	SplineMeshTiling_U3COnEnableU3Eb__13_0_m29C4ADD8CEB4E49C669E36800C05A0C3E138E8A6,
	CameraUtility_IsOnScreen_m9DA3BC15A360DC4500BF36F63A273E6670D6A6E5,
	MeshUtility_GetReversedTriangles_mB206A681F7CDA3F30C19DEE6046D9E2943C6E21F,
	MeshUtility_Update_m293B74E825E7809BB9DF84C1E7193767DE4989F2,
	MeshUtility__ctor_mF62C47BD567C1DEEFC1933A55407FE8FEF6105D5,
	UOUtility_Create_m79A06FD68FD2DF6A4FBF179B77FD676919D2B118,
	UOUtility_Instantiate_mC3E3C4012590C8982944FC570A72737C2A9BD276,
	UOUtility_Destroy_mC8A244AE4C70CCEA82F9C3250CB15018911BE9AD,
	UOUtility_Destroy_m39BE6A8E0CF388BE124F9FDBEFF519C468CC1E73,
	UOUtility_DestroyChildren_m4A9667ABF2363D3461F8EF4C6E65D82EB825B022,
	GlobalDataController_Awake_mAC99E23BD358EBE17E35531388E5A6C742006902,
	GlobalDataController__ctor_m19B00F456AD9EABA1A6BE11ADCA8E4A4020BDE4F,
	NULL,
	NULL,
	RequestHeader_get_Key_m3A2B4A1CDFB2B7F9A8AC9EFCCED4E2026F224B4F,
	RequestHeader_set_Key_mC722B90800295D0D2F8D7A111CD15EF6AF2EE94E,
	RequestHeader_get_Value_mBE7C0AFC2B54F388655D6B2ACD06F29DA81F1DFF,
	RequestHeader_set_Value_mE1C87A6D85D786A8FCA77342258B10E0D5327C44,
	RequestHeader__ctor_mF2C37FD3744852423F9AC2BB0892DBEDDE333554,
	Response_get_StatusCode_m4AFEDEB66072BEEA17D379AAA3098142095B154B,
	Response_set_StatusCode_m0174486BB7B6D527995007E4F8D986682CF3E3E4,
	Response_get_Error_m6B827CE1C5216DC68CD02B2D14CC4BCF734AEBEE,
	Response_set_Error_m61FA9D6F0B3448EE4CA3EE997C2C07548F7C0738,
	Response_get_Data_m533C8EECADD1590D8D91E034FE35684EA0A12B3C,
	Response_set_Data_m889420365BC4745949E086BBF51D9BC4C9F80054,
	Response_get_Headers_mCC3E0D3F6ED69D447BF101368EB26CF5486A9402,
	Response_set_Headers_m5FB48E7AD57BFC340E1420F46BB7C367AA298450,
	Response__ctor_mA3F7F1B3A1B6633E46C8845BEE8B8665F77549C3,
	RestWebClient_HttpGet_m927816995B02693B5AF7D25D437533589D477813,
	RestWebClient_HttpDelete_mF84B5E07CE36E24ECD0AACBC058B324E823A47C8,
	RestWebClient_HttpPost_m804535ABC9F95CC87ED4CB3F25D0BDE3F7263656,
	RestWebClient_HttpPut_m6792D259CEB2BB8ED7207CAAB0EDC0F35FD50CEC,
	RestWebClient_HttpHead_m5F32AC030F72C4FC04D76C091505C0996A126119,
	RestWebClient__ctor_mB223998642156DF065213E4913438D0DD84E387F,
	ArmatureData__ctor_mE9305C84DB20D08F32729136813D50039BD48C70,
	ArmatureDataCollection__ctor_mB024E4452A4F274EA0A68A1AE0AE547CEE232EE1,
	ItemData__ctor_m29EFD39FFB2B4A1BEF42CDE5DE78AF32D03D7117,
	PipeData__ctor_mF0AE9CBAA970A085FF23AC52204BAF381DB43AD1,
	PipeDataCollection__ctor_m2152F3F1CD4BB5808478CF6A082BA9C95BF45124,
	ChooseArmatures_Start_m411049CCA2C5677EA297C7E589BB33EC6B152E1B,
	ChooseArmatures_OnRequestComplete_mD3CCDC931D907E111A0A9A84901AE7B4A366545B,
	ChooseArmatures_OnPlaceArmatureButtonClicked_m6E9AE0C202245A64CAB828F8D39CA1C3F9A81442,
	ChooseArmatures_OnAdoptArmatureToggleClicked_mFCE677C3414E0548EABFB8BFCF76DD5D7FFA2A29,
	ChooseArmatures_ArmatureOneDropdownValueChanged_mC51FEF91F26A4BA593D4BE149823289A8B53D4FA,
	ChooseArmatures_ArmatureTwoDropdownValueChanged_m1E76804734D5FD8BA88D86118488941EF670E953,
	ChooseArmatures_AddEventListener_mAB53D5ECCCA49855DAF517330F98497C2892E4BD,
	ChooseArmatures_FillDropDownWithData_m5D5C779F40BEC42FCC3DBC4E102DAE4B41E9738A,
	ChooseArmatures_DataCheck_m5710E3CC80A068DB61D3D0D86892AF8EE9B05D48,
	ChooseArmatures__ctor_m5E65C06EE9E7EC399025FA6BC8556212D58956BE,
	ChooseArmatures_U3CStartU3Eb__10_0_m581EC5F0A0F4C41E347BB166E81773912F6BFE9D,
	ChooseArmatures_U3COnRequestCompleteU3Eb__11_0_m603E06009E8B69BC473C1488D71FB4296A65C7A3,
	ChooseArmatures_U3COnAdoptArmatureToggleClickedU3Eb__13_0_m4E05E1D6FA098BD7A93CD36337BF2CEE4FA38658,
	ChooseArmatures_U3CAddEventListenerU3Eb__16_0_m28DBBECD92E9DEB8F542AD958E35A6BBB4FDF117,
	ChooseArmatures_U3CAddEventListenerU3Eb__16_1_m93CF1C44334C20114513308D8249939F46D1D0E3,
	ChoosePipe_Awake_mAAED78B05A5917C3E024AE3CF9E75A5DC60CC08E,
	ChoosePipe_ResetConfiguration_m7CE295103DC51B5719B53C77997EEA421489E1B9,
	ChoosePipe_CategoryDropdownValueChanged_m9D2D405C6C9C846BD26E2979DE0F5ADD561A3804,
	ChoosePipe_PipeTypeDropdownValueChanged_mA2E0F4B245693F26758333F20E7C37C8E96B456D,
	ChoosePipe_OnRequestComplete_m1E0E51E56ED6471C9F7A4275910AB3D122F3A822,
	ChoosePipe_AddEventListener_m03C1BD855D372A155FF90969FDA562893E47C5A6,
	ChoosePipe_OnPlacePipeButtonClicked_mBE70A741E913D1D14829E5D0639D0619B6B5EA82,
	ChoosePipe__ctor_mAD7E37B4448A60E4C4070E6E92D21D16BAF2AB51,
	ChoosePipe_U3CCategoryDropdownValueChangedU3Eb__11_0_m847D8470A75F4D5F62678F5CA7ADA402C2BABC38,
	ChoosePipe_U3COnRequestCompleteU3Eb__13_0_mBE4679A5351937DC9E86EEA471D279B083DF37CD,
	ChoosePipe_U3CAddEventListenerU3Eb__14_0_mEC750E2BFC7148B65EC0BF6EE849A7FA430D47DC,
	ChoosePipe_U3CAddEventListenerU3Eb__14_1_mF86E2C6D22C0B42E1C0DE621357ED5297D611D3A,
	Error_Update_m882CC7E0B84B9591B05823AB88E2244BBEE57674,
	Error__ctor_m8BE2636542E3622585AA12AC27C485BC736E31ED,
	OrderOverview_Start_m0EB29791580D029C6EABFF19377F07A1DFA3F752,
	OrderOverview_DataCheck_m4469E5A063B7CEF435DED7F722ADF236AE87D324,
	OrderOverview_InitTextFields_m97AFC9DCF1C0280CC61E4905E823C0A813211D2B,
	OrderOverview_AddListeners_m0B1E0ED4839FFDC9D05CDAFE7268ECE25EFA461C,
	OrderOverview_LengthValueChanged_mAD182FED1D9D18DAD789C0C4862D6BA7CC9128BC,
	OrderOverview_AngleValueChanged_mDBAEA85AAC16E0473FABBADC9D3F03728A96CC6C,
	OrderOverview_IsAngled_m8E1AE6B43C777CE178D3AF6F2266ECDB11D5598C,
	OrderOverview_OnRequestComplete_mA23FF8DAA8D95D702EEA18FFF9087ADCB5CCF69E,
	OrderOverview_IsAvailable_mBC83D8C43A4B6E34123B17C2B3D2A820CD33EB3F,
	OrderOverview_CalculateGrossPrice_m8A79FE6D28C50823B145D1999F48144E44A50497,
	OrderOverview_CalculateNetPrice_mFC6CC3DF5459A3C66CAD21A1A96891CE36617F9E,
	OrderOverview_ParseXml_m1D84A1BF66749D77F0880997DD8616E00C1A3495,
	OrderOverview__ctor_mF5D5324828ED94E9E92F176E8AD99BDAF52D5041,
	OrderOverview_U3CDataCheckU3Eb__16_0_m18450BCC626FE31417EC9A03E9B80D7C1B5CC614,
	OrderOverview_U3CAddListenersU3Eb__18_0_m3F22AA6237DB3BD6AA796EB88FDF39425E3EA839,
	OrderOverview_U3CAddListenersU3Eb__18_1_mCC20ED68290726A1D464FBC3699F99F2D6A3A581,
	PlacementController_Start_m4420C505AA8D9289FEADC6D6DA1A2E44826DF8B1,
	PlacementController_Update_m506C92280E3D90949D2DBD3DA5F58A50771FCF61,
	PlacementController_NegateCanvasVisibility_mA14CD8300E6A4F2EA4340061FEC69C8D4F2AA4CB,
	PlacementController_DataCheck_mC5A943B4CE29584A5876EB16C69D9975A1D45627,
	PlacementController__ctor_mE7D8E5E75398D80B9113BA447CFEC03D116B2B24,
	PlacePipe_Start_mCB4C514447D2BC5D90CD6C8DFB2BEAB992C97AAB,
	PlacePipe_Update_m0F08EAA6966D129C82D4408E34DF7F45CA92D2B4,
	PlacePipe_Raycasting_mEFBE4A4C72B1AE367B293DAC79D08BF525752813,
	PlacePipe_Add_m2DCB9A00D1152856F808DFAAACBED087A467066B,
	PlacePipe_RemoveLast_m34262F3BB0913C0B4AF5A3C06DDBAF6A7C8E465D,
	PlacePipe_DrawPipe_mB24AC37A65645CD95A21D3EA5B337E2CF72D7455,
	PlacePipe_UpdateButtons_m6E95510F0302A03BDA15F0BA78C81B7A1E376E7C,
	PlacePipe_UpdateTextField_mDD75A75DC3D8DF88036400C1EF400CBD51FFAE87,
	PlacePipe_ResetScene_m0662F6D1A24FAED0668C4C4909F017E1819490BB,
	PlacePipe_Finish_mC8714AB0AD4A216264D434F16E94821D776C6163,
	PlacePipe__ctor_mACD9DC8F612DE8958832572345E0EA2D73DA09EA,
	PlacePipe__cctor_m274AD8DCC794FFB213BA6C7CC116A593408F0599,
	PlaceArmatures_Start_m65DA4BEFA56D2D36EA0E6810148FAE3CD5966AE8,
	PlaceArmatures_Update_mF900C2E2693C7ABCC8EE4C03ACD4823A5B2645AD,
	PlaceArmatures_SetArmaturesOnPosition_m0A8B7C5B5745DECCCA5B4B56DE9D316D24F5A4BD,
	PlaceArmatures_CheckAngled_mC896B38DD7F6EFE3CF37FB7C4CA910F813076E78,
	PlaceArmatures_InitializeTexts_mAD4468F9B7D6B4367E983DCE2D81B5ECB1E46618,
	PlaceArmatures_SelectObject_m72806760DC2AC7E68E4B0648B3CE2EB23552839D,
	PlaceArmatures_Deselect_mB27E85C6B94391558FA64EB457383F3CDFFD57C6,
	PlaceArmatures_SetObjectVisualizer_m2607E122E08F39DFF68DA5DD1E8AB367E7A38CB1,
	PlaceArmatures_RotateObject_mDE7B762D0E0D060C10CCEFE0C56B4CBE06F919FF,
	PlaceArmatures_UpdateAngleValue_mBA3632EDB5FFDB764C8832CD38733DEC9A5D4632,
	PlaceArmatures_ChangePipeMaterial_m39DE00385C2DDA294E7BCE31E3A99AAACD60BB31,
	PlaceArmatures_ChangeArmatureMaterial_m74D6A1B67F99E95A0CFF0000AEE00E32A85F6EB9,
	PlaceArmatures_Finish_m7EF3100CE5BDC954493A98070A52E94E39A945E9,
	PlaceArmatures__ctor_mC1CDD280A12F690D7F0D10F66105F6BE9924358C,
	LoadingProgress_Start_m89EAC7EFCD1E51CE32D9C31E9AC320840747533C,
	LoadingProgress_Update_m38F1D29CA5E253A7362F7383CB2C69761FF400D9,
	LoadingProgress__ctor_mF133A4E04E677D7338C4E5E833DF13EA18CF9104,
	SceneLoader_Load_m4356F5746D09B88AE072993683CF457E2DF6580D,
	SceneLoader_LoadSceneAdaptive_m4413745F03F34942419CD0EFFAFE2F4CC699F4E6,
	SceneLoader_SetActive_m2A5B7D715F6916212EC27BD102527F9E189DBE1C,
	SceneLoader_UnLoadScene_m356A6C86F3F8825E28F40F78122BEDEEC48F7799,
	SceneLoader_LoadSceneAsync_mA7B1EB4E8A6C428BD42C74AA97573219FC6F1EB1,
	SceneLoader_GetLoadingProgress_mCE591F3C8E279E39B40B7D6DD60AB33F9EDEE31C,
	SceneLoader_FindScene_m778C5CCC46E9BEBA487C4EA32F6DB7DCA8CF58BC,
	SceneLoader_SceneLoaderCallback_m1777D34AAB6327B512DDEF49E2A228E5A1E43DCA,
	SceneLoaderCallback_Update_mA0B08178E51AB738F9567F55081B56D50EE630C8,
	SceneLoaderCallback__ctor_m41083CDC93B7561E8F47CE927B3D21DBEE209291,
	ButtonHandler_LoadScene_m9194A4B6B1990C84E5BC9349EB80ACDB2074E1DE,
	ButtonHandler__ctor_m2FE0B4B9A17406C58B3DA2D88249F784FE50EC5D,
	ButtonSwitcher_Start_m7F11F558F06D1A169E981AD650A8AB8FC1058EA2,
	ButtonSwitcher_Update_m3806F33A98679AE2AC045491E747697033A95892,
	ButtonSwitcher_MoveCanvas_mB95A921EEE0F25CBA1970675BF52394EFAAD296F,
	ButtonSwitcher_CheckPlacementMethod_m9197A1BAAE48F35DA13BA108EA48EC5FD6329851,
	ButtonSwitcher__ctor_mBA1EA68B10F5BB8130A2C1E68690986E409C11C0,
	ButtonSwitcher__cctor_m50BC7200C20DD3E67E41592FF116A8F6E7296AEC,
	SwipeGesture_Start_mD3A7D399C033A145F74E843929C3252CE533BB46,
	SwipeGesture_Update_m15CB5B36D9C8F88DF209C9F9EB8471E1A9F7B7E9,
	SwipeGesture__ctor_mF056D731568CC4418941F4CA6196B7A5A98F6EA2,
	Create3DPipe_set_WayPoints_m50E6F11C5BED4EA7B0BF3140498CCA284F09C526,
	Create3DPipe_DrawPipeFromWaypoints_m8F23C3DCC5216B883BDB48CD2B36D60E78479657,
	Create3DPipe__ctor_m12A8AA279C1BECB267B521A518CAACE4529EE673,
	FindArmatureModel__ctor_m290A3FFA26F9B60A94760E76DC60D1C8F87E76B1,
	FindArmatureModel_GetArmatureDependingOnName_mE41B088012B3CB556CF8820AE2217F65AEC9E536,
	Redirect_Update_m7BCCA10904C2FCA79BEAD291F2C5D3B4AABF52D1,
	Redirect__ctor_m2A77D47045DBD6A24CC314E64449DFA1CE9CA7D2,
	U3CStartU3Ed__4__ctor_m8B0264798939C569742263D32E0054DBAB9AE6FF,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m3EFE2ADAD412045F666CFA1C8C9FF53AF92CBD75,
	U3CStartU3Ed__4_MoveNext_m84F94A5CD6012300AC80698CDCA870A0A146E226,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m129CB3E5CAFFA1D19D4988182EEF116F2086A637,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m345E255900454CC505A8AAE3BF6AEF3C06467EAB,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m5920F51DCC2DC7C8BC98EE95D6CD4D7784997272,
	CharacterSelectionEvent__ctor_mE2C306B8090F90261252C94D26AB5085580B11D5,
	SpriteSelectionEvent__ctor_m9D9F101CB717ACD5449336DFFF70F86AE32BB6EC,
	WordSelectionEvent__ctor_mFD7F2937426D4AA1A8CBB13F62C3CC1D2061AD1E,
	LineSelectionEvent__ctor_mA23AFEC8E11183CF472044FA72B07AD28ED6E675,
	LinkSelectionEvent__ctor_m02CC491DBE4B2FF05A8FD4285813215ED3D323E5,
	U3CStartU3Ed__10__ctor_m328932E4B6124311CD738F2F84F69BC149209129,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m209F531CE6ED7F07649497DD15817C1D7C1880A1,
	U3CStartU3Ed__10_MoveNext_mD927C85D41034011055A7CA3AFFAF4E10464F65D,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m18BA91C8A20CBD6976D52E335563D9B42C1AE9A8,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m5A67B5BDE759A157229E6CF24E653B79B2AC0200,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA521C1EFA357A9F4F4CAA68A4D0B85468764323C,
	U3CStartU3Ed__10__ctor_m9A8C7C0644996520AD443A4F7CA527BF05C54C3C,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m94D1420C08F57F2901E2499D36778BB8F1C76932,
	U3CStartU3Ed__10_MoveNext_m31AF957FFAEEED4BE0F39A1185C6112C4EB6F7AA,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39B535B222104759319A54A6D7E2E81482A1F71E,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mC2B435140D045B6A20FB105E0E2CBD625218CA74,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m9A98CCB7604AAD93919CAE48955C6A6CB8C38790,
	U3CAnimatePropertiesU3Ed__6__ctor_mB5F6ED6FCDA5BEAD56E22B64283D7A4D7F7EAE71,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m29AAE9560CA4EEB4A548A68ACA085EC9E4CB8EA5,
	U3CAnimatePropertiesU3Ed__6_MoveNext_m4F2D37B672E95820F49489611196CDE334736157,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m21D2B0A0B0CADF520D05FE4948F1DE94CF119630,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_m1E942C0FD32005FDBB182CF646FD2312BA273BC7,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mFC3602799F1D07BB002093DFB879FC759384FDD3,
	U3CWarpTextU3Ed__7__ctor_mA03118DB0FD3BF160500E127D1FACDAF45313047,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m9D7F6A90DA911D77EE72A2824FF9690CED05FBC8,
	U3CWarpTextU3Ed__7_MoveNext_m7FE0DD003507BAD92E35CC5DACE5D043ADD766ED,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m349F81ECD49FF12E4009E2E56DB81974D68C6DAD,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_m3122CE754238FB7815F7ABE8E7DFAF3AB7B03278,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m79BF250C4ADC29ACF11370E2B5BD4FFD78709565,
	U3CStartU3Ed__4__ctor_m8231909D78A27061165C450481E233339F300046,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m6886DB5D83361607B72BBCCB7D484B9C0BFE1981,
	U3CStartU3Ed__4_MoveNext_m1CD1306C9E074D3F941AC906A48D3CA97C148774,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE50AC5FA9F27773C51DD3E4188A748BA0A513F8A,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m5D6EE5C4B2C20A433129D8BFD13DFC82681346A2,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m1FA5600514131056D8198F8442F37A4A22A9F065,
	U3CRevealCharactersU3Ed__7__ctor_m48510711FC78DFEA9CF4603E1E75F4DF7C5F1489,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m5B88486625A74566DF3FC7BFB4CE327A58C57ED4,
	U3CRevealCharactersU3Ed__7_MoveNext_mE45076151810A7C1F83802B7754DE92E812EABAB,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C330834C7113C8468CC1A09417B7C521CAE833B,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_m25B719FFD0CAB1DFF2853FF47A4EE2032176E287,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9A540E1B18E93F749F4BFD4C8597AEC9F2C199F7,
	U3CRevealWordsU3Ed__8__ctor_mDF41BA5FE3D53FEC3CB8214FCA7853A1142DE70C,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m46827499CD3657AF468926B6302D2340ED975965,
	U3CRevealWordsU3Ed__8_MoveNext_mE0CB1189CFAD7F7B3E74438A4528D9BFAABB48DE,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4A3E941DF6C67BC9ACEFEAA09D11167B3F3A38EC,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_m967B427886233CDB5DFFEA323F02A89CE9330CC8,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m0C1B2941BEC04593993127F6D9DCDBA6FAE7CC20,
	U3CDisplayTextMeshProFloatingTextU3Ed__12__ctor_m13B7271203EDC80E649C1CE40F09A93BDA2633DF,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_IDisposable_Dispose_m3D1611AA38746EF0827F5260DADCC361DD56DF0C,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_MoveNext_m03111B7039F928512A7A53F8DA9C04671AA8D7EE,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55B08E77035437C2B75332F29214C33214417414,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_Reset_m6737F2D695AB295240F15C5B0B4F24A59106BFDA,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_get_Current_m6BD4D2442BDDDFB6859CFE646182580A0A1E130A,
	U3CDisplayTextMeshFloatingTextU3Ed__13__ctor_m7E4C3B87E56A7B23D725D653E52ADE02554EAE3E,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_IDisposable_Dispose_m485A7C4CF3496858A72CBA647B29BC610F39FE39,
	U3CDisplayTextMeshFloatingTextU3Ed__13_MoveNext_m6A3C88B1149D12B58E6E580BC04622F553ED1424,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2EE54B2AC8AAE44BACF8EE8954A6D824045CFC55,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_Reset_m8524C9700DEF2DE7A28BBFDB938FE159985E86EE,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_get_Current_m2293C6A327D4D1CCC1ACBC90DBE00DC1C6F39EBE,
	U3CAnimateVertexColorsU3Ed__3__ctor_m3D7543ED636AFCD2C59E834668568DB2A4005F6A,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m3F7092E831D4CFDACC5B6254958DFEC2D313D0EE,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_mC0D42DAE0A614F2B91AF1F9A2F8C0AF471CA0AE4,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6597EE639379454CADA3823A1B955FEFBAF894BD,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m9943586181F26EEE58A42138CE0489DDF07EA359,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m2D4E4AA5EEB4F07F283E61018685199A4C2D56BD,
	U3CAnimateVertexColorsU3Ed__11__ctor_mC74A801C40038DA74D856FACFBAD12F3BC3E11E7,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m07C61223FC885322B6066E81CB130879661D5A72,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m04CD6FB321DE3AD8D5890766C1F2CAAE4112EDF2,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35F5F559A34D3F9BE1E5DD636FEAF517164A6B07,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m325AE901312C158848B79B302EBA7BE847C93D49,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_mA688DA41E2C04FF9774F607794C114057FA055C6,
	U3CAnimateVertexColorsU3Ed__11__ctor_m89953000A887F8C0931B0E98B484FBAAC37748C5,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m9AFBB2A87D38A1358F9EB09D617075D72DEED19B,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m9F607EF7DBDFFC4FB2307B0EC4C7F33EEE63BBE8,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF47F7A3AB51F9BC1B8F74E10FD82B29C1B223DCD,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m99B988724C4F53F7F8334C5F633C8B1603185ADD,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_mE9127628FC1726149DA7C8FE95A7D4CFB1EE1655,
	U3CAnimateVertexColorsU3Ed__10__ctor_mBA04B89258FA2EF09266E1766AB0B815E521897A,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mE001B767DE85B4B3B86A0C080B9FC00381340A1C,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m611487BEE2BB82A9BFF5EA2157BDCA610F87876D,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A54A1BF63433F860822B43ABA9FAC6A4124409C,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m48F09B740CBEEC27459498932572D2869A1A4CBE,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mA915AC55E6DEE343235545FC1FE6F6CA5611DF3C,
	U3CU3Ec__DisplayClass10_0__ctor_m1C2F2204ADD6E4BA14E14CF255F520B7E2464941,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m673C7031DB1882DEEFB53F179E3C2FB13FB6CA5A,
	U3CAnimateVertexColorsU3Ed__10__ctor_m2F5D29C1CA797C0BCEC16C8B5D96D1CF5B07F6F3,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m1A43A8EA2FB689EE2B39D8A624580594374905B9,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_mCA826F12F72BDBB79F9B50DC9CBC6E7F80B2110F,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF0E754E9557C03F892EFA19C0307AECD6BA8C4D,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m002A7C6C8AE61BE6CD6FD0B2173C75DBF47BCC56,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mE91B03C99FCCBC8ED4E37649C5364E83D047B053,
	U3CWarpTextU3Ed__8__ctor_m845C9410F3856EF25585F59C425200EEFCEFB3C0,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m63AC2AE8BC0FCF89812A33CAF150E9D1B56BAE6A,
	U3CWarpTextU3Ed__8_MoveNext_m98D3E999A69E233C7AD5F357A0D0623D731DCDAA,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3971C0D86C5903812972245A6F872D101ACB5189,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_mF17827612AC7C91DE879114D1D6428450B9504D0,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m98A13A358D4E71D9612F68731A7AE11A3DD080DE,
	U3CU3Ec__cctor_m7C968439A45D026920C6C5D5EA65AAEA83606E34,
	U3CU3Ec__ctor_m696942DE4DFB5A7467CB313B1CD8799AE0305B7C,
	U3CU3Ec_U3CCreateMeshesU3Eb__8_0_m4D637995C58AD7110AD8C8B21CD55B89D2E2BC7E,
	Vertex__ctor_m3FB3A2AD740159C83FD82DB3DCFF580697881CEA,
	Vertex__ctor_mA4F00945EA013CC81D49C6E27F703ED1C10F3D7A,
	U3CU3Ec__cctor_mF54855846ADBDCD8620E152C3BA4B1AFFD5E9E0F,
	U3CU3Ec__ctor_m787A4B7E3084867184B5514748E2BD62DCA9205A,
	U3CU3Ec_U3CComputeU3Eb__31_0_mC45343E4EF8EC7D90B72F38CA404AB86D11A0E84,
	U3CU3Ec_U3CComputeU3Eb__31_1_mC03DC6F65E6A3B445E3328079587C23076951BBE,
	U3CU3Ec_U3CComputeU3Eb__31_2_mD24885FCBDCAC928EB1C5F5DDEB962D25848A1BA,
	U3CU3Ec__cctor_m6DBE3096D5D93F61ED3EFABD0E0928E35ECE6352,
	U3CU3Ec__ctor_m131D8F81BC128201C8B49F1FA539D2204F099C45,
	U3CU3Ec_U3CFillOnceU3Eb__25_0_mC0FC995AB7363510A1CE19C2FAAD8F629C2D0970,
	U3CU3Ec_U3CFillOnceU3Eb__25_1_mDBDE40CEFF9892DCAB0A4F8613373DCD46153AFC,
	U3CU3Ec_U3CFillRepeatU3Eb__26_0_m2E5A74EB91BF04A38755EAF1E9D7FF73C2598079,
	U3CU3Ec_U3CFillRepeatU3Eb__26_1_m414F9F219164338E88CCC5F249DA887A0BB71639,
	U3CU3Ec_U3CFillStretchU3Eb__27_0_m044AE6F02BEB6874F2E161AC4B528E6BC9B0F26C,
	U3CU3Ec_U3CFillStretchU3Eb__27_1_mE62BEE4B7056B3A23AA994D2CE7DECBC81E9521C,
	U3CU3Ec__cctor_m6D4552D449AF496E6984E9F5C4BC8F85F258ADC9,
	U3CU3Ec__ctor_mF27C1536AD315F65371F655EF08330E1FC806955,
	U3CU3Ec_U3CCreateMeshesU3Eb__16_0_mC746B56628831C50327E26DC568649EB0B5DC799,
	U3CHttpGetU3Ed__1__ctor_m57D54108B5D6778C7D0C398D073C8F6EF2CC4DF7,
	U3CHttpGetU3Ed__1_System_IDisposable_Dispose_m49664829AB4E380552E7D5EE0BD65E3DC313A210,
	U3CHttpGetU3Ed__1_MoveNext_mF39BBEA2F4457E8671DC8E306899F97867FA0ABA,
	U3CHttpGetU3Ed__1_U3CU3Em__Finally1_m07935C8658851E12B3A1DC2A92641D5C47273309,
	U3CHttpGetU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC8AFD58E65080D05C735D4CBA1636B05D287664B,
	U3CHttpGetU3Ed__1_System_Collections_IEnumerator_Reset_m4CED69761DFE7BDFC1651467750D5D596B294E4D,
	U3CHttpGetU3Ed__1_System_Collections_IEnumerator_get_Current_m2C32722CD63DCFEF8DFCCE0BF179C5F14045F306,
	U3CHttpDeleteU3Ed__2__ctor_mDF4467E0D713CBC9F6B586FFC01D56D55BB86F2A,
	U3CHttpDeleteU3Ed__2_System_IDisposable_Dispose_m388748DB132AD4CB3126992A5BC9FD3A7433B704,
	U3CHttpDeleteU3Ed__2_MoveNext_mA32A60E0E49984D7E37CF1B484DDE8AD02E54918,
	U3CHttpDeleteU3Ed__2_U3CU3Em__Finally1_mBF6925D255EA08802C17D9C47FA1F704EC0976EF,
	U3CHttpDeleteU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m63C103756875EAD5C410F846345E98A39F33A9A6,
	U3CHttpDeleteU3Ed__2_System_Collections_IEnumerator_Reset_mB26CB445F177724DD6988E41F4DDFFF08F7A4732,
	U3CHttpDeleteU3Ed__2_System_Collections_IEnumerator_get_Current_m07C4AA79AD28CC69714DC58818BE0A361409FCF7,
	U3CHttpPostU3Ed__3__ctor_m7D509067A83191FFE91CF09B515278678A88BE53,
	U3CHttpPostU3Ed__3_System_IDisposable_Dispose_mADE1BD09F179274CAD120B0BD3E73F8F454FD53A,
	U3CHttpPostU3Ed__3_MoveNext_m662DA8DC365F0A396A19B5CF0CCD6FA12E67E6D0,
	U3CHttpPostU3Ed__3_U3CU3Em__Finally1_mB9CFF4DFEC8475E6454F0DF85D7740271C03D5A6,
	U3CHttpPostU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m91767642FBE9F9692217E14DEE61ACE8537D8418,
	U3CHttpPostU3Ed__3_System_Collections_IEnumerator_Reset_m2C7B761948466DF4646737A6CD67C509BD3AEB2F,
	U3CHttpPostU3Ed__3_System_Collections_IEnumerator_get_Current_mA5D0E1D452C35DE206D6A53BA619A2A7FA4958F9,
	U3CHttpPutU3Ed__4__ctor_m2F50B7D77CE5A37976C5D63CC7D15E738F89B625,
	U3CHttpPutU3Ed__4_System_IDisposable_Dispose_m24150DE19A56E505C30DE38145F514B83EE8382B,
	U3CHttpPutU3Ed__4_MoveNext_mBD244ABDD5CCFC7D808DF6B01FA4883558A49CDF,
	U3CHttpPutU3Ed__4_U3CU3Em__Finally1_mE001D9F071D634B01C6ED5CB190E709F0C82F008,
	U3CHttpPutU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m699CB4EE20DC7442553E05DA5FD6C1B6B4F22546,
	U3CHttpPutU3Ed__4_System_Collections_IEnumerator_Reset_mB2AD79998C84296E056B941A8F163FBE6AFF20B7,
	U3CHttpPutU3Ed__4_System_Collections_IEnumerator_get_Current_m8B0E55472EE5B3E056C2C9AD5421F472C16E0333,
	U3CHttpHeadU3Ed__5__ctor_mA4D217EC377A3600BD1D0FE57A96F329DE8681C3,
	U3CHttpHeadU3Ed__5_System_IDisposable_Dispose_mAE0A40AF567CEBDB8CB083570763C35123508B8A,
	U3CHttpHeadU3Ed__5_MoveNext_m2C4BFA52CB068854B947278F6A98C98D8E05F7DA,
	U3CHttpHeadU3Ed__5_U3CU3Em__Finally1_mE5D51E1314AD7FE24C8472CEDAE4213DD0A2ACA0,
	U3CHttpHeadU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m18F1FD8934F2A5A96ECA978FD0DF490990CF5CC8,
	U3CHttpHeadU3Ed__5_System_Collections_IEnumerator_Reset_m47B2858FEACBA7688A514E223DABE2269C0183D4,
	U3CHttpHeadU3Ed__5_System_Collections_IEnumerator_get_Current_m4D321DA89DA0BAAF06ED049F36ACB4429EB3DDA6,
	U3CU3Ec__DisplayClass14_0__ctor_mCC5BC6D39808F7814535121AEE5ABCE05119DFD7,
	U3CU3Ec__DisplayClass14_0_U3CArmatureOneDropdownValueChangedU3Eb__0_m1FF82FDB237DBE3C20F37F7A011416A28E3821C8,
	U3CU3Ec__DisplayClass15_0__ctor_mCC735845FF56CC8F219F0356E6EA525EDCF06620,
	U3CU3Ec__DisplayClass15_0_U3CArmatureTwoDropdownValueChangedU3Eb__0_m87A76DA791982B20F4B17FD988BFDE693CA6C9BB,
	U3CU3Ec__cctor_mCD32F4052D5DCA2488F83ADE1F77EA74282E7484,
	U3CU3Ec__ctor_m0704454B4060DF5148CB28AABCD257ED778CE2C1,
	U3CU3Ec_U3CFillDropDownWithDataU3Eb__17_0_m323B51AAC9BACE24415F71E0EBEAB644A90A9A36,
	U3CU3Ec_U3CFillDropDownWithDataU3Eb__17_1_mFAAD20D0F7F748329A597385D7AA4612ECDE42FE,
	U3CU3Ec__DisplayClass12_0__ctor_m06A70BF29D05DCA5B848DFD8E9FE7DCA0F5F8C59,
	U3CU3Ec__DisplayClass12_0_U3CPipeTypeDropdownValueChangedU3Eb__0_m4A677BE2097122742A6F3C6E15510D541417931E,
	U3CU3Ec__cctor_m8738A4DA0168457CAFD0FDD08BC5384F67104ABC,
	U3CU3Ec__ctor_mADF95EB19A64F136EFA3469B04094B38A90BD2AA,
	U3CU3Ec_U3COnRequestCompleteU3Eb__13_1_m0744AF2BE14B54C32D3D8347D4A99BAB61AE51A9,
	U3CU3Ec_U3COnRequestCompleteU3Eb__13_2_m6E03B7C3DE1FD8F94CA23CF8983AAA8E97926D26,
	U3CU3Ec_U3COnRequestCompleteU3Eb__13_3_m7416AE4D4F7A69CE6EEEC838D7D2D264E78B318B,
	U3CU3Ec__cctor_mD4F25E26E4EB3A33C54B1549C95592EF319A9560,
	U3CU3Ec__ctor_mEFDF2424AFF97BF99B58BBBF0427AA4C80CB0C6F,
	U3CU3Ec_U3CDrawPipeU3Eb__24_0_mC39F2087C3CF55D79D1C51A1C5FD467FFAD6EDDB,
	LoadingMonoBehaviour__ctor_m3DCBD199B32CBBE0610F74E6C22643BFD0DF2539,
	U3CU3Ec__DisplayClass4_0__ctor_m74FAA460B03BB06E2961735B3435DB380F13EB44,
	U3CU3Ec__DisplayClass4_0_U3CLoadU3Eb__0_mA2971131149CCFEC0E01CA07A6A512C94FEA4829,
	U3CSetActiveU3Ed__6__ctor_m81ACD5EE7022F82CFA0A0FD0956421B57B9C4B7D,
	U3CSetActiveU3Ed__6_System_IDisposable_Dispose_mAEB6CF32BD97C3042986C0D647F2B341F62DB8AC,
	U3CSetActiveU3Ed__6_MoveNext_m37B35356F15F93B6AF4692600E53BDF039F22F80,
	U3CSetActiveU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m38A6639AA4F908C4367B876D0C9CBF047E1B48DD,
	U3CSetActiveU3Ed__6_System_Collections_IEnumerator_Reset_m02B300ACA6ED1F151062AB5C3CFB6F09FC32FF86,
	U3CSetActiveU3Ed__6_System_Collections_IEnumerator_get_Current_m7B67385E706B00A1FEB3BD44824288448E2D071D,
	U3CLoadSceneAsyncU3Ed__8__ctor_m6047B94C7A8011CE80C28FE67F1DE1FDD6B13D7F,
	U3CLoadSceneAsyncU3Ed__8_System_IDisposable_Dispose_m91FFE31DDB26C503FD5790094CB3F2200EDACD93,
	U3CLoadSceneAsyncU3Ed__8_MoveNext_m1DED49F10A337A0C12435EBFD8560931083EF52C,
	U3CLoadSceneAsyncU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7CC1BC0C97A47A7F4E3ABA728FA9DC6FB098DD20,
	U3CLoadSceneAsyncU3Ed__8_System_Collections_IEnumerator_Reset_m8CF831F3429473C0CD2195F27B02F5A8FE2B3129,
	U3CLoadSceneAsyncU3Ed__8_System_Collections_IEnumerator_get_Current_m885A63CBCB0B2254290645F2B81F05EACC950C88,
	U3CU3Ec__DisplayClass4_0__ctor_m7BD16E395EE70B11119E37B755EB1FB45B438079,
	U3CU3Ec__DisplayClass4_0_U3CGetArmatureDependingOnNameU3Eb__2_m57FCBDAB61AAEBE628247A8F021B4AA45F33522B,
	U3CU3Ec__cctor_mF7F04136E6109C63ACE6A52FF0DA47226B80ABFC,
	U3CU3Ec__ctor_m2EA6A9C7CA87FC516C96AE04E9FF589D7BB9FF90,
	U3CU3Ec_U3CGetArmatureDependingOnNameU3Eb__4_0_mE89C1AFFC0964CF1FB52A96F0DF92B15C5442FB4,
	U3CU3Ec_U3CGetArmatureDependingOnNameU3Eb__4_1_mDE313AE457A61CB8075BAEF4FFD84E652C7454F6,
	U3CU3Ec_U3CGetArmatureDependingOnNameU3Eb__4_3_m90F7645184DD9284517BB089D7481A8B06561462,
};
static const int32_t s_InvokerIndices[715] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	706,
	324,
	23,
	23,
	23,
	2130,
	26,
	23,
	3,
	14,
	26,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	14,
	23,
	2023,
	23,
	2023,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	23,
	26,
	26,
	484,
	484,
	35,
	35,
	118,
	23,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	484,
	484,
	35,
	35,
	118,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	26,
	23,
	26,
	26,
	26,
	26,
	32,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	32,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	28,
	28,
	23,
	23,
	23,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	28,
	14,
	23,
	706,
	324,
	27,
	26,
	26,
	1402,
	1409,
	1409,
	1409,
	2210,
	1390,
	27,
	2211,
	2212,
	2212,
	2213,
	2214,
	1559,
	2215,
	9,
	10,
	2216,
	2216,
	2217,
	28,
	89,
	31,
	26,
	26,
	23,
	23,
	14,
	26,
	23,
	2212,
	1357,
	471,
	23,
	2212,
	26,
	62,
	26,
	23,
	27,
	27,
	2214,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	1402,
	1403,
	1402,
	1403,
	1402,
	1403,
	1413,
	1558,
	706,
	324,
	1405,
	26,
	26,
	14,
	23,
	23,
	23,
	27,
	27,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	706,
	324,
	706,
	324,
	706,
	324,
	23,
	26,
	1542,
	23,
	23,
	23,
	14,
	23,
	23,
	2220,
	2221,
	10,
	32,
	26,
	1542,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	2222,
	1405,
	14,
	14,
	14,
	706,
	706,
	26,
	2221,
	2223,
	2224,
	2225,
	2226,
	2224,
	2225,
	23,
	9,
	10,
	2227,
	2227,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	23,
	23,
	23,
	23,
	28,
	23,
	27,
	2228,
	0,
	2229,
	23,
	2,
	1,
	154,
	154,
	154,
	23,
	23,
	-1,
	-1,
	14,
	26,
	14,
	26,
	23,
	172,
	200,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	105,
	105,
	125,
	125,
	105,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	31,
	26,
	26,
	23,
	26,
	23,
	23,
	26,
	9,
	9,
	32,
	32,
	23,
	23,
	26,
	26,
	26,
	23,
	23,
	23,
	26,
	9,
	32,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	89,
	26,
	89,
	445,
	445,
	28,
	23,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	27,
	23,
	27,
	1388,
	23,
	26,
	27,
	23,
	23,
	23,
	23,
	23,
	164,
	164,
	0,
	164,
	43,
	1426,
	94,
	3,
	23,
	23,
	26,
	23,
	23,
	23,
	324,
	23,
	23,
	3,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	28,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	56,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	3,
	23,
	28,
	2218,
	26,
	3,
	23,
	2219,
	2219,
	2047,
	3,
	23,
	2219,
	2219,
	2219,
	2219,
	2219,
	2219,
	3,
	23,
	28,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	23,
	9,
	23,
	9,
	3,
	23,
	41,
	28,
	23,
	9,
	3,
	23,
	28,
	41,
	28,
	3,
	23,
	2219,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	9,
	3,
	23,
	2230,
	2231,
	2232,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x02000002, { 0, 11 } },
	{ 0x02000042, { 11, 5 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[16] = 
{
	{ (Il2CppRGCTXDataType)2, 23620 },
	{ (Il2CppRGCTXDataType)3, 17598 },
	{ (Il2CppRGCTXDataType)2, 23621 },
	{ (Il2CppRGCTXDataType)3, 17599 },
	{ (Il2CppRGCTXDataType)3, 17600 },
	{ (Il2CppRGCTXDataType)2, 23622 },
	{ (Il2CppRGCTXDataType)3, 17601 },
	{ (Il2CppRGCTXDataType)3, 17602 },
	{ (Il2CppRGCTXDataType)3, 17603 },
	{ (Il2CppRGCTXDataType)2, 22937 },
	{ (Il2CppRGCTXDataType)2, 22938 },
	{ (Il2CppRGCTXDataType)2, 23623 },
	{ (Il2CppRGCTXDataType)2, 23224 },
	{ (Il2CppRGCTXDataType)1, 23224 },
	{ (Il2CppRGCTXDataType)2, 23624 },
	{ (Il2CppRGCTXDataType)3, 17604 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	715,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	16,
	s_rgctxValues,
	NULL,
};
