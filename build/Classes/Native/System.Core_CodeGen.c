﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B (void);
// 0x00000002 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64 (void);
// 0x00000003 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712 (void);
// 0x00000004 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E (void);
// 0x00000005 System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722 (void);
// 0x00000006 System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088 (void);
// 0x00000007 System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC (void);
// 0x00000008 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6 (void);
// 0x00000009 System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349 (void);
// 0x0000000A System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA (void);
// 0x0000000B System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8 (void);
// 0x0000000C System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0 (void);
// 0x0000000D System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23 (void);
// 0x0000000E System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD (void);
// 0x0000000F System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1 (void);
// 0x00000010 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4 (void);
// 0x00000011 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91 (void);
// 0x00000012 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED (void);
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC (void);
// 0x00000014 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146 (void);
// 0x00000015 System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E (void);
// 0x00000016 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC (void);
// 0x00000017 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586 (void);
// 0x00000018 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139 (void);
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672 (void);
// 0x0000001A System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F (void);
// 0x0000001B System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C (void);
// 0x0000001C System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A (void);
// 0x0000001D System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700 (void);
// 0x0000001E System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA (void);
// 0x0000001F System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B (void);
// 0x00000020 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F (void);
// 0x00000021 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F (void);
// 0x00000022 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA (void);
// 0x00000023 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438 (void);
// 0x00000024 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22 (void);
// 0x00000025 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44 (void);
// 0x00000026 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA (void);
// 0x00000027 System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F (void);
// 0x00000028 System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD (void);
// 0x00000029 System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA (void);
// 0x0000002A System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24 (void);
// 0x0000002B System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA (void);
// 0x0000002C System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463 (void);
// 0x0000002D System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325 (void);
// 0x0000002E System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x0000002F System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000030 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 (void);
// 0x00000031 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000032 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000033 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000034 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000035 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TCollection>>,System.Func`3<TSource,TCollection,TResult>)
// 0x00000036 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TCollection>>,System.Func`3<TSource,TCollection,TResult>)
// 0x00000037 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000038 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000039 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Except(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ExceptIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000003B TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003C System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x0000003E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x0000003F TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000040 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000041 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000042 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000043 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000044 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000045 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000046 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000047 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000048 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000049 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x0000004A System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x0000004B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000004C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000004D System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000004E System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000004F System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000050 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000051 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000052 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000053 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000054 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000055 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000056 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000057 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000058 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000059 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000005A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000005B System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000005C System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x0000005D System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000005E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000005F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000060 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000061 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x00000062 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x00000063 System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000064 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000065 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000066 System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000067 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000068 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000069 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000006A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000006B System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000006C System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x0000006D System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x0000006E System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000006F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000070 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000071 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000072 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x00000073 TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000074 System.Void System.Linq.Enumerable_<SelectManyIterator>d__23`3::.ctor(System.Int32)
// 0x00000075 System.Void System.Linq.Enumerable_<SelectManyIterator>d__23`3::System.IDisposable.Dispose()
// 0x00000076 System.Boolean System.Linq.Enumerable_<SelectManyIterator>d__23`3::MoveNext()
// 0x00000077 System.Void System.Linq.Enumerable_<SelectManyIterator>d__23`3::<>m__Finally1()
// 0x00000078 System.Void System.Linq.Enumerable_<SelectManyIterator>d__23`3::<>m__Finally2()
// 0x00000079 TResult System.Linq.Enumerable_<SelectManyIterator>d__23`3::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x0000007A System.Void System.Linq.Enumerable_<SelectManyIterator>d__23`3::System.Collections.IEnumerator.Reset()
// 0x0000007B System.Object System.Linq.Enumerable_<SelectManyIterator>d__23`3::System.Collections.IEnumerator.get_Current()
// 0x0000007C System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectManyIterator>d__23`3::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x0000007D System.Collections.IEnumerator System.Linq.Enumerable_<SelectManyIterator>d__23`3::System.Collections.IEnumerable.GetEnumerator()
// 0x0000007E System.Void System.Linq.Enumerable_<ExceptIterator>d__77`1::.ctor(System.Int32)
// 0x0000007F System.Void System.Linq.Enumerable_<ExceptIterator>d__77`1::System.IDisposable.Dispose()
// 0x00000080 System.Boolean System.Linq.Enumerable_<ExceptIterator>d__77`1::MoveNext()
// 0x00000081 System.Void System.Linq.Enumerable_<ExceptIterator>d__77`1::<>m__Finally1()
// 0x00000082 TSource System.Linq.Enumerable_<ExceptIterator>d__77`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000083 System.Void System.Linq.Enumerable_<ExceptIterator>d__77`1::System.Collections.IEnumerator.Reset()
// 0x00000084 System.Object System.Linq.Enumerable_<ExceptIterator>d__77`1::System.Collections.IEnumerator.get_Current()
// 0x00000085 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<ExceptIterator>d__77`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000086 System.Collections.IEnumerator System.Linq.Enumerable_<ExceptIterator>d__77`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000087 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::.ctor(System.Int32)
// 0x00000088 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x00000089 System.Boolean System.Linq.Enumerable_<CastIterator>d__99`1::MoveNext()
// 0x0000008A System.Void System.Linq.Enumerable_<CastIterator>d__99`1::<>m__Finally1()
// 0x0000008B TResult System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x0000008C System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x0000008D System.Object System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x0000008E System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x0000008F System.Collections.IEnumerator System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000090 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000091 System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x00000092 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x00000093 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x00000094 System.Void System.Linq.Set`1::Resize()
// 0x00000095 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x00000096 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x00000097 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000098 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000099 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000009A System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x0000009B System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x0000009C System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x0000009D System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x0000009E TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x0000009F System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x000000A0 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x000000A1 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000A2 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000A3 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x000000A4 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x000000A5 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x000000A6 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x000000A7 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x000000A8 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x000000A9 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x000000AA System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x000000AB System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x000000AC TElement[] System.Linq.Buffer`1::ToArray()
// 0x000000AD System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x000000AE System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000AF System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000B0 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x000000B1 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x000000B2 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x000000B3 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x000000B4 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x000000B5 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x000000B6 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x000000B7 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x000000B8 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000B9 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000BA System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000BB System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x000000BC System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x000000BD System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x000000BE System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x000000BF System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x000000C0 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000C1 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000C2 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000C3 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000C4 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000C5 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x000000C6 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x000000C7 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x000000C8 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000C9 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[201] = 
{
	SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B,
	AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64,
	AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712,
	AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E,
	AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722,
	AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088,
	AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC,
	AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6,
	AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349,
	AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA,
	AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8,
	AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0,
	AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23,
	AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD,
	AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1,
	AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4,
	AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91,
	AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED,
	AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC,
	AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146,
	AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E,
	AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC,
	AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586,
	AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139,
	AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672,
	AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F,
	AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C,
	AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A,
	AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700,
	AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA,
	AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B,
	AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F,
	AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F,
	AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA,
	AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438,
	AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22,
	AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44,
	AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA,
	AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F,
	AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD,
	AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA,
	AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24,
	AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA,
	AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463,
	AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325,
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[201] = 
{
	0,
	23,
	10,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	105,
	14,
	105,
	31,
	23,
	23,
	23,
	23,
	23,
	105,
	105,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	10,
	32,
	10,
	32,
	14,
	14,
	31,
	886,
	27,
	37,
	197,
	197,
	3,
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[48] = 
{
	{ 0x02000008, { 66, 4 } },
	{ 0x02000009, { 70, 9 } },
	{ 0x0200000A, { 81, 7 } },
	{ 0x0200000B, { 90, 10 } },
	{ 0x0200000C, { 102, 11 } },
	{ 0x0200000D, { 116, 9 } },
	{ 0x0200000E, { 128, 12 } },
	{ 0x0200000F, { 143, 1 } },
	{ 0x02000010, { 144, 2 } },
	{ 0x02000011, { 146, 13 } },
	{ 0x02000012, { 159, 11 } },
	{ 0x02000013, { 170, 6 } },
	{ 0x02000015, { 176, 8 } },
	{ 0x02000017, { 184, 3 } },
	{ 0x02000018, { 189, 5 } },
	{ 0x02000019, { 194, 7 } },
	{ 0x0200001A, { 201, 3 } },
	{ 0x0200001B, { 204, 7 } },
	{ 0x0200001C, { 211, 4 } },
	{ 0x0200001D, { 215, 21 } },
	{ 0x0200001F, { 236, 2 } },
	{ 0x06000031, { 0, 10 } },
	{ 0x06000032, { 10, 10 } },
	{ 0x06000033, { 20, 5 } },
	{ 0x06000034, { 25, 5 } },
	{ 0x06000035, { 30, 1 } },
	{ 0x06000036, { 31, 2 } },
	{ 0x06000037, { 33, 2 } },
	{ 0x06000038, { 35, 1 } },
	{ 0x06000039, { 36, 1 } },
	{ 0x0600003A, { 37, 2 } },
	{ 0x0600003B, { 39, 3 } },
	{ 0x0600003C, { 42, 2 } },
	{ 0x0600003D, { 44, 2 } },
	{ 0x0600003E, { 46, 2 } },
	{ 0x0600003F, { 48, 4 } },
	{ 0x06000040, { 52, 3 } },
	{ 0x06000041, { 55, 1 } },
	{ 0x06000042, { 56, 3 } },
	{ 0x06000043, { 59, 2 } },
	{ 0x06000044, { 61, 5 } },
	{ 0x06000054, { 79, 2 } },
	{ 0x06000059, { 88, 2 } },
	{ 0x0600005E, { 100, 2 } },
	{ 0x06000064, { 113, 3 } },
	{ 0x06000069, { 125, 3 } },
	{ 0x0600006E, { 140, 3 } },
	{ 0x06000099, { 187, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[238] = 
{
	{ (Il2CppRGCTXDataType)2, 23484 },
	{ (Il2CppRGCTXDataType)3, 17122 },
	{ (Il2CppRGCTXDataType)2, 23485 },
	{ (Il2CppRGCTXDataType)2, 23486 },
	{ (Il2CppRGCTXDataType)3, 17123 },
	{ (Il2CppRGCTXDataType)2, 23487 },
	{ (Il2CppRGCTXDataType)2, 23488 },
	{ (Il2CppRGCTXDataType)3, 17124 },
	{ (Il2CppRGCTXDataType)2, 23489 },
	{ (Il2CppRGCTXDataType)3, 17125 },
	{ (Il2CppRGCTXDataType)2, 23490 },
	{ (Il2CppRGCTXDataType)3, 17126 },
	{ (Il2CppRGCTXDataType)2, 23491 },
	{ (Il2CppRGCTXDataType)2, 23492 },
	{ (Il2CppRGCTXDataType)3, 17127 },
	{ (Il2CppRGCTXDataType)2, 23493 },
	{ (Il2CppRGCTXDataType)2, 23494 },
	{ (Il2CppRGCTXDataType)3, 17128 },
	{ (Il2CppRGCTXDataType)2, 23495 },
	{ (Il2CppRGCTXDataType)3, 17129 },
	{ (Il2CppRGCTXDataType)2, 23496 },
	{ (Il2CppRGCTXDataType)3, 17130 },
	{ (Il2CppRGCTXDataType)3, 17131 },
	{ (Il2CppRGCTXDataType)2, 17030 },
	{ (Il2CppRGCTXDataType)3, 17132 },
	{ (Il2CppRGCTXDataType)2, 23497 },
	{ (Il2CppRGCTXDataType)3, 17133 },
	{ (Il2CppRGCTXDataType)3, 17134 },
	{ (Il2CppRGCTXDataType)2, 17037 },
	{ (Il2CppRGCTXDataType)3, 17135 },
	{ (Il2CppRGCTXDataType)3, 17136 },
	{ (Il2CppRGCTXDataType)2, 23498 },
	{ (Il2CppRGCTXDataType)3, 17137 },
	{ (Il2CppRGCTXDataType)2, 23499 },
	{ (Il2CppRGCTXDataType)3, 17138 },
	{ (Il2CppRGCTXDataType)3, 17139 },
	{ (Il2CppRGCTXDataType)3, 17140 },
	{ (Il2CppRGCTXDataType)2, 23500 },
	{ (Il2CppRGCTXDataType)3, 17141 },
	{ (Il2CppRGCTXDataType)2, 23501 },
	{ (Il2CppRGCTXDataType)3, 17142 },
	{ (Il2CppRGCTXDataType)3, 17143 },
	{ (Il2CppRGCTXDataType)2, 17073 },
	{ (Il2CppRGCTXDataType)3, 17144 },
	{ (Il2CppRGCTXDataType)2, 17074 },
	{ (Il2CppRGCTXDataType)3, 17145 },
	{ (Il2CppRGCTXDataType)2, 23502 },
	{ (Il2CppRGCTXDataType)3, 17146 },
	{ (Il2CppRGCTXDataType)2, 23503 },
	{ (Il2CppRGCTXDataType)2, 23504 },
	{ (Il2CppRGCTXDataType)2, 17078 },
	{ (Il2CppRGCTXDataType)2, 23505 },
	{ (Il2CppRGCTXDataType)2, 17080 },
	{ (Il2CppRGCTXDataType)2, 23506 },
	{ (Il2CppRGCTXDataType)3, 17147 },
	{ (Il2CppRGCTXDataType)2, 17083 },
	{ (Il2CppRGCTXDataType)2, 17085 },
	{ (Il2CppRGCTXDataType)2, 23507 },
	{ (Il2CppRGCTXDataType)3, 17148 },
	{ (Il2CppRGCTXDataType)2, 23508 },
	{ (Il2CppRGCTXDataType)3, 17149 },
	{ (Il2CppRGCTXDataType)3, 17150 },
	{ (Il2CppRGCTXDataType)2, 23509 },
	{ (Il2CppRGCTXDataType)2, 17090 },
	{ (Il2CppRGCTXDataType)2, 23510 },
	{ (Il2CppRGCTXDataType)2, 17092 },
	{ (Il2CppRGCTXDataType)3, 17151 },
	{ (Il2CppRGCTXDataType)3, 17152 },
	{ (Il2CppRGCTXDataType)2, 17095 },
	{ (Il2CppRGCTXDataType)3, 17153 },
	{ (Il2CppRGCTXDataType)3, 17154 },
	{ (Il2CppRGCTXDataType)2, 17107 },
	{ (Il2CppRGCTXDataType)2, 23511 },
	{ (Il2CppRGCTXDataType)3, 17155 },
	{ (Il2CppRGCTXDataType)3, 17156 },
	{ (Il2CppRGCTXDataType)2, 17109 },
	{ (Il2CppRGCTXDataType)2, 23386 },
	{ (Il2CppRGCTXDataType)3, 17157 },
	{ (Il2CppRGCTXDataType)3, 17158 },
	{ (Il2CppRGCTXDataType)2, 23512 },
	{ (Il2CppRGCTXDataType)3, 17159 },
	{ (Il2CppRGCTXDataType)3, 17160 },
	{ (Il2CppRGCTXDataType)2, 17119 },
	{ (Il2CppRGCTXDataType)2, 23513 },
	{ (Il2CppRGCTXDataType)3, 17161 },
	{ (Il2CppRGCTXDataType)3, 17162 },
	{ (Il2CppRGCTXDataType)3, 16584 },
	{ (Il2CppRGCTXDataType)3, 17163 },
	{ (Il2CppRGCTXDataType)2, 23514 },
	{ (Il2CppRGCTXDataType)3, 17164 },
	{ (Il2CppRGCTXDataType)3, 17165 },
	{ (Il2CppRGCTXDataType)2, 17131 },
	{ (Il2CppRGCTXDataType)2, 23515 },
	{ (Il2CppRGCTXDataType)3, 17166 },
	{ (Il2CppRGCTXDataType)3, 17167 },
	{ (Il2CppRGCTXDataType)3, 17168 },
	{ (Il2CppRGCTXDataType)3, 17169 },
	{ (Il2CppRGCTXDataType)3, 17170 },
	{ (Il2CppRGCTXDataType)3, 16590 },
	{ (Il2CppRGCTXDataType)3, 17171 },
	{ (Il2CppRGCTXDataType)2, 23516 },
	{ (Il2CppRGCTXDataType)3, 17172 },
	{ (Il2CppRGCTXDataType)3, 17173 },
	{ (Il2CppRGCTXDataType)2, 17144 },
	{ (Il2CppRGCTXDataType)2, 23517 },
	{ (Il2CppRGCTXDataType)3, 17174 },
	{ (Il2CppRGCTXDataType)3, 17175 },
	{ (Il2CppRGCTXDataType)2, 17146 },
	{ (Il2CppRGCTXDataType)2, 23518 },
	{ (Il2CppRGCTXDataType)3, 17176 },
	{ (Il2CppRGCTXDataType)3, 17177 },
	{ (Il2CppRGCTXDataType)2, 23519 },
	{ (Il2CppRGCTXDataType)3, 17178 },
	{ (Il2CppRGCTXDataType)3, 17179 },
	{ (Il2CppRGCTXDataType)2, 23520 },
	{ (Il2CppRGCTXDataType)3, 17180 },
	{ (Il2CppRGCTXDataType)3, 17181 },
	{ (Il2CppRGCTXDataType)2, 17161 },
	{ (Il2CppRGCTXDataType)2, 23521 },
	{ (Il2CppRGCTXDataType)3, 17182 },
	{ (Il2CppRGCTXDataType)3, 17183 },
	{ (Il2CppRGCTXDataType)3, 17184 },
	{ (Il2CppRGCTXDataType)3, 16601 },
	{ (Il2CppRGCTXDataType)2, 23522 },
	{ (Il2CppRGCTXDataType)3, 17185 },
	{ (Il2CppRGCTXDataType)3, 17186 },
	{ (Il2CppRGCTXDataType)2, 23523 },
	{ (Il2CppRGCTXDataType)3, 17187 },
	{ (Il2CppRGCTXDataType)3, 17188 },
	{ (Il2CppRGCTXDataType)2, 17177 },
	{ (Il2CppRGCTXDataType)2, 23524 },
	{ (Il2CppRGCTXDataType)3, 17189 },
	{ (Il2CppRGCTXDataType)3, 17190 },
	{ (Il2CppRGCTXDataType)3, 17191 },
	{ (Il2CppRGCTXDataType)3, 17192 },
	{ (Il2CppRGCTXDataType)3, 17193 },
	{ (Il2CppRGCTXDataType)3, 17194 },
	{ (Il2CppRGCTXDataType)3, 16607 },
	{ (Il2CppRGCTXDataType)2, 23525 },
	{ (Il2CppRGCTXDataType)3, 17195 },
	{ (Il2CppRGCTXDataType)3, 17196 },
	{ (Il2CppRGCTXDataType)2, 23526 },
	{ (Il2CppRGCTXDataType)3, 17197 },
	{ (Il2CppRGCTXDataType)3, 17198 },
	{ (Il2CppRGCTXDataType)3, 17199 },
	{ (Il2CppRGCTXDataType)3, 17200 },
	{ (Il2CppRGCTXDataType)3, 17201 },
	{ (Il2CppRGCTXDataType)3, 17202 },
	{ (Il2CppRGCTXDataType)2, 23527 },
	{ (Il2CppRGCTXDataType)2, 23528 },
	{ (Il2CppRGCTXDataType)3, 17203 },
	{ (Il2CppRGCTXDataType)2, 17212 },
	{ (Il2CppRGCTXDataType)2, 23529 },
	{ (Il2CppRGCTXDataType)3, 17204 },
	{ (Il2CppRGCTXDataType)3, 17205 },
	{ (Il2CppRGCTXDataType)2, 17205 },
	{ (Il2CppRGCTXDataType)2, 23530 },
	{ (Il2CppRGCTXDataType)3, 17206 },
	{ (Il2CppRGCTXDataType)3, 17207 },
	{ (Il2CppRGCTXDataType)3, 17208 },
	{ (Il2CppRGCTXDataType)2, 23531 },
	{ (Il2CppRGCTXDataType)3, 17209 },
	{ (Il2CppRGCTXDataType)2, 17233 },
	{ (Il2CppRGCTXDataType)2, 17225 },
	{ (Il2CppRGCTXDataType)3, 17210 },
	{ (Il2CppRGCTXDataType)3, 17211 },
	{ (Il2CppRGCTXDataType)2, 17224 },
	{ (Il2CppRGCTXDataType)2, 23532 },
	{ (Il2CppRGCTXDataType)3, 17212 },
	{ (Il2CppRGCTXDataType)3, 17213 },
	{ (Il2CppRGCTXDataType)3, 17214 },
	{ (Il2CppRGCTXDataType)2, 17237 },
	{ (Il2CppRGCTXDataType)3, 17215 },
	{ (Il2CppRGCTXDataType)2, 23533 },
	{ (Il2CppRGCTXDataType)3, 17216 },
	{ (Il2CppRGCTXDataType)3, 17217 },
	{ (Il2CppRGCTXDataType)3, 17218 },
	{ (Il2CppRGCTXDataType)2, 23534 },
	{ (Il2CppRGCTXDataType)2, 23535 },
	{ (Il2CppRGCTXDataType)3, 17219 },
	{ (Il2CppRGCTXDataType)3, 17220 },
	{ (Il2CppRGCTXDataType)2, 17253 },
	{ (Il2CppRGCTXDataType)3, 17221 },
	{ (Il2CppRGCTXDataType)2, 17254 },
	{ (Il2CppRGCTXDataType)2, 23536 },
	{ (Il2CppRGCTXDataType)3, 17222 },
	{ (Il2CppRGCTXDataType)3, 17223 },
	{ (Il2CppRGCTXDataType)2, 23537 },
	{ (Il2CppRGCTXDataType)3, 17224 },
	{ (Il2CppRGCTXDataType)2, 23538 },
	{ (Il2CppRGCTXDataType)3, 17225 },
	{ (Il2CppRGCTXDataType)3, 17226 },
	{ (Il2CppRGCTXDataType)3, 17227 },
	{ (Il2CppRGCTXDataType)2, 17273 },
	{ (Il2CppRGCTXDataType)3, 17228 },
	{ (Il2CppRGCTXDataType)2, 17281 },
	{ (Il2CppRGCTXDataType)3, 17229 },
	{ (Il2CppRGCTXDataType)2, 23539 },
	{ (Il2CppRGCTXDataType)2, 23540 },
	{ (Il2CppRGCTXDataType)3, 17230 },
	{ (Il2CppRGCTXDataType)3, 17231 },
	{ (Il2CppRGCTXDataType)3, 17232 },
	{ (Il2CppRGCTXDataType)3, 17233 },
	{ (Il2CppRGCTXDataType)3, 17234 },
	{ (Il2CppRGCTXDataType)3, 17235 },
	{ (Il2CppRGCTXDataType)2, 17297 },
	{ (Il2CppRGCTXDataType)2, 23541 },
	{ (Il2CppRGCTXDataType)3, 17236 },
	{ (Il2CppRGCTXDataType)3, 17237 },
	{ (Il2CppRGCTXDataType)2, 17301 },
	{ (Il2CppRGCTXDataType)3, 17238 },
	{ (Il2CppRGCTXDataType)2, 23542 },
	{ (Il2CppRGCTXDataType)2, 17311 },
	{ (Il2CppRGCTXDataType)2, 17309 },
	{ (Il2CppRGCTXDataType)2, 23543 },
	{ (Il2CppRGCTXDataType)3, 17239 },
	{ (Il2CppRGCTXDataType)2, 23544 },
	{ (Il2CppRGCTXDataType)3, 17240 },
	{ (Il2CppRGCTXDataType)3, 17241 },
	{ (Il2CppRGCTXDataType)3, 17242 },
	{ (Il2CppRGCTXDataType)2, 17315 },
	{ (Il2CppRGCTXDataType)3, 17243 },
	{ (Il2CppRGCTXDataType)3, 17244 },
	{ (Il2CppRGCTXDataType)2, 17318 },
	{ (Il2CppRGCTXDataType)3, 17245 },
	{ (Il2CppRGCTXDataType)1, 23545 },
	{ (Il2CppRGCTXDataType)2, 17317 },
	{ (Il2CppRGCTXDataType)3, 17246 },
	{ (Il2CppRGCTXDataType)1, 17317 },
	{ (Il2CppRGCTXDataType)1, 17315 },
	{ (Il2CppRGCTXDataType)2, 23546 },
	{ (Il2CppRGCTXDataType)2, 17317 },
	{ (Il2CppRGCTXDataType)3, 17247 },
	{ (Il2CppRGCTXDataType)3, 17248 },
	{ (Il2CppRGCTXDataType)3, 17249 },
	{ (Il2CppRGCTXDataType)2, 17316 },
	{ (Il2CppRGCTXDataType)3, 17250 },
	{ (Il2CppRGCTXDataType)2, 17329 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	201,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	48,
	s_rgctxIndices,
	238,
	s_rgctxValues,
	NULL,
};
