using System.Collections.Generic;
using SplineMesh;
using UnityEngine;

namespace Helper
{
    public class Create3DPipe
    {
        private List<Vector3> _wayPoints = new List<Vector3>();

        public List<Vector3> WayPoints
        {
            set => _wayPoints = value;
        }

        public void DrawPipeFromWaypoints(Spline spline)
        {
            spline.Reset();
            for (var i = 0; i < _wayPoints.Count; i++)
            {
                if (i < 2)
                {
                    spline.nodes[i].Position = _wayPoints[i];
                    spline.nodes[i].Direction = _wayPoints[i] + new Vector3(0.1f, 0.1f, 0.1f);
                }
                else
                {
                    spline.AddNode(new SplineNode(_wayPoints[i], _wayPoints[i] + new Vector3(0.1f, 0.1f, 0.1f)));
                }
            }
        }
    }
}