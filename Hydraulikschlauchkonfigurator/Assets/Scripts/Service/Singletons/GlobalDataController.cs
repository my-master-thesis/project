﻿using Service.Models;
using UnityEngine;

namespace Service.Singletons
{
    public class GlobalDataController : MonoBehaviour
    {
        public static GlobalDataController Instance;

        public string baseUrl = "http://192.168.178.73:3000/";

        public PipeData pipe = new PipeData();
        public ArmatureData armatureOne = new ArmatureData();
        public ArmatureData armatureTwo = new ArmatureData();
        public int length = 0;
        public int angle = 0;

        private void Awake ()   
        {
            if (Instance == null)
            {
                DontDestroyOnLoad(gameObject);
                Instance = this;
            }
            else if (Instance != this)
            {
                Destroy (gameObject);
            }
        }
    }  
}
