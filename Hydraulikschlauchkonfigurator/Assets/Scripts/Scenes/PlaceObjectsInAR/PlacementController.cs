﻿using Scenes.PlaceObjectsInAR.Armature;
using Scenes.PlaceObjectsInAR.Pipe;
using Service.Models;
using Service.Singletons;
using UnityEngine;

namespace Scenes.PlaceObjectsInAR
{
    public class PlacementController : MonoBehaviour
    {
        public static bool DataCheckPassed;
        
        [SerializeField] private Canvas placePipeCanvas;
        [SerializeField] private Canvas placeArmatureCanvas;
        
        private PipeData _selectedPipe = new PipeData();
        private readonly ArmatureData[] _selectedArmatures = new ArmatureData[2];
        private bool _pipeIsActive;

        private void Start()
        {
            DataCheckPassed = false;
        }

        private void Update()
        {
            if (!DataCheckPassed)
            {
                DataCheck();
            }
        }

        public void NegateCanvasVisibility()
        {
            if (_pipeIsActive)
            {
                placePipeCanvas.enabled = !placePipeCanvas.enabled;
            }
            else
            {
                placeArmatureCanvas.enabled = !placeArmatureCanvas.enabled;
            }
        }

        private void DataCheck()
        {
            _selectedPipe = GlobalDataController.Instance.pipe;
            _selectedArmatures[0] = GlobalDataController.Instance.armatureOne;
            _selectedArmatures[1] = GlobalDataController.Instance.armatureTwo;
            if (_selectedPipe._id != "" && _selectedArmatures[0]._id == "" || _selectedArmatures[0]._id == null)
            {
                GetComponent<PlacePipe>().enabled = true;
                GetComponent<PlaceArmatures>().enabled = false;
                placePipeCanvas.enabled = true;
                placeArmatureCanvas.enabled = false;
                _pipeIsActive = true;
            }
            else if (_selectedPipe._id != "" &&
                     _selectedArmatures[0]._id != "" &&
                     _selectedArmatures[1]._id != "")
            {
                GetComponent<PlacePipe>().enabled = false;
                GetComponent<PlaceArmatures>().enabled = true;
                placePipeCanvas.enabled = false;
                placeArmatureCanvas.enabled = true;
                _pipeIsActive = false;
            }

            DataCheckPassed = true;
        }
    }
}
