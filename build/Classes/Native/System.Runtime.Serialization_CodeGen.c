﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void System.Runtime.Serialization.KnownTypeAttribute::.ctor(System.Type)
extern void KnownTypeAttribute__ctor_m30087B9F58E0A9A2C13141D6D5C67D93600AF500 (void);
static Il2CppMethodPointer s_methodPointers[1] = 
{
	KnownTypeAttribute__ctor_m30087B9F58E0A9A2C13141D6D5C67D93600AF500,
};
static const int32_t s_InvokerIndices[1] = 
{
	26,
};
extern const Il2CppCodeGenModule g_System_Runtime_SerializationCodeGenModule;
const Il2CppCodeGenModule g_System_Runtime_SerializationCodeGenModule = 
{
	"System.Runtime.Serialization.dll",
	1,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
