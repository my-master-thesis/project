using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Helper
{
    public class FindArmatureModel
    {
        private GameObject _armature;
        
        private string _armatureName = "fallBackArmature";
        private readonly Dictionary<string, string[]> _dictionary = new Dictionary<string, string[]>();
        public FindArmatureModel()
        {
            _dictionary.Add("JIC/AGJ", new[] {"AGJ"});
            _dictionary.Add("BSP/AGR", new[] {"AGN", "AGR", "AGORF", "AGS"});
            _dictionary.Add("JIC/DKJ", new[] {"DKJ"});
            _dictionary.Add("metric/light_series_DKOL", new[] {"DKOL", "DKOS", "DKM", "DKORF", "DK-KMSU", "DK-TYTA"});
            _dictionary.Add("metric/light_series_AGL", new []{"AGL", "AGM"});
            _dictionary.Add("BSP/DKR", new []{"DKR", "DKOR"});
            _dictionary.Add("banjo_connector_special/ring_eye_RAM_metric", new []{"RAM", "RAR"});
            _dictionary.Add("metric/RSL_RSS", new []{"RSL", "RSS"});
            _dictionary.Add("SAE_flange/SFL_3000", new []{"SFL", "SFS"});
            _dictionary.Add("banjo_connector_special/hose_connector", new []{"VB"});
        }
    
        public GameObject GetArmatureDependingOnName(string name)
        {
            foreach (var armaturePair in from armaturePair in _dictionary from s in armaturePair.Value where name.Contains(s) select armaturePair)
            {
                _armatureName = armaturePair.Key;
            }
            if (name.Contains("45°"))
                _armatureName += "45";
            if (name.Contains("90°"))
                _armatureName += "90";
            if (name.Contains("DKR") && name.Contains(" k"))
                _armatureName = "BSP/DKR90_kompakt";
    
            try
            {
                _armature = Resources.Load("Armatures/" + _armatureName) as GameObject;
                return _armature;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}