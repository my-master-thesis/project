﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Loading
{
    public static class SceneLoader
    {
        private class LoadingMonoBehaviour : MonoBehaviour
        {
        }

        public enum Scene
        {
            StartScreen,
            ChoosePipeScreen,
            ChooseArmaturesScreen,
            PlaceInArScreen,
            LoadingScreen,
            ErrorScreen,
            OrderOverviewScreen,
            OrderScreen
        }

        private static Action _onSceneLoaderCallback;
        private static AsyncOperation _loadingAsyncOperation;

        public static void Load(Scene scene)
        {
            _onSceneLoaderCallback = () =>
            {
                var loadingGameObject = new GameObject("Loading Game Object");
                loadingGameObject.AddComponent<LoadingMonoBehaviour>().StartCoroutine(LoadSceneAsync(scene));
            };
            SceneManager.LoadScene(Scene.LoadingScreen.ToString());
        }

        public static void LoadSceneAdaptive(Scene scene)
        {
            SceneManager.LoadScene(scene.ToString(), LoadSceneMode.Additive);
            var loadingGameObject = new GameObject("Loading Game Object");
            loadingGameObject.AddComponent<LoadingMonoBehaviour>()
                .StartCoroutine(SetActive(Scene.ChooseArmaturesScreen.ToString()));
        }

        private static IEnumerator SetActive(string scene)
        {
            var i = 0;
            while (i == 0)
            {
                i++;
                yield return null;
            }

            SceneManager.SetActiveScene(SceneManager.GetSceneByName(scene));
            yield break;
        }

        public static void UnLoadScene(Scene scene)
        {
            SceneManager.UnloadSceneAsync(scene.ToString());
        }

        private static IEnumerator LoadSceneAsync(Scene scene)
        {
            yield return null;
            _loadingAsyncOperation = SceneManager.LoadSceneAsync(scene.ToString());
            while (!_loadingAsyncOperation.isDone)
            {
                yield return null;
            }
        }

        public static float GetLoadingProgress()
        {
            return _loadingAsyncOperation?.progress ?? 1f;
        }

        public static Scene FindScene(string sceneName)
        {
            return (Scene) System.Enum.Parse(typeof(Scene), sceneName);
        }

        public static void SceneLoaderCallback()
        {
            if (_onSceneLoaderCallback == null) return;
            _onSceneLoaderCallback();
            _onSceneLoaderCallback = null;
        }
    }
}