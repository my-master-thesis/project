﻿using System;

namespace Service.Models
{
    [Serializable]
    public class PipeData
    {
        public string _id;
        public string name;
        public bool approved;
        public string articlenumber;
        public string category;
        public string code;
        public int dia;
        public string doublepipe;
        public string matchingsleeve;
        public string type;
        public double price;
    }
    
    [Serializable]
    public class PipeDataCollection
    {
        public PipeData[] pipes;
    }
}
