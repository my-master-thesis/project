﻿using UnityEngine;
using TMPro;

namespace Loading
{
    public class LoadingProgress : MonoBehaviour
    {

        public TMP_Text progressText;

        private void Start()
        {
            progressText.text = "0 %";
        }
    
        private void Update()
        {
            progressText.text = (SceneLoader.GetLoadingProgress() * 100) + "%";
        }
    }  
}
