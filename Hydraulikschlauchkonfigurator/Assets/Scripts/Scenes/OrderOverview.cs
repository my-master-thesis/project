﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Loading;
using Service.Models;
using Service.Rest;
using Service.Singletons;
using TMPro;
using UnityEngine;

namespace Scenes
{
    public class OrderOverview : MonoBehaviour
    {
        [SerializeField] private TMP_Text availableText;
        [SerializeField] private TMP_Text netPriceText;
        [SerializeField] private TMP_Text grossPriceText;
        [SerializeField] private TMP_Text pipeTypeText;
        [SerializeField] private TMP_Text pipeDiaText;
        [SerializeField] private TMP_InputField pipeLengthInputField;
        [SerializeField] private TMP_Text armatureOneText;
        [SerializeField] private TMP_Text armatureTwoText;
        [SerializeField] private TMP_InputField angleInputField;
        [SerializeField] private GameObject loadingOverlay;
    
        private PipeData _pipe;
        private readonly ArmatureData[] _armatures = new ArmatureData[2];
        private int _length;
        private int _angle;
        private List<ItemData> _itemData;

        private void Start()
        {
            DataCheck();
        }
    
        private void DataCheck()
        {
            loadingOverlay.SetActive(true);
            if (GlobalDataController.Instance.pipe._id != null &&
                GlobalDataController.Instance.armatureOne._id != null &&
                GlobalDataController.Instance.armatureTwo._id != null &&
                GlobalDataController.Instance.length != 0
            )
            {
                _pipe = GlobalDataController.Instance.pipe;
                _armatures[0] = GlobalDataController.Instance.armatureOne;
                _armatures[1] = GlobalDataController.Instance.armatureTwo;
                _length = GlobalDataController.Instance.length;
                _angle = GlobalDataController.Instance.angle;
    
                StartCoroutine(RestWebClient.Instance.HttpGet(
                    "https://www.granit-parts.com/productremotedetails?Ver=1.0&User=18177&Password=geheim&Itemcode_1=" +
                    _pipe.articlenumber + "&Itemcode_2=" + _armatures[0].articlenumber + "&Itemcode_3=" + _armatures[1].articlenumber, (r) => OnRequestComplete(r)));
            }
            else
            {
                SceneLoader.Load(SceneLoader.Scene.ErrorScreen);
            }
        }
    
        private void InitTextFields()
        {
            availableText.text = IsAvailable() ? "Sofort lieferbar" : "Bitte anfragen";
            availableText.color = IsAvailable() ? Color.green : Color.red;
            netPriceText.text = $"{CalculateNetPrice()}€";
            grossPriceText.text = $"{CalculateGrossPrice()}€";
    
            pipeTypeText.text = $"{_pipe.name}";
            pipeDiaText.text = $"{_pipe.dia}";
            pipeLengthInputField.text = $"{_length}";
            armatureOneText.text = $"{_armatures[0].name}";
            armatureTwoText.text = $"{_armatures[1].name}";
            angleInputField.text = $"{_angle}";
    
            angleInputField.interactable = IsAngled();
        }
    
        private void AddListeners()
        {
            pipeLengthInputField.onValueChanged.AddListener(delegate { LengthValueChanged(pipeLengthInputField); });
            angleInputField.onValueChanged.AddListener(delegate { AngleValueChanged(pipeLengthInputField); });
        }
    
        private void LengthValueChanged(TMP_InputField change)
        {
            if(change.text == "")
                return;
            if(Convert.ToInt32(change.text) <= 100 && Convert.ToInt32(change.text) == _length)
                return;
            _length = Convert.ToInt32(change.text);
            GlobalDataController.Instance.length = _length;
            InitTextFields();
        }
    
        private void AngleValueChanged(TMP_InputField change)
        {
            if (Convert.ToInt32(change.text) >= 0 && Convert.ToInt32(change.text) < 360)
                _angle = Convert.ToInt32(change.text);
            GlobalDataController.Instance.angle = _angle;
        }
    
        private bool IsAngled()
        {
            return _armatures[0].isAngledArmature && _armatures[1].isAngledArmature;
        }
    
        private void OnRequestComplete(Response response)
        {
            _itemData = ParseXml(response.Data);
            loadingOverlay.SetActive(false);
            InitTextFields();
            AddListeners();
        }
    
        private bool IsAvailable()
        {
            if (_itemData[1].available_on_stock > 0 && _itemData[2].available_on_stock > 0)
                return _length <= _itemData[0].available_on_stock * 1000;
            return false;
        }
    
        private double CalculateGrossPrice()
        {
            var grossPrice = 0.0;
            int lengthInMeter;
            if (_length % 1000 == 0)
            {
                lengthInMeter = Convert.ToInt32(_length / 1000);
            }
            else
            {
                lengthInMeter = Convert.ToInt32(_length / 1000)+1;
            }
            grossPrice += _itemData[0].grossprice * lengthInMeter;
            grossPrice += _pipe.price;
            grossPrice += _itemData[1].grossprice;
            grossPrice += _itemData[2].grossprice;
            return grossPrice;
        }
        
        private double CalculateNetPrice()
        {
            var netPrice = 0.0;
            int lengthInMeter;
            if (_length % 1000 == 0)
                lengthInMeter = Convert.ToInt32(_length / 1000);
            else
                lengthInMeter = Convert.ToInt32(_length / 1000)+1;
            netPrice += _itemData[0].netprice * lengthInMeter;
            netPrice += _pipe.price;
            netPrice += _itemData[1].netprice;
            netPrice += _itemData[2].netprice;
            return netPrice;
        }
    
        private List<ItemData> ParseXml(string xmlString)
        {
            var data = new List<ItemData>();
            var doc = XDocument.Parse(xmlString);
            foreach (var element in doc.Descendants("item"))
            {
                var item = new ItemData();
                item.available_on_stock = Convert.ToInt32(element.Element("available_on_stock")?.Value) | 0;
                item.requested_quantity = Convert.ToInt32(element.Element("requested_quantity")?.Value) | 0;
                item.next_delivery_date = element.Element("next_delivery_date")?.Value;
                item.grossprice = Convert.ToDouble(element.Element("grossprice")?.Value);
                item.netprice = Convert.ToDouble(element.Element("netprice")?.Value);
                data.Add(item);
            }
            return data;
        }
    }
}
