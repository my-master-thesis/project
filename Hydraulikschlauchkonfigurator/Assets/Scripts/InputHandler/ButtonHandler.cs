﻿using Loading;
using UnityEngine;

namespace InputHandler
{
    public class ButtonHandler : MonoBehaviour
    {
        public void LoadScene(string scene)
        {
            SceneLoader.Load(SceneLoader.FindScene(scene));
        }
    }
}