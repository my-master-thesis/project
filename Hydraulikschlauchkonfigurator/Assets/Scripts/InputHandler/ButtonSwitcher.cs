﻿using System;
using TMPro;
using UnityEngine;

namespace InputHandler
{
    public class ButtonSwitcher : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text cameraLabel;
        [SerializeField]
        private TMP_Text featureLabel;
        
        private Vector2 _touchInput;
        private float _previousTouchPosition;

        public static bool FeaturePointBased = true;

        private void Start()
        {
            cameraLabel.alpha = 0.1f;
        }

        private void Update()
        {
            if(Input.touchCount <= 0)
                return;
            var touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    _touchInput = touch.position;
                    break;
                case TouchPhase.Moved:
                    if (_previousTouchPosition == default)
                        _previousTouchPosition = _touchInput.x;
                    MoveCanvas(touch.position.x);
                    _previousTouchPosition = touch.position.x;
                    break;
                case TouchPhase.Ended:
                    CheckPlacementMethod();
                    break;
                case TouchPhase.Stationary:
                    break;
                case TouchPhase.Canceled:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void MoveCanvas(float currentTouchPosition)
        {
            float difference = 0;
            var position = transform.position;
            if (currentTouchPosition > _previousTouchPosition && position.x < 536)
            {
                difference = currentTouchPosition - _previousTouchPosition;
                transform.position = new Vector3(position.x + difference, position.y, position.z);
                if (!(cameraLabel.alpha > 0.25f) || !(featureLabel.alpha <= 1f)) return;
                difference *= 0.001f;
                cameraLabel.alpha -= difference;
                featureLabel.alpha += difference;
            }
            else if (currentTouchPosition < _previousTouchPosition && position.x > 293)
            {
                difference = _previousTouchPosition - currentTouchPosition;
                transform.position = new Vector3(position.x-difference, position.y, position.z);
                if (!(featureLabel.alpha > 0.25f) || !(cameraLabel.alpha <= 1f)) return;
                difference *= 0.001f;
                cameraLabel.alpha += difference;
                featureLabel.alpha -= difference;
            }
        }

        private void CheckPlacementMethod()
        {
            var position = transform.position;
            Vector3 featureBased = new Vector3(536, position.y, position.z),
                cameraBased = new Vector3(293, position.y, position.z);
            if (position.x < 350)
                FeaturePointBased = false;
            else if (position.x > 500)
                FeaturePointBased = true;
            transform.position = FeaturePointBased ? featureBased : cameraBased;
            cameraLabel.alpha = FeaturePointBased ? 0.25f : 1f;
            featureLabel.alpha = FeaturePointBased ? 1f : 0.25f;
        }
    } 
}
