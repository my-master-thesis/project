﻿using System;

namespace Service.Models
{
    [Serializable]
    public class ArmatureData
    {
        public string _id;
        public string name;
        public bool approved;
        public string articlenumber;
        public string category;
        public string code;
        public int dia;
        public string type;
        public bool isAngledArmature = false;
    }
    
    [Serializable]
    public class ArmatureDataCollection
    {
        public ArmatureData[] armatures;
    }
}
