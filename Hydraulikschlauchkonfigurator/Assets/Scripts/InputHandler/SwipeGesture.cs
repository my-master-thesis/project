﻿using UnityEngine;

namespace InputHandler
{
    public class SwipeGesture : MonoBehaviour
    {
        public string sceneName;
        private Vector2 _startPosition;
        private float _startTime;
        private ButtonHandler _handler;
    
        private void Start () {
            _handler = gameObject.AddComponent<ButtonHandler>();
        }
    
        private void Update () {
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) 
            {
                var endPosition = Input.GetTouch(0).position;
                var delta = endPosition - _startPosition;
           
                var dist = Mathf.Sqrt(Mathf.Pow(delta.x, 2) + Mathf.Pow (delta.y, 2));
                var angle = Mathf.Atan (delta.y/delta.x) * (180.0f/Mathf.PI);
                var duration = Time.time - _startTime;
                var speed = dist/duration;
           
                // Left to right swipe
                if (_startPosition.y < endPosition.y) 
                {
                    if (angle < 0) angle = angle * 1.0f;
                    if (dist > 300 &&  angle < 10 && speed > 2500 && _startPosition.x < 150)
                    {
                        if(!string.IsNullOrEmpty(sceneName))
                            _handler.LoadScene(sceneName);
                    }
                }
            }

            if (Input.touchCount <= 0 || Input.GetTouch(0).phase != TouchPhase.Began) return;
            _startPosition = Input.GetTouch(0).position;
            _startTime = Time.time;
        }
    }
}