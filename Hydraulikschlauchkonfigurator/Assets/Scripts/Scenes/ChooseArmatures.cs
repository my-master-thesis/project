﻿using System;
using System.Collections.Generic;
using System.Linq;
using Loading;
using Scenes.PlaceObjectsInAR;
using Service.Models;
using Service.Rest;
using Service.Singletons;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Scenes
{
    public class ChooseArmatures : MonoBehaviour
    {
        public Button placeArmaturesButton;
        public TMP_Dropdown armatureOneDropdown;
        public TMP_Dropdown armatureTwoDropdown;
        public GameObject loadingPrefab;
        public Toggle adoptArmature;
        private PipeData _selectedPipe;
        private ArmatureData[] _armatures;
        private ArmatureData _firstSelectedArmature;
        private ArmatureData _secondSelectedArmature;
        private const string DefaultDropdownValue = "Bitte wählen ...";

        private void Start()
        {
            loadingPrefab.SetActive(true);
            _selectedPipe = GlobalDataController.Instance.pipe;
            DataCheck();
            AddEventListener();
            StartCoroutine(RestWebClient.Instance.HttpGet(
                GlobalDataController.Instance.baseUrl + "armatures/?articlenumber=" + _selectedPipe.articlenumber,
                (r) => OnRequestComplete(r)));
        }

        private void OnRequestComplete(Response response)
        {
            if (response.Error != null)
            {
                if (response.Error == "Cannot connect to destination host")
                {
                    var mockData = Resources.Load<TextAsset>("Data/armatures");
                    var data = (JsonUtility.FromJson<ArmatureDataCollection>(mockData.ToString())).armatures;
                    _armatures = Array.FindAll(data,
                        a => a.approved && a.category == _selectedPipe.category && a.type == _selectedPipe.type &&
                             a.dia == _selectedPipe.dia);
                    FillDropDownWithData(armatureOneDropdown);
                }
                else
                {
                    Debug.Log(response.Error);
                }
            }
            else if (response.Data != null)
            {
                _armatures = (JsonUtility.FromJson<ArmatureDataCollection>(response.Data)).armatures;
                FillDropDownWithData(armatureOneDropdown);
            }
            else
            {
                Debug.Log("SOMETHING WENT WRONG");
            }

            loadingPrefab.SetActive(false);
        }

        private void OnPlaceArmatureButtonClicked()
        {
            if (_selectedPipe == null ||
                _firstSelectedArmature == null ||
                _secondSelectedArmature == null
            )
            {
                return;
            }

            GlobalDataController.Instance.armatureOne = _firstSelectedArmature;
            GlobalDataController.Instance.armatureTwo = _secondSelectedArmature;
            SceneLoader.UnLoadScene(SceneLoader.FindScene("ChooseArmaturesScreen"));
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(SceneLoader.Scene.PlaceInArScreen.ToString()));
            PlacementController.DataCheckPassed = false;
        }

        private void OnAdoptArmatureToggleClicked(bool selection)
        {
            if(!selection || _firstSelectedArmature == null)
                return;
            armatureTwoDropdown.value = Array.FindIndex(_armatures, ad => ad.name == _firstSelectedArmature.name) + 1;
            armatureTwoDropdown.RefreshShownValue();
        }

        private void ArmatureOneDropdownValueChanged(TMP_Dropdown change)
        {
            var selectedOption = change.captionText.text;
            _firstSelectedArmature = selectedOption != DefaultDropdownValue ? Array.Find(_armatures,
                ad => ad.name == selectedOption) : null;
            if (_firstSelectedArmature != null)
            {
                FillDropDownWithData(armatureTwoDropdown);
                OnAdoptArmatureToggleClicked(adoptArmature.isOn);
            }
            else
            {
                _secondSelectedArmature = null;
                adoptArmature.isOn = false;
            }

            armatureTwoDropdown.interactable = _firstSelectedArmature != null;
            adoptArmature.interactable = _firstSelectedArmature != null;
        }

        private void ArmatureTwoDropdownValueChanged(TMP_Dropdown change)
        {
            var selectedOption = change.captionText.text;
            if (selectedOption != DefaultDropdownValue)
            {
                _secondSelectedArmature =
                    Array.Find(_armatures, ad => ad.name == selectedOption);
                if (adoptArmature.isOn && _secondSelectedArmature.name != _firstSelectedArmature.name)
                    adoptArmature.isOn = false;
            }
            else
            {
                _secondSelectedArmature = null;
            }

            placeArmaturesButton.interactable = _secondSelectedArmature != null;
        }

        private void AddEventListener()
        {
            armatureOneDropdown.onValueChanged.AddListener(delegate
            {
                ArmatureOneDropdownValueChanged(armatureOneDropdown);
            });

            armatureTwoDropdown.onValueChanged.AddListener(delegate
            {
                ArmatureTwoDropdownValueChanged(armatureTwoDropdown);
            });

            placeArmaturesButton.onClick.AddListener(OnPlaceArmatureButtonClicked);
            adoptArmature.onValueChanged.AddListener(OnAdoptArmatureToggleClicked);
        }

        private void FillDropDownWithData(TMP_Dropdown dropdown)
        {
            var armatureList = new List<string>();
            armatureList.Add(DefaultDropdownValue);
            Array.Sort(_armatures,
                (x, y) => string.Compare(x.name, y.name, StringComparison.Ordinal));
            armatureList.AddRange(_armatures.Select(armature => armature.name.ToString()));

            dropdown.ClearOptions();
            dropdown.AddOptions(armatureList);
            dropdown.interactable = true;
        }

        private void DataCheck()
        {
            if (_selectedPipe == null)
            {
                SceneLoader.Load(SceneLoader.Scene.PlaceInArScreen);
            }
        }
    }
}