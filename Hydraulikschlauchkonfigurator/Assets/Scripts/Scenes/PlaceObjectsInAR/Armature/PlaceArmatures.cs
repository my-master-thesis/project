﻿using System;
using System.Collections.Generic;
using Helper;
using Loading;
using Scenes.PlaceObjectsInAR.Pipe;
using Service.Models;
using Service.Singletons;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Scenes.PlaceObjectsInAR.Armature
{
    public class PlaceArmatures : MonoBehaviour
    {
        [SerializeField] private Camera arCamera;
        [SerializeField] private TMP_Text pipeText;
        [SerializeField] private TMP_Text firstArmatureText;
        [SerializeField] private TMP_Text secondArmatureText;
        [SerializeField] private TMP_Text lengthText;
        [SerializeField] private TMP_Text twistedAngleText;
        [SerializeField] private GameObject selectedObjectVisualizer;
        [SerializeField] private GameObject pipeObject;
        [SerializeField] private Canvas angleCanvas;
        [SerializeField] private TMP_Text angleText;
        [SerializeField] private Button overlayButton;
        [SerializeField] private Material pipeMaterial;
        [SerializeField] private Material armatureMaterial;
        [SerializeField] private Material grayedMaterial;

        private readonly FindArmatureModel _findArmatureModel = new FindArmatureModel();
        private readonly ArmatureData[] _armatures = new ArmatureData[2];
        private List<GameObject> _wayPoints = new List<GameObject>();
        private GameObject _firstArmature;
        private GameObject _secondArmature;
        private PipeData _pipe;
        private Vector2 _touchInput;
        private float _previousTouchPosition;
        private GameObject _selectedArmature;
        private GameObject _selectedArmatureVisualizer;
        private int _angle;

        private void Start()
        {
            angleCanvas.enabled = false;
            _wayPoints = PlacePipe._wayPoints;
            _pipe = GlobalDataController.Instance.pipe;
            _armatures[0] = GlobalDataController.Instance.armatureOne;
            _armatures[1] = GlobalDataController.Instance.armatureTwo;
            CheckAngled();
            SetArmaturesOnPosition();
            InitializeTexts();
        }

        private void Update()
        {
            if (Input.touchCount <= 0) return;
            var touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    _touchInput = touch.position;
                    break;
                case TouchPhase.Moved:
                    if (_previousTouchPosition == default)
                        _previousTouchPosition = _touchInput.y;
                    RotateObject(_previousTouchPosition, touch.position.y);
                    _previousTouchPosition = touch.position.y;
                    break;
                case TouchPhase.Ended:
                    if (touch.position == _touchInput)
                    {
                        var ray = arCamera.ScreenPointToRay(_touchInput);
                        if (Physics.Raycast(ray, out var hitObject))
                        {
                            if (hitObject.transform.position == _firstArmature.transform.position &&
                                _armatures[0].isAngledArmature)
                                SelectObject(_firstArmature, _secondArmature);
                            else if (hitObject.transform.position == _secondArmature.transform.position &&
                                     _armatures[1].isAngledArmature)
                                SelectObject(_secondArmature, _firstArmature);
                            else
                                Deselect();
                        }
                        else
                        {
                            Deselect();
                        }
                    }

                    break;
                case TouchPhase.Stationary:
                    break;
                case TouchPhase.Canceled:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void SetArmaturesOnPosition()
        {
            _firstArmature = Instantiate(_findArmatureModel.GetArmatureDependingOnName(_armatures[0].name),
                _wayPoints[0].transform);
            _firstArmature.transform.LookAt(_wayPoints[1].transform.position);
            _secondArmature = Instantiate(_findArmatureModel.GetArmatureDependingOnName(_armatures[1].name),
                _wayPoints[_wayPoints.Count - 1].transform);
            _secondArmature.transform.LookAt(_wayPoints[_wayPoints.Count - 2].transform.position);
        }

        private void CheckAngled()
        {
            foreach (var armatureData in _armatures)
            {
                if (armatureData.name.Contains("45°") || armatureData.name.Contains("90°"))
                {
                    armatureData.isAngledArmature = true;
                }
                else
                {
                    overlayButton.transform.position = new Vector3(754, 1552, 0);
                }
            }

            if (_armatures[0].isAngledArmature && _armatures[1].isAngledArmature)
            {
                angleCanvas.enabled = true;
                overlayButton.transform.position = new Vector3(754, 1458, 0);
            }

            GlobalDataController.Instance.armatureOne = _armatures[0];
            GlobalDataController.Instance.armatureTwo = _armatures[1];
        }

        private void InitializeTexts()
        {
            pipeText.text = _pipe.name;
            firstArmatureText.text = _armatures[0].name;
            secondArmatureText.text = _armatures[1].name;
            lengthText.text = $"{GlobalDataController.Instance.length}mm";
            UpdateAngleValue();
        }

        private void SelectObject(GameObject selectedGameObject, GameObject notSelectedGameObject)
        {
            if (_selectedArmatureVisualizer)
            {
                if (_selectedArmatureVisualizer.transform.position != selectedGameObject.transform.position)
                {
                    SetObjectVisualizer(selectedGameObject, notSelectedGameObject);
                }
            }
            else
            {
                SetObjectVisualizer(selectedGameObject, notSelectedGameObject);
            }
        }

        private void Deselect()
        {
            Destroy(_selectedArmatureVisualizer);
            ChangePipeMaterial(pipeMaterial);
            ChangeArmatureMaterial(_firstArmature, armatureMaterial);
            ChangeArmatureMaterial(_secondArmature, armatureMaterial);
        }

        private void SetObjectVisualizer(GameObject selectedGameObject, GameObject notSelectedGameObject)
        {
            _selectedArmatureVisualizer = Instantiate(selectedObjectVisualizer, selectedGameObject.transform);
            _selectedArmatureVisualizer.transform.localScale = new Vector3(1.5f, 1.5f, 0.25f);
            _selectedArmature = selectedGameObject;

            ChangePipeMaterial(grayedMaterial);
            ChangeArmatureMaterial(selectedGameObject, armatureMaterial);
            ChangeArmatureMaterial(notSelectedGameObject, grayedMaterial);
        }

        private void RotateObject(float startPosition, float currentPosition)
        {
            if (!_selectedArmature) return;
            var eulerAngles = _selectedArmature.transform.eulerAngles;
            if (currentPosition > startPosition)
                eulerAngles = new Vector3(eulerAngles.x, eulerAngles.y, eulerAngles.z - 5);
            else if (currentPosition < startPosition)
                eulerAngles = new Vector3(eulerAngles.x, eulerAngles.y, eulerAngles.z + 5);

            _selectedArmature.transform.eulerAngles = eulerAngles;
            UpdateAngleValue();
        }

        private void UpdateAngleValue()
        {
            double angle = 0;
            var leftArmature = Math.Round(_firstArmature.transform.eulerAngles.z / 5.0) * 5;
            leftArmature = leftArmature > 0 ? leftArmature - 360 : leftArmature;
            leftArmature = Math.Abs(leftArmature);
            var rightArmature = Math.Round(_secondArmature.transform.eulerAngles.z / 5.0) * 5;
            if (leftArmature > rightArmature)
                angle = leftArmature - rightArmature;
            else if (rightArmature > leftArmature)
                angle = rightArmature - leftArmature;
            angle = angle > 0 ? angle - 360 : angle;
            _angle = Convert.ToInt32(Math.Abs(angle));
            twistedAngleText.text = $"{_angle}˚";
            angleText.text = $"{_angle}˚";
        }

        private void ChangePipeMaterial(Material material)
        {
            var child = pipeObject.transform.GetChild(0);
            for (var i = 0; i < child.transform.childCount; i++)
            {
                child.transform.GetChild(i).GetComponent<MeshRenderer>().material = material;
            }
        }

        private void ChangeArmatureMaterial(GameObject whichArmature, Material material)
        {
            var child = whichArmature.transform.GetChild(0);
            child.GetComponent<MeshRenderer>().material = material;
        }

        public void Finish()
        {
            GlobalDataController.Instance.angle = _angle;
            SceneLoader.Load(SceneLoader.Scene.OrderOverviewScreen);
        }
    }
}