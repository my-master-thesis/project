﻿using UnityEngine;

namespace Loading
{
    public class SceneLoaderCallback : MonoBehaviour
    {
        private bool _isFirstUpdate = true;

        private void Update()
        {
            if (!_isFirstUpdate)
                return;
            _isFirstUpdate = false;
            SceneLoader.SceneLoaderCallback();
        }
    }   
}
