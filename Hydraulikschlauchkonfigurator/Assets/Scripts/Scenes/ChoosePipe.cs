﻿using System;
using System.Collections.Generic;
using System.Linq;
using Loading;
using Service.Models;
using Service.Rest;
using Service.Singletons;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Scenes
{
    public class ChoosePipe : MonoBehaviour
    {
        public Button placePipeButton;
        public TMP_Dropdown categoryDropdown;
        public TMP_Dropdown typeDropdown;
        public GameObject loadingPrefab;
        private PipeData[] _pipes;
        private PipeData _selectedPipe;
        private string _selectedCategoryValue;
        private const string DefaultDropdownValue = "Bitte wählen ...";

        private readonly Dictionary<string, string> _categories = new Dictionary<string, string>()
        {
            {"Standard", "GR"},
            {"Waschgeräteschläuche", "GRW"},
            {"Sonstige", "GRS"}
        };

        private void Awake()
        {
            placePipeButton.interactable = false;
            typeDropdown.interactable = false;
            AddEventListener();
            if (GlobalDataController.Instance.pipe._id != "" || GlobalDataController.Instance.pipe._id != null)
            {
                ResetConfiguration();
            }
        }

        private void ResetConfiguration()
        {
            GlobalDataController.Instance.pipe = new PipeData();
            GlobalDataController.Instance.armatureOne = new ArmatureData();
            GlobalDataController.Instance.armatureTwo = new ArmatureData();
            GlobalDataController.Instance.length = 0;
        }

        private void CategoryDropdownValueChanged(TMP_Dropdown change)
        {
            typeDropdown.interactable = false;
            var selectedOption = change.captionText.text;
            if (_categories.ContainsKey(selectedOption))
            {
                _selectedCategoryValue = _categories[selectedOption];
                loadingPrefab.SetActive(true);
                StartCoroutine(RestWebClient.Instance.HttpGet(
                    GlobalDataController.Instance.baseUrl + "pipes/?category=" + _selectedCategoryValue,
                    (r) => OnRequestComplete(r)));
            }
            else
            {
                typeDropdown.ClearOptions();
                _selectedPipe = null;
                placePipeButton.interactable = false;
            }
        }

        private void PipeTypeDropdownValueChanged(TMP_Dropdown change)
        { 
            var selectedOption = change.captionText.text;
            _selectedPipe = selectedOption != DefaultDropdownValue
                ? Array.Find(_pipes, pd => pd.name == selectedOption)
                : null;
            placePipeButton.interactable = _selectedPipe != null;
        }

        private void OnRequestComplete(Response response)
        {
            if (response.Error != null)
            {
                if (response.Error == "Cannot connect to destination host")
                {
                    var mockData = Resources.Load<TextAsset>("Data/pipes");
                    _pipes = (JsonUtility.FromJson<PipeDataCollection>(mockData.ToString())).pipes;
                    _pipes = Array.FindAll(_pipes, p => p.category == _selectedCategoryValue);
                    var pipeList = new List<string> {DefaultDropdownValue};
                    pipeList.AddRange(_pipes.Select(pipe => pipe.name));

                    typeDropdown.ClearOptions();
                    typeDropdown.AddOptions(pipeList);
                    typeDropdown.interactable = true;
                }
                else
                {
                    Debug.Log(response.Error);
                }
            }
            else if (response.Data != null)
            {
                _pipes = (JsonUtility.FromJson<PipeDataCollection>(response.Data)).pipes;
                var pipeList = new List<string> {DefaultDropdownValue};
                Array.Sort(_pipes, (x,y) => x.dia.CompareTo(y.dia));
                pipeList.AddRange(_pipes.Select(pipe => pipe.name.ToString()));

                typeDropdown.ClearOptions();
                typeDropdown.AddOptions(pipeList);
                typeDropdown.interactable = true;
            }
            else
            {
                Debug.Log("SOMETHING WENT WRONG");
            }
            loadingPrefab.SetActive(false);
        }

        private void AddEventListener()
        {
            categoryDropdown.onValueChanged.AddListener(delegate { CategoryDropdownValueChanged(categoryDropdown); });
            typeDropdown.onValueChanged.AddListener(delegate { PipeTypeDropdownValueChanged(typeDropdown); });
            placePipeButton.onClick.AddListener(OnPlacePipeButtonClicked);
        }

        private void OnPlacePipeButtonClicked()
        {
            if (_selectedPipe == null)
                return;
            GlobalDataController.Instance.pipe = _selectedPipe;
            SceneLoader.Load(SceneLoader.Scene.PlaceInArScreen);
        }
    }
}