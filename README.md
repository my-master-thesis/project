# Implementierung des AR-Hydraulikschlauchkonfigurators

In diesem Projekt wurde der AR-Hydraulikschlauchkonfigurator basierend auf dem Web-Konfigurator von Granit-Parts implementiert.

## Voraussetzungen

- Unity 2019.4.20f1
- iOS Gerät mit mind. iOS 11.0
- AR Foundation 2.1.14
- ARKit XR Plugin 2.1.14 


## Build
Im Ordner [build](build/) ist ein iOS build der Anwendung zu finden. Dieser kann mit xCode geöffnet und auf einem iOS-Gerät installiert werden.