﻿using Loading;
using UnityEngine;

namespace Helper
{
    public class Redirect : MonoBehaviour
    {
        private float _timeLeft = 5.0f;
        private void Update()
        {
            _timeLeft -= Time.deltaTime;
            if (_timeLeft < 0)
            {
                SceneLoader.Load(SceneLoader.Scene.StartScreen);
            }
        }
    }  
}
