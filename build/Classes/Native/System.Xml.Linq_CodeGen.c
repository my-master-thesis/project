﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void System.Xml.Linq.XName::.ctor(System.Xml.Linq.XNamespace,System.String)
extern void XName__ctor_m11BAB50C83C349465FFB63B5EEDDAF713C11942F (void);
// 0x00000002 System.String System.Xml.Linq.XName::get_LocalName()
extern void XName_get_LocalName_mC959CACE19C206C85FAD179AB086DD11BF5221E3 (void);
// 0x00000003 System.Xml.Linq.XNamespace System.Xml.Linq.XName::get_Namespace()
extern void XName_get_Namespace_mBD833FBB757D8C41E9D2404F5BE036B8E446CBBC (void);
// 0x00000004 System.String System.Xml.Linq.XName::get_NamespaceName()
extern void XName_get_NamespaceName_m5F826139218C51FEDAC51BF97453C7E4525D176F (void);
// 0x00000005 System.String System.Xml.Linq.XName::ToString()
extern void XName_ToString_m611A4E60C1783A842415A480C1EED1C273EE5AA3 (void);
// 0x00000006 System.Xml.Linq.XName System.Xml.Linq.XName::Get(System.String)
extern void XName_Get_m5DC4D0C2C5C965F51164366808A577A7B6989D13 (void);
// 0x00000007 System.Xml.Linq.XName System.Xml.Linq.XName::op_Implicit(System.String)
extern void XName_op_Implicit_mD82E6E7414AED8E24BB48B7F2914C5A21EE4AF3F (void);
// 0x00000008 System.Boolean System.Xml.Linq.XName::Equals(System.Object)
extern void XName_Equals_m48725EB0D37130A1341509CAC239670F6530152D (void);
// 0x00000009 System.Int32 System.Xml.Linq.XName::GetHashCode()
extern void XName_GetHashCode_mC0E9FDF696C03C889DC391F229D373612D1F3998 (void);
// 0x0000000A System.Boolean System.Xml.Linq.XName::op_Equality(System.Xml.Linq.XName,System.Xml.Linq.XName)
extern void XName_op_Equality_m0D26C036FA8957FE3419F714BA1B0371AE0B7598 (void);
// 0x0000000B System.Boolean System.Xml.Linq.XName::op_Inequality(System.Xml.Linq.XName,System.Xml.Linq.XName)
extern void XName_op_Inequality_mAD594A0AC5072A85071F8FBADBFD52936530232A (void);
// 0x0000000C System.Boolean System.Xml.Linq.XName::System.IEquatable<System.Xml.Linq.XName>.Equals(System.Xml.Linq.XName)
extern void XName_System_IEquatableU3CSystem_Xml_Linq_XNameU3E_Equals_mA462744020AE77E5E3E892ABEA5B012F1EFBD91C (void);
// 0x0000000D System.Void System.Xml.Linq.XName::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void XName_System_Runtime_Serialization_ISerializable_GetObjectData_mAC29C083365CDAAD94099D2E05E20B6601C90F4C (void);
// 0x0000000E System.Void System.Xml.Linq.XName::.ctor()
extern void XName__ctor_m4BD5071BDD613AC497E8CB4CC5F3BEA550F22C07 (void);
// 0x0000000F System.Void System.Xml.Linq.NameSerializer::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void NameSerializer__ctor_m8F1196AF1E27A0FD7C7DFB1BC9CDA9C48AB91BCC (void);
// 0x00000010 System.Object System.Xml.Linq.NameSerializer::System.Runtime.Serialization.IObjectReference.GetRealObject(System.Runtime.Serialization.StreamingContext)
extern void NameSerializer_System_Runtime_Serialization_IObjectReference_GetRealObject_mAF50D783824AAF24448B624A4C9B9D6064283B9F (void);
// 0x00000011 System.Void System.Xml.Linq.NameSerializer::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void NameSerializer_System_Runtime_Serialization_ISerializable_GetObjectData_m9FAC4FC6119A316EC9E033F9DA26C2F88C53B1F7 (void);
// 0x00000012 System.Void System.Xml.Linq.XNamespace::.ctor(System.String)
extern void XNamespace__ctor_m9E2F1387816E4F402AD6E52F0AF0B73FA9DE4253 (void);
// 0x00000013 System.String System.Xml.Linq.XNamespace::get_NamespaceName()
extern void XNamespace_get_NamespaceName_m805748C83B347006CC65F0ECC5B1C9124213557E (void);
// 0x00000014 System.Xml.Linq.XName System.Xml.Linq.XNamespace::GetName(System.String)
extern void XNamespace_GetName_mE5576274C571BF2BCFDB2185A1C18EFA115FC6C7 (void);
// 0x00000015 System.String System.Xml.Linq.XNamespace::ToString()
extern void XNamespace_ToString_m64DA46BA983C78CDD07B3E77D09E6019F2D1AC65 (void);
// 0x00000016 System.Xml.Linq.XNamespace System.Xml.Linq.XNamespace::get_None()
extern void XNamespace_get_None_mF6622A086F9CD5ED2DB377D831B9AAA15C56C2F7 (void);
// 0x00000017 System.Xml.Linq.XNamespace System.Xml.Linq.XNamespace::get_Xml()
extern void XNamespace_get_Xml_mF6025F66B00A6841332A05092EE781E12B162274 (void);
// 0x00000018 System.Xml.Linq.XNamespace System.Xml.Linq.XNamespace::get_Xmlns()
extern void XNamespace_get_Xmlns_m6DB2CF70CF8044C21834146A91615CBFCC9F162F (void);
// 0x00000019 System.Xml.Linq.XNamespace System.Xml.Linq.XNamespace::Get(System.String)
extern void XNamespace_Get_mE26B2D740E108101DA4687D724EC4DD7FFD57AF6 (void);
// 0x0000001A System.Boolean System.Xml.Linq.XNamespace::Equals(System.Object)
extern void XNamespace_Equals_m8BDD54FF83402923462E8994852C4594A6D94C6C (void);
// 0x0000001B System.Int32 System.Xml.Linq.XNamespace::GetHashCode()
extern void XNamespace_GetHashCode_mCBB26B617C1C85E5B26E6847F48A56F7B5B3318D (void);
// 0x0000001C System.Boolean System.Xml.Linq.XNamespace::op_Equality(System.Xml.Linq.XNamespace,System.Xml.Linq.XNamespace)
extern void XNamespace_op_Equality_mC02B35B87BD22807C6CA43815E94170EFAC261AC (void);
// 0x0000001D System.Boolean System.Xml.Linq.XNamespace::op_Inequality(System.Xml.Linq.XNamespace,System.Xml.Linq.XNamespace)
extern void XNamespace_op_Inequality_mA84E8D1A01DB9A00F467B6214F1EB28C7140479C (void);
// 0x0000001E System.Xml.Linq.XName System.Xml.Linq.XNamespace::GetName(System.String,System.Int32,System.Int32)
extern void XNamespace_GetName_mC7F14EFE2F8B4B4E0CBF944B0CF90DFED1575108 (void);
// 0x0000001F System.Xml.Linq.XNamespace System.Xml.Linq.XNamespace::Get(System.String,System.Int32,System.Int32)
extern void XNamespace_Get_mF6BCD4BC1C153E6080A751A7B3786732BD32659E (void);
// 0x00000020 System.String System.Xml.Linq.XNamespace::ExtractLocalName(System.Xml.Linq.XName)
extern void XNamespace_ExtractLocalName_mDB91372235FDAED58052AB971963F79C3D23D3C0 (void);
// 0x00000021 System.String System.Xml.Linq.XNamespace::ExtractNamespace(System.WeakReference)
extern void XNamespace_ExtractNamespace_m1F6A139C9924E2464BB0771E05D3CE39833440E8 (void);
// 0x00000022 System.Xml.Linq.XNamespace System.Xml.Linq.XNamespace::EnsureNamespace(System.WeakReference&,System.String)
extern void XNamespace_EnsureNamespace_mDEFD4E69216E12031B2D6EF170213023A21C1749 (void);
// 0x00000023 System.Void System.Xml.Linq.XHashtable`1::.ctor(System.Xml.Linq.XHashtable`1_ExtractKeyDelegate<TValue>,System.Int32)
// 0x00000024 System.Boolean System.Xml.Linq.XHashtable`1::TryGetValue(System.String,System.Int32,System.Int32,TValue&)
// 0x00000025 TValue System.Xml.Linq.XHashtable`1::Add(TValue)
// 0x00000026 System.Void System.Xml.Linq.XHashtable`1_ExtractKeyDelegate::.ctor(System.Object,System.IntPtr)
// 0x00000027 System.String System.Xml.Linq.XHashtable`1_ExtractKeyDelegate::Invoke(TValue)
// 0x00000028 System.IAsyncResult System.Xml.Linq.XHashtable`1_ExtractKeyDelegate::BeginInvoke(TValue,System.AsyncCallback,System.Object)
// 0x00000029 System.String System.Xml.Linq.XHashtable`1_ExtractKeyDelegate::EndInvoke(System.IAsyncResult)
// 0x0000002A System.Void System.Xml.Linq.XHashtable`1_XHashtableState::.ctor(System.Xml.Linq.XHashtable`1_ExtractKeyDelegate<TValue>,System.Int32)
// 0x0000002B System.Xml.Linq.XHashtable`1_XHashtableState<TValue> System.Xml.Linq.XHashtable`1_XHashtableState::Resize()
// 0x0000002C System.Boolean System.Xml.Linq.XHashtable`1_XHashtableState::TryGetValue(System.String,System.Int32,System.Int32,TValue&)
// 0x0000002D System.Boolean System.Xml.Linq.XHashtable`1_XHashtableState::TryAdd(TValue,TValue&)
// 0x0000002E System.Boolean System.Xml.Linq.XHashtable`1_XHashtableState::FindEntry(System.Int32,System.String,System.Int32,System.Int32,System.Int32&)
// 0x0000002F System.Int32 System.Xml.Linq.XHashtable`1_XHashtableState::ComputeHashCode(System.String,System.Int32,System.Int32)
// 0x00000030 System.Void System.Xml.Linq.XObject::.ctor()
extern void XObject__ctor_m7C54F218B4232FE2278ED1F8CEEB51FA9D8059E3 (void);
// 0x00000031 System.String System.Xml.Linq.XObject::get_BaseUri()
extern void XObject_get_BaseUri_m21E44D54AF7D631DA1017ECC8856E77D8FAF771B (void);
// 0x00000032 System.Xml.XmlNodeType System.Xml.Linq.XObject::get_NodeType()
// 0x00000033 System.Void System.Xml.Linq.XObject::AddAnnotation(System.Object)
extern void XObject_AddAnnotation_m90169D87A51DB9EC16D18F66D63059B515536C17 (void);
// 0x00000034 System.Object System.Xml.Linq.XObject::Annotation(System.Type)
extern void XObject_Annotation_m9C08117D1974D059A084A25D6FAAFA91754A6ACC (void);
// 0x00000035 T System.Xml.Linq.XObject::Annotation()
// 0x00000036 System.Boolean System.Xml.Linq.XObject::System.Xml.IXmlLineInfo.HasLineInfo()
extern void XObject_System_Xml_IXmlLineInfo_HasLineInfo_m45AFB5B7292EEEDC22C26EADF67868F6C784CE96 (void);
// 0x00000037 System.Int32 System.Xml.Linq.XObject::System.Xml.IXmlLineInfo.get_LineNumber()
extern void XObject_System_Xml_IXmlLineInfo_get_LineNumber_m699F2F923FACEF97C28D7721D7ED2CCFB3401AC3 (void);
// 0x00000038 System.Int32 System.Xml.Linq.XObject::System.Xml.IXmlLineInfo.get_LinePosition()
extern void XObject_System_Xml_IXmlLineInfo_get_LinePosition_m6EFA0448297C2FEBB187D0763D7E4C26A4AF7F33 (void);
// 0x00000039 System.Boolean System.Xml.Linq.XObject::get_HasBaseUri()
extern void XObject_get_HasBaseUri_m571789C97A20A08EE48E3AE293E564769523691D (void);
// 0x0000003A System.Void System.Xml.Linq.XObject::SetBaseUri(System.String)
extern void XObject_SetBaseUri_mFD7D5326440D073876DDE99C768CA6585946B0FE (void);
// 0x0000003B System.Void System.Xml.Linq.XObject::SetLineInfo(System.Int32,System.Int32)
extern void XObject_SetLineInfo_m9BF3FC30A56328AEAD32EA57AF6C0A635FE4D687 (void);
// 0x0000003C System.Xml.Linq.SaveOptions System.Xml.Linq.XObject::GetSaveOptionsFromAnnotations()
extern void XObject_GetSaveOptionsFromAnnotations_mA6A8CFD79C02801110BD726ACBCEDF272729DE92 (void);
// 0x0000003D System.Void System.Xml.Linq.BaseUriAnnotation::.ctor(System.String)
extern void BaseUriAnnotation__ctor_mB42CD28291DD19E64ABE16FA4C8041ED73291E7E (void);
// 0x0000003E System.Void System.Xml.Linq.LineInfoAnnotation::.ctor(System.Int32,System.Int32)
extern void LineInfoAnnotation__ctor_mBE2123924099AC38A102AAB235760B95220E7F1A (void);
// 0x0000003F System.Void System.Xml.Linq.LineInfoEndElementAnnotation::.ctor(System.Int32,System.Int32)
extern void LineInfoEndElementAnnotation__ctor_mCA8A79B2D5736641FBA29C8CCEDCD3DCD06A2E4A (void);
// 0x00000040 System.Void System.Xml.Linq.XNode::.ctor()
extern void XNode__ctor_m4AF8DD0B3E4B1667BCAC37789ADBC8901B3E4ECA (void);
// 0x00000041 System.String System.Xml.Linq.XNode::ToString()
extern void XNode_ToString_m5812DCAA56C17BBA26CC6D429C669D1C5E5CF0F2 (void);
// 0x00000042 System.Void System.Xml.Linq.XNode::WriteTo(System.Xml.XmlWriter)
// 0x00000043 System.Void System.Xml.Linq.XNode::AppendText(System.Text.StringBuilder)
extern void XNode_AppendText_m166AA963267247C2CD9F80CA8A1043B2C9AAF52E (void);
// 0x00000044 System.Xml.Linq.XNode System.Xml.Linq.XNode::CloneNode()
// 0x00000045 System.Xml.XmlReaderSettings System.Xml.Linq.XNode::GetXmlReaderSettings(System.Xml.Linq.LoadOptions)
extern void XNode_GetXmlReaderSettings_m21904651ECE11BB82BE39E7E78CEB7EE16C26690 (void);
// 0x00000046 System.String System.Xml.Linq.XNode::GetXmlString(System.Xml.Linq.SaveOptions)
extern void XNode_GetXmlString_m95A05D4CD2CD05B13A1C9BDF93DDCF275EFC59B4 (void);
// 0x00000047 System.Void System.Xml.Linq.XText::.ctor(System.String)
extern void XText__ctor_m0A3EACDE243AC0873E6206A9A5A464D331875E06 (void);
// 0x00000048 System.Void System.Xml.Linq.XText::.ctor(System.Xml.Linq.XText)
extern void XText__ctor_m6F531A8CC0FF187696E6F9E6EB7B6FAD5986BCD1 (void);
// 0x00000049 System.Xml.XmlNodeType System.Xml.Linq.XText::get_NodeType()
extern void XText_get_NodeType_m9FE626CA1D7E6DC8F40EA32D080E7D4ADAD8748D (void);
// 0x0000004A System.String System.Xml.Linq.XText::get_Value()
extern void XText_get_Value_mAFC7BF7D7592EC2A209C4D9099FE80AF5D8C11BD (void);
// 0x0000004B System.Void System.Xml.Linq.XText::WriteTo(System.Xml.XmlWriter)
extern void XText_WriteTo_m6B3131200C5B437503451E904F3D50C9DF7B2C52 (void);
// 0x0000004C System.Void System.Xml.Linq.XText::AppendText(System.Text.StringBuilder)
extern void XText_AppendText_mE54274616BB3F9DE8CA95E75DA173A0FF564ADF0 (void);
// 0x0000004D System.Xml.Linq.XNode System.Xml.Linq.XText::CloneNode()
extern void XText_CloneNode_mADC3EBB72502874B6A79B6C67F6DD772C827506D (void);
// 0x0000004E System.Void System.Xml.Linq.XCData::.ctor(System.String)
extern void XCData__ctor_m137776E830857540338CE2AAB8B83EAF50FA6088 (void);
// 0x0000004F System.Void System.Xml.Linq.XCData::.ctor(System.Xml.Linq.XCData)
extern void XCData__ctor_m0F8FBFD5D2FC99F646C85571F04370AEF114744E (void);
// 0x00000050 System.Xml.XmlNodeType System.Xml.Linq.XCData::get_NodeType()
extern void XCData_get_NodeType_m600F2A7A80473E2EDCA30D7490AF6618F555A54E (void);
// 0x00000051 System.Void System.Xml.Linq.XCData::WriteTo(System.Xml.XmlWriter)
extern void XCData_WriteTo_m01B3EE1E48E0A916115E2C7B216195A99DFEA45D (void);
// 0x00000052 System.Xml.Linq.XNode System.Xml.Linq.XCData::CloneNode()
extern void XCData_CloneNode_mF62DC13E644DA175F0DFAA685DECE1F6453BAF2E (void);
// 0x00000053 System.Void System.Xml.Linq.XContainer::.ctor()
extern void XContainer__ctor_mB30E6406CDA48D8D0C6B414EFA7F7708B518BDF1 (void);
// 0x00000054 System.Void System.Xml.Linq.XContainer::.ctor(System.Xml.Linq.XContainer)
extern void XContainer__ctor_m8BB5B8E32EDD237865B0933CDD0E0339E4340BC6 (void);
// 0x00000055 System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XElement> System.Xml.Linq.XContainer::Descendants(System.Xml.Linq.XName)
extern void XContainer_Descendants_m425006D378AB50B27727B42308FC075375F5A668 (void);
// 0x00000056 System.Xml.Linq.XElement System.Xml.Linq.XContainer::Element(System.Xml.Linq.XName)
extern void XContainer_Element_m867E2EB66D483CCE0568D25C15F64CAF21575B67 (void);
// 0x00000057 System.Void System.Xml.Linq.XContainer::AddNodeSkipNotify(System.Xml.Linq.XNode)
extern void XContainer_AddNodeSkipNotify_mCA9792F6BB1EB81DB43A4616050B89C8BC342216 (void);
// 0x00000058 System.Void System.Xml.Linq.XContainer::AddStringSkipNotify(System.String)
extern void XContainer_AddStringSkipNotify_mE5E437A0139A87048A53CA355B5E8EF413717C9E (void);
// 0x00000059 System.Void System.Xml.Linq.XContainer::AppendNodeSkipNotify(System.Xml.Linq.XNode)
extern void XContainer_AppendNodeSkipNotify_m10C3918A7187895C8B004D3B43F707135F8B4189 (void);
// 0x0000005A System.Void System.Xml.Linq.XContainer::AppendText(System.Text.StringBuilder)
extern void XContainer_AppendText_mDC3F1D812CE2A088AD10758A73A322B28627E783 (void);
// 0x0000005B System.Void System.Xml.Linq.XContainer::ConvertTextToNode()
extern void XContainer_ConvertTextToNode_m1CED57A04715CD4B6AFE574294AA4435E858CE2A (void);
// 0x0000005C System.String System.Xml.Linq.XContainer::GetDateTimeString(System.DateTime)
extern void XContainer_GetDateTimeString_m796C652374BD60037EC674EE9FF87CDBDCEF4AC9 (void);
// 0x0000005D System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XElement> System.Xml.Linq.XContainer::GetDescendants(System.Xml.Linq.XName,System.Boolean)
extern void XContainer_GetDescendants_mE82C3B6ED5A31694F4AEC2709AEA09B8DD7FF5A8 (void);
// 0x0000005E System.String System.Xml.Linq.XContainer::GetStringValue(System.Object)
extern void XContainer_GetStringValue_m7C5431D52850BAC83E05435E126501F2C322CA39 (void);
// 0x0000005F System.Void System.Xml.Linq.XContainer::ReadContentFrom(System.Xml.XmlReader)
extern void XContainer_ReadContentFrom_mDF3F10E3761A010407EDD2B1701BD17F09FF6578 (void);
// 0x00000060 System.Void System.Xml.Linq.XContainer::ReadContentFrom(System.Xml.XmlReader,System.Xml.Linq.LoadOptions)
extern void XContainer_ReadContentFrom_m54CB5102E59D21957BF3DF512A589F69268C46E4 (void);
// 0x00000061 System.Void System.Xml.Linq.XContainer::ValidateNode(System.Xml.Linq.XNode,System.Xml.Linq.XNode)
extern void XContainer_ValidateNode_mFC2F7A818A4BF3AF4B82DAEF0E38B2CD9F431A30 (void);
// 0x00000062 System.Void System.Xml.Linq.XContainer::ValidateString(System.String)
extern void XContainer_ValidateString_mA262AE0924728ADF83CB24EF236F944048706689 (void);
// 0x00000063 System.Void System.Xml.Linq.XContainer::WriteContentTo(System.Xml.XmlWriter)
extern void XContainer_WriteContentTo_m685E1C3020AF83F1F1D5E53D74D77509A7B6E6D8 (void);
// 0x00000064 System.Void System.Xml.Linq.XContainer_<GetDescendants>d__39::.ctor(System.Int32)
extern void U3CGetDescendantsU3Ed__39__ctor_m7B171EFABDBA3789E5B32F52B269CDF3B26E38F7 (void);
// 0x00000065 System.Void System.Xml.Linq.XContainer_<GetDescendants>d__39::System.IDisposable.Dispose()
extern void U3CGetDescendantsU3Ed__39_System_IDisposable_Dispose_m7EB41E295C3DF7867F7F2DB34D307F6D44B6A214 (void);
// 0x00000066 System.Boolean System.Xml.Linq.XContainer_<GetDescendants>d__39::MoveNext()
extern void U3CGetDescendantsU3Ed__39_MoveNext_m96FEE57B6E8F95ED61F8CFF31FC40B3855FC6E11 (void);
// 0x00000067 System.Xml.Linq.XElement System.Xml.Linq.XContainer_<GetDescendants>d__39::System.Collections.Generic.IEnumerator<System.Xml.Linq.XElement>.get_Current()
extern void U3CGetDescendantsU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_Xml_Linq_XElementU3E_get_Current_mA50996D4BE37D57C02010059B1975E83986879BF (void);
// 0x00000068 System.Void System.Xml.Linq.XContainer_<GetDescendants>d__39::System.Collections.IEnumerator.Reset()
extern void U3CGetDescendantsU3Ed__39_System_Collections_IEnumerator_Reset_mEBDF66638C0F1C1360C90B6711174D356240104B (void);
// 0x00000069 System.Object System.Xml.Linq.XContainer_<GetDescendants>d__39::System.Collections.IEnumerator.get_Current()
extern void U3CGetDescendantsU3Ed__39_System_Collections_IEnumerator_get_Current_mAABB550D2670DB92F0F8DB7E47F9A63DF654383F (void);
// 0x0000006A System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XElement> System.Xml.Linq.XContainer_<GetDescendants>d__39::System.Collections.Generic.IEnumerable<System.Xml.Linq.XElement>.GetEnumerator()
extern void U3CGetDescendantsU3Ed__39_System_Collections_Generic_IEnumerableU3CSystem_Xml_Linq_XElementU3E_GetEnumerator_m9E2BF33FBBABF2CE488337FDA1F9E72E87AFBF6D (void);
// 0x0000006B System.Collections.IEnumerator System.Xml.Linq.XContainer_<GetDescendants>d__39::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetDescendantsU3Ed__39_System_Collections_IEnumerable_GetEnumerator_mC48E7E7056DF5805830B14874BBBA07C4CAB3814 (void);
// 0x0000006C System.Xml.Linq.XNamespace System.Xml.Linq.NamespaceCache::Get(System.String)
extern void NamespaceCache_Get_m617B1D8CD4BF120BB3ABDC1C2FA5D204E1157FF1_AdjustorThunk (void);
// 0x0000006D System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XElement> System.Xml.Linq.XElement::get_EmptySequence()
extern void XElement_get_EmptySequence_m7FF58487144D2E1343139552401AA9D93D84126D (void);
// 0x0000006E System.Void System.Xml.Linq.XElement::.ctor(System.Xml.Linq.XName)
extern void XElement__ctor_m6598392EE71FFB985FD2649DA0AF79A6B04754B9 (void);
// 0x0000006F System.Void System.Xml.Linq.XElement::.ctor(System.Xml.Linq.XElement)
extern void XElement__ctor_m1B9977917FB1B8E3FEB1D95F9A7FA08575AFB54C (void);
// 0x00000070 System.Xml.Linq.XName System.Xml.Linq.XElement::get_Name()
extern void XElement_get_Name_m46016ACB0276F3197414EC13D354B5C50E5F1ECD (void);
// 0x00000071 System.Xml.XmlNodeType System.Xml.Linq.XElement::get_NodeType()
extern void XElement_get_NodeType_mD94D10A57FA920767D963C0878F73BC9E8CD9A75 (void);
// 0x00000072 System.String System.Xml.Linq.XElement::get_Value()
extern void XElement_get_Value_m58EC8F265EC7B8921070A30D281CA3E24AF5D4FF (void);
// 0x00000073 System.String System.Xml.Linq.XElement::GetPrefixOfNamespace(System.Xml.Linq.XNamespace)
extern void XElement_GetPrefixOfNamespace_m416B5CBFDDFAF2E72B2244F31891BB3A74253BA7 (void);
// 0x00000074 System.Void System.Xml.Linq.XElement::WriteTo(System.Xml.XmlWriter)
extern void XElement_WriteTo_mA05B3D7F76873055C3A2307C2E2FCC57719767CA (void);
// 0x00000075 System.Void System.Xml.Linq.XElement::AppendAttributeSkipNotify(System.Xml.Linq.XAttribute)
extern void XElement_AppendAttributeSkipNotify_m2B3076226DD2C736CA637F610FE3617277FF0051 (void);
// 0x00000076 System.Xml.Linq.XNode System.Xml.Linq.XElement::CloneNode()
extern void XElement_CloneNode_mDE6871E3AAFED49A68406A018E620C468118A727 (void);
// 0x00000077 System.String System.Xml.Linq.XElement::GetNamespaceOfPrefixInScope(System.String,System.Xml.Linq.XElement)
extern void XElement_GetNamespaceOfPrefixInScope_mECA610CCD1AFA0B0EBB853E042310E4AE1932306 (void);
// 0x00000078 System.Void System.Xml.Linq.XElement::SetEndElementLineInfo(System.Int32,System.Int32)
extern void XElement_SetEndElementLineInfo_mE21F92F942D54250C37C9D1257F61DBF6EA43024 (void);
// 0x00000079 System.Void System.Xml.Linq.XElement::ValidateNode(System.Xml.Linq.XNode,System.Xml.Linq.XNode)
extern void XElement_ValidateNode_mCADFA1A4571AB6521A113F0BDD8044205710B43C (void);
// 0x0000007A System.Void System.Xml.Linq.ElementWriter::.ctor(System.Xml.XmlWriter)
extern void ElementWriter__ctor_mF5EC39D2FACA052CD0400649F6C97439C50F2CCD_AdjustorThunk (void);
// 0x0000007B System.Void System.Xml.Linq.ElementWriter::WriteElement(System.Xml.Linq.XElement)
extern void ElementWriter_WriteElement_mE7B0DBF3466AD8E41EE575B19E1BC46A8B20B265_AdjustorThunk (void);
// 0x0000007C System.String System.Xml.Linq.ElementWriter::GetPrefixOfNamespace(System.Xml.Linq.XNamespace,System.Boolean)
extern void ElementWriter_GetPrefixOfNamespace_mFBF3B564CA259C011D62B50AE4D1F82350D3DDAE_AdjustorThunk (void);
// 0x0000007D System.Void System.Xml.Linq.ElementWriter::PushAncestors(System.Xml.Linq.XElement)
extern void ElementWriter_PushAncestors_mC23EA98721039488FB1740BD9BBC776F9467992B_AdjustorThunk (void);
// 0x0000007E System.Void System.Xml.Linq.ElementWriter::PushElement(System.Xml.Linq.XElement)
extern void ElementWriter_PushElement_mDD333F8219B6F32C63B3E4983C587EDAA2858816_AdjustorThunk (void);
// 0x0000007F System.Void System.Xml.Linq.ElementWriter::WriteEndElement()
extern void ElementWriter_WriteEndElement_m9E53D14DECD22552E12997DD6A524F1F9D21A062_AdjustorThunk (void);
// 0x00000080 System.Void System.Xml.Linq.ElementWriter::WriteFullEndElement()
extern void ElementWriter_WriteFullEndElement_mD95A4447FC6330D7021BAD141388136B05433C60_AdjustorThunk (void);
// 0x00000081 System.Void System.Xml.Linq.ElementWriter::WriteStartElement(System.Xml.Linq.XElement)
extern void ElementWriter_WriteStartElement_m2538ED7269F8AFB5DB20DD85F150251A384709E0_AdjustorThunk (void);
// 0x00000082 System.Void System.Xml.Linq.NamespaceResolver::PushScope()
extern void NamespaceResolver_PushScope_m470CD4A84E54AE34219C93A3DBCC27782A5BB0CD_AdjustorThunk (void);
// 0x00000083 System.Void System.Xml.Linq.NamespaceResolver::PopScope()
extern void NamespaceResolver_PopScope_m53B07DE341947EA45B67FA9FB53910905840BB33_AdjustorThunk (void);
// 0x00000084 System.Void System.Xml.Linq.NamespaceResolver::Add(System.String,System.Xml.Linq.XNamespace)
extern void NamespaceResolver_Add_mC34C03B18174C62DB1EB1D2EA486E92FCFC359FC_AdjustorThunk (void);
// 0x00000085 System.Void System.Xml.Linq.NamespaceResolver::AddFirst(System.String,System.Xml.Linq.XNamespace)
extern void NamespaceResolver_AddFirst_mFA5E0D330A5BBE30FAD2FA8EEA5799A0971D65B1_AdjustorThunk (void);
// 0x00000086 System.String System.Xml.Linq.NamespaceResolver::GetPrefixOfNamespace(System.Xml.Linq.XNamespace,System.Boolean)
extern void NamespaceResolver_GetPrefixOfNamespace_mBDDDF4A31794C36CD5CEF25471A04DEC2D28DF9E_AdjustorThunk (void);
// 0x00000087 System.Void System.Xml.Linq.NamespaceResolver_NamespaceDeclaration::.ctor()
extern void NamespaceDeclaration__ctor_mBAE0A487DEA2888E7C675AE71FD28DABD12E38FC (void);
// 0x00000088 System.Void System.Xml.Linq.XDocument::.ctor()
extern void XDocument__ctor_mA19AE8E641537630474156158B0AA413B178F9D0 (void);
// 0x00000089 System.Void System.Xml.Linq.XDocument::.ctor(System.Xml.Linq.XDocument)
extern void XDocument__ctor_m0A2141DE41096648DF643513EFA087D32D1BC749 (void);
// 0x0000008A System.Void System.Xml.Linq.XDocument::set_Declaration(System.Xml.Linq.XDeclaration)
extern void XDocument_set_Declaration_mCBC64164EEE19F5D963FC89B4E2618E234CF8565 (void);
// 0x0000008B System.Xml.XmlNodeType System.Xml.Linq.XDocument::get_NodeType()
extern void XDocument_get_NodeType_m885F2923AE2D44B3D3A5A9FCC07F1B45EF815882 (void);
// 0x0000008C System.Xml.Linq.XElement System.Xml.Linq.XDocument::get_Root()
extern void XDocument_get_Root_mB8FBE6A2416946D861E32B34F5A756A91345BF00 (void);
// 0x0000008D System.Xml.Linq.XDocument System.Xml.Linq.XDocument::Load(System.Xml.XmlReader,System.Xml.Linq.LoadOptions)
extern void XDocument_Load_mB18063229902BEC3D71470E0573652781BFE0B3D (void);
// 0x0000008E System.Xml.Linq.XDocument System.Xml.Linq.XDocument::Parse(System.String)
extern void XDocument_Parse_mC150339001FE189E56A93C67404286967826B5FD (void);
// 0x0000008F System.Xml.Linq.XDocument System.Xml.Linq.XDocument::Parse(System.String,System.Xml.Linq.LoadOptions)
extern void XDocument_Parse_mDCD08F7B4433BB9928C368C76974B757FA1822D9 (void);
// 0x00000090 System.Void System.Xml.Linq.XDocument::WriteTo(System.Xml.XmlWriter)
extern void XDocument_WriteTo_mDE40AA006D14CE1FCB22AD36E98F3B59720827FF (void);
// 0x00000091 System.Xml.Linq.XNode System.Xml.Linq.XDocument::CloneNode()
extern void XDocument_CloneNode_m9F5A7F59C781C4C0A27B780F93F053E80547EE54 (void);
// 0x00000092 T System.Xml.Linq.XDocument::GetFirstNode()
// 0x00000093 System.Boolean System.Xml.Linq.XDocument::IsWhitespace(System.String)
extern void XDocument_IsWhitespace_m1D143440031BB2894BB0E08232050B18AD397F17 (void);
// 0x00000094 System.Void System.Xml.Linq.XDocument::ValidateNode(System.Xml.Linq.XNode,System.Xml.Linq.XNode)
extern void XDocument_ValidateNode_m0DA6C56577B09A88672FD004139153F33E0163B5 (void);
// 0x00000095 System.Void System.Xml.Linq.XDocument::ValidateDocument(System.Xml.Linq.XNode,System.Xml.XmlNodeType,System.Xml.XmlNodeType)
extern void XDocument_ValidateDocument_m98AFEA48AAA944A5949ABBCFF9A81C95B2EB7DC7 (void);
// 0x00000096 System.Void System.Xml.Linq.XDocument::ValidateString(System.String)
extern void XDocument_ValidateString_m787A90AEA54D2AB8E35E3481DD2FC14EA8314FF6 (void);
// 0x00000097 System.Void System.Xml.Linq.XComment::.ctor(System.String)
extern void XComment__ctor_mB8CAC011B4C5C0FD8E9FD9CF7E865E03C981938E (void);
// 0x00000098 System.Void System.Xml.Linq.XComment::.ctor(System.Xml.Linq.XComment)
extern void XComment__ctor_m28A1215BF1FEC8A9542F1FF9FEBC78DAF01C1DE3 (void);
// 0x00000099 System.Xml.XmlNodeType System.Xml.Linq.XComment::get_NodeType()
extern void XComment_get_NodeType_mB3E4E4292E74B655FA7BA48358433C189ECAEBA2 (void);
// 0x0000009A System.Void System.Xml.Linq.XComment::WriteTo(System.Xml.XmlWriter)
extern void XComment_WriteTo_m74138EDFEAA8BCA9701CADB65E817C081D3138B3 (void);
// 0x0000009B System.Xml.Linq.XNode System.Xml.Linq.XComment::CloneNode()
extern void XComment_CloneNode_m5045061010FD2F3E6EE637D565DAFEADAFBCB70D (void);
// 0x0000009C System.Void System.Xml.Linq.XProcessingInstruction::.ctor(System.String,System.String)
extern void XProcessingInstruction__ctor_m68DB736A1B5340D09C86ACD0FF17507DB308978F (void);
// 0x0000009D System.Void System.Xml.Linq.XProcessingInstruction::.ctor(System.Xml.Linq.XProcessingInstruction)
extern void XProcessingInstruction__ctor_mD355BE3DCE287836A5A6DFED8AD4FFC80B1014A8 (void);
// 0x0000009E System.Xml.XmlNodeType System.Xml.Linq.XProcessingInstruction::get_NodeType()
extern void XProcessingInstruction_get_NodeType_m3FEAC4998A2E2153032723D19F5F1A6E3F899EA1 (void);
// 0x0000009F System.Void System.Xml.Linq.XProcessingInstruction::WriteTo(System.Xml.XmlWriter)
extern void XProcessingInstruction_WriteTo_mB36F7772FD68FF208BFBB5562914F6B408332C52 (void);
// 0x000000A0 System.Xml.Linq.XNode System.Xml.Linq.XProcessingInstruction::CloneNode()
extern void XProcessingInstruction_CloneNode_m894C4E2233276435EC110D384AED395D481A7F45 (void);
// 0x000000A1 System.Void System.Xml.Linq.XProcessingInstruction::ValidateName(System.String)
extern void XProcessingInstruction_ValidateName_mA2196FD0B33D05D93C3C024C43C7E1CE1B0B5DCA (void);
// 0x000000A2 System.Void System.Xml.Linq.XDeclaration::.ctor(System.Xml.Linq.XDeclaration)
extern void XDeclaration__ctor_mDF8296DA144E6F8B168ABAB8ECD364BF98FE2201 (void);
// 0x000000A3 System.Void System.Xml.Linq.XDeclaration::.ctor(System.Xml.XmlReader)
extern void XDeclaration__ctor_m4FCAAD471F5EA6411F5D4528EE5DDEA7F0057AF8 (void);
// 0x000000A4 System.String System.Xml.Linq.XDeclaration::get_Standalone()
extern void XDeclaration_get_Standalone_mBA2716CE655FDFF0797D3CDEEBCBF60F5B756ED8 (void);
// 0x000000A5 System.String System.Xml.Linq.XDeclaration::ToString()
extern void XDeclaration_ToString_m9CF422CAB0DFC6EC2AF13443759AFC751D3C3176 (void);
// 0x000000A6 System.Void System.Xml.Linq.XDocumentType::.ctor(System.String,System.String,System.String,System.String)
extern void XDocumentType__ctor_m96057B4CAF2FA8E385A916DAC72F76788EACCA8E (void);
// 0x000000A7 System.Void System.Xml.Linq.XDocumentType::.ctor(System.Xml.Linq.XDocumentType)
extern void XDocumentType__ctor_m271051C72D313E83008CA7C812C01A220F70D0AF (void);
// 0x000000A8 System.Void System.Xml.Linq.XDocumentType::.ctor(System.String,System.String,System.String,System.String,System.Xml.IDtdInfo)
extern void XDocumentType__ctor_m6E667F5616C60D7E44A69D5ADD72713D88E7779C (void);
// 0x000000A9 System.Xml.XmlNodeType System.Xml.Linq.XDocumentType::get_NodeType()
extern void XDocumentType_get_NodeType_m33096AE14FDE212B323B4040C58C9426AC43D4DC (void);
// 0x000000AA System.Void System.Xml.Linq.XDocumentType::WriteTo(System.Xml.XmlWriter)
extern void XDocumentType_WriteTo_mAD3CE6CA16EB048E5830AE074511A5BD9754208D (void);
// 0x000000AB System.Xml.Linq.XNode System.Xml.Linq.XDocumentType::CloneNode()
extern void XDocumentType_CloneNode_m1D5F9DE76CB2C67E2485856AF850983C5F7BF7E3 (void);
// 0x000000AC System.Void System.Xml.Linq.XAttribute::.ctor(System.Xml.Linq.XName,System.Object)
extern void XAttribute__ctor_m60BAA1AC6ED0AEC8FAE4822DE951FDBA3088C726 (void);
// 0x000000AD System.Void System.Xml.Linq.XAttribute::.ctor(System.Xml.Linq.XAttribute)
extern void XAttribute__ctor_mE44888FB074027ACDEC63862A55E68DAA4734D93 (void);
// 0x000000AE System.Boolean System.Xml.Linq.XAttribute::get_IsNamespaceDeclaration()
extern void XAttribute_get_IsNamespaceDeclaration_mF2F6A018FD0377B321764CC967936EA1DE15D43D (void);
// 0x000000AF System.Xml.Linq.XName System.Xml.Linq.XAttribute::get_Name()
extern void XAttribute_get_Name_m44CB8DEBEF318BF2D1C5B6B9213DD49551780D16 (void);
// 0x000000B0 System.Xml.XmlNodeType System.Xml.Linq.XAttribute::get_NodeType()
extern void XAttribute_get_NodeType_m6F2A94333292F14870CBD65E7E4F5203F9961D30 (void);
// 0x000000B1 System.String System.Xml.Linq.XAttribute::get_Value()
extern void XAttribute_get_Value_m91DDE7E38DA35BF3E74E462C647D63125ECA7C4A (void);
// 0x000000B2 System.String System.Xml.Linq.XAttribute::ToString()
extern void XAttribute_ToString_m8F8DF424AA30D926EDD35D77F53FB71F2D7DEBD2 (void);
// 0x000000B3 System.String System.Xml.Linq.XAttribute::GetPrefixOfNamespace(System.Xml.Linq.XNamespace)
extern void XAttribute_GetPrefixOfNamespace_mD44D3EACD0DE9F08B9AE138BE3F135287386E7AB (void);
// 0x000000B4 System.Void System.Xml.Linq.XAttribute::ValidateAttribute(System.Xml.Linq.XName,System.String)
extern void XAttribute_ValidateAttribute_m03CE15615703DC8C44E9BA57F30409884F9B2E87 (void);
// 0x000000B5 System.String System.Xml.Linq.Res::GetString(System.String)
extern void Res_GetString_m394A7CC04CBBC770E0476D906DC3D22D6711F3AD (void);
// 0x000000B6 System.String System.Xml.Linq.Res::GetString(System.String,System.Object[])
extern void Res_GetString_m011D9682DD3F461A6D93997DC39FE4D6355E82C0 (void);
// 0x000000B7 System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_m8B7D591B7AC5FDB664F0C6F1161A4720EA315488 (void);
// 0x000000B8 System.Void Unity.ThrowStub::ThrowNotSupportedException()
extern void ThrowStub_ThrowNotSupportedException_m2B95525055210633B02DAAC664B40119828A33F8 (void);
static Il2CppMethodPointer s_methodPointers[184] = 
{
	XName__ctor_m11BAB50C83C349465FFB63B5EEDDAF713C11942F,
	XName_get_LocalName_mC959CACE19C206C85FAD179AB086DD11BF5221E3,
	XName_get_Namespace_mBD833FBB757D8C41E9D2404F5BE036B8E446CBBC,
	XName_get_NamespaceName_m5F826139218C51FEDAC51BF97453C7E4525D176F,
	XName_ToString_m611A4E60C1783A842415A480C1EED1C273EE5AA3,
	XName_Get_m5DC4D0C2C5C965F51164366808A577A7B6989D13,
	XName_op_Implicit_mD82E6E7414AED8E24BB48B7F2914C5A21EE4AF3F,
	XName_Equals_m48725EB0D37130A1341509CAC239670F6530152D,
	XName_GetHashCode_mC0E9FDF696C03C889DC391F229D373612D1F3998,
	XName_op_Equality_m0D26C036FA8957FE3419F714BA1B0371AE0B7598,
	XName_op_Inequality_mAD594A0AC5072A85071F8FBADBFD52936530232A,
	XName_System_IEquatableU3CSystem_Xml_Linq_XNameU3E_Equals_mA462744020AE77E5E3E892ABEA5B012F1EFBD91C,
	XName_System_Runtime_Serialization_ISerializable_GetObjectData_mAC29C083365CDAAD94099D2E05E20B6601C90F4C,
	XName__ctor_m4BD5071BDD613AC497E8CB4CC5F3BEA550F22C07,
	NameSerializer__ctor_m8F1196AF1E27A0FD7C7DFB1BC9CDA9C48AB91BCC,
	NameSerializer_System_Runtime_Serialization_IObjectReference_GetRealObject_mAF50D783824AAF24448B624A4C9B9D6064283B9F,
	NameSerializer_System_Runtime_Serialization_ISerializable_GetObjectData_m9FAC4FC6119A316EC9E033F9DA26C2F88C53B1F7,
	XNamespace__ctor_m9E2F1387816E4F402AD6E52F0AF0B73FA9DE4253,
	XNamespace_get_NamespaceName_m805748C83B347006CC65F0ECC5B1C9124213557E,
	XNamespace_GetName_mE5576274C571BF2BCFDB2185A1C18EFA115FC6C7,
	XNamespace_ToString_m64DA46BA983C78CDD07B3E77D09E6019F2D1AC65,
	XNamespace_get_None_mF6622A086F9CD5ED2DB377D831B9AAA15C56C2F7,
	XNamespace_get_Xml_mF6025F66B00A6841332A05092EE781E12B162274,
	XNamespace_get_Xmlns_m6DB2CF70CF8044C21834146A91615CBFCC9F162F,
	XNamespace_Get_mE26B2D740E108101DA4687D724EC4DD7FFD57AF6,
	XNamespace_Equals_m8BDD54FF83402923462E8994852C4594A6D94C6C,
	XNamespace_GetHashCode_mCBB26B617C1C85E5B26E6847F48A56F7B5B3318D,
	XNamespace_op_Equality_mC02B35B87BD22807C6CA43815E94170EFAC261AC,
	XNamespace_op_Inequality_mA84E8D1A01DB9A00F467B6214F1EB28C7140479C,
	XNamespace_GetName_mC7F14EFE2F8B4B4E0CBF944B0CF90DFED1575108,
	XNamespace_Get_mF6BCD4BC1C153E6080A751A7B3786732BD32659E,
	XNamespace_ExtractLocalName_mDB91372235FDAED58052AB971963F79C3D23D3C0,
	XNamespace_ExtractNamespace_m1F6A139C9924E2464BB0771E05D3CE39833440E8,
	XNamespace_EnsureNamespace_mDEFD4E69216E12031B2D6EF170213023A21C1749,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	XObject__ctor_m7C54F218B4232FE2278ED1F8CEEB51FA9D8059E3,
	XObject_get_BaseUri_m21E44D54AF7D631DA1017ECC8856E77D8FAF771B,
	NULL,
	XObject_AddAnnotation_m90169D87A51DB9EC16D18F66D63059B515536C17,
	XObject_Annotation_m9C08117D1974D059A084A25D6FAAFA91754A6ACC,
	NULL,
	XObject_System_Xml_IXmlLineInfo_HasLineInfo_m45AFB5B7292EEEDC22C26EADF67868F6C784CE96,
	XObject_System_Xml_IXmlLineInfo_get_LineNumber_m699F2F923FACEF97C28D7721D7ED2CCFB3401AC3,
	XObject_System_Xml_IXmlLineInfo_get_LinePosition_m6EFA0448297C2FEBB187D0763D7E4C26A4AF7F33,
	XObject_get_HasBaseUri_m571789C97A20A08EE48E3AE293E564769523691D,
	XObject_SetBaseUri_mFD7D5326440D073876DDE99C768CA6585946B0FE,
	XObject_SetLineInfo_m9BF3FC30A56328AEAD32EA57AF6C0A635FE4D687,
	XObject_GetSaveOptionsFromAnnotations_mA6A8CFD79C02801110BD726ACBCEDF272729DE92,
	BaseUriAnnotation__ctor_mB42CD28291DD19E64ABE16FA4C8041ED73291E7E,
	LineInfoAnnotation__ctor_mBE2123924099AC38A102AAB235760B95220E7F1A,
	LineInfoEndElementAnnotation__ctor_mCA8A79B2D5736641FBA29C8CCEDCD3DCD06A2E4A,
	XNode__ctor_m4AF8DD0B3E4B1667BCAC37789ADBC8901B3E4ECA,
	XNode_ToString_m5812DCAA56C17BBA26CC6D429C669D1C5E5CF0F2,
	NULL,
	XNode_AppendText_m166AA963267247C2CD9F80CA8A1043B2C9AAF52E,
	NULL,
	XNode_GetXmlReaderSettings_m21904651ECE11BB82BE39E7E78CEB7EE16C26690,
	XNode_GetXmlString_m95A05D4CD2CD05B13A1C9BDF93DDCF275EFC59B4,
	XText__ctor_m0A3EACDE243AC0873E6206A9A5A464D331875E06,
	XText__ctor_m6F531A8CC0FF187696E6F9E6EB7B6FAD5986BCD1,
	XText_get_NodeType_m9FE626CA1D7E6DC8F40EA32D080E7D4ADAD8748D,
	XText_get_Value_mAFC7BF7D7592EC2A209C4D9099FE80AF5D8C11BD,
	XText_WriteTo_m6B3131200C5B437503451E904F3D50C9DF7B2C52,
	XText_AppendText_mE54274616BB3F9DE8CA95E75DA173A0FF564ADF0,
	XText_CloneNode_mADC3EBB72502874B6A79B6C67F6DD772C827506D,
	XCData__ctor_m137776E830857540338CE2AAB8B83EAF50FA6088,
	XCData__ctor_m0F8FBFD5D2FC99F646C85571F04370AEF114744E,
	XCData_get_NodeType_m600F2A7A80473E2EDCA30D7490AF6618F555A54E,
	XCData_WriteTo_m01B3EE1E48E0A916115E2C7B216195A99DFEA45D,
	XCData_CloneNode_mF62DC13E644DA175F0DFAA685DECE1F6453BAF2E,
	XContainer__ctor_mB30E6406CDA48D8D0C6B414EFA7F7708B518BDF1,
	XContainer__ctor_m8BB5B8E32EDD237865B0933CDD0E0339E4340BC6,
	XContainer_Descendants_m425006D378AB50B27727B42308FC075375F5A668,
	XContainer_Element_m867E2EB66D483CCE0568D25C15F64CAF21575B67,
	XContainer_AddNodeSkipNotify_mCA9792F6BB1EB81DB43A4616050B89C8BC342216,
	XContainer_AddStringSkipNotify_mE5E437A0139A87048A53CA355B5E8EF413717C9E,
	XContainer_AppendNodeSkipNotify_m10C3918A7187895C8B004D3B43F707135F8B4189,
	XContainer_AppendText_mDC3F1D812CE2A088AD10758A73A322B28627E783,
	XContainer_ConvertTextToNode_m1CED57A04715CD4B6AFE574294AA4435E858CE2A,
	XContainer_GetDateTimeString_m796C652374BD60037EC674EE9FF87CDBDCEF4AC9,
	XContainer_GetDescendants_mE82C3B6ED5A31694F4AEC2709AEA09B8DD7FF5A8,
	XContainer_GetStringValue_m7C5431D52850BAC83E05435E126501F2C322CA39,
	XContainer_ReadContentFrom_mDF3F10E3761A010407EDD2B1701BD17F09FF6578,
	XContainer_ReadContentFrom_m54CB5102E59D21957BF3DF512A589F69268C46E4,
	XContainer_ValidateNode_mFC2F7A818A4BF3AF4B82DAEF0E38B2CD9F431A30,
	XContainer_ValidateString_mA262AE0924728ADF83CB24EF236F944048706689,
	XContainer_WriteContentTo_m685E1C3020AF83F1F1D5E53D74D77509A7B6E6D8,
	U3CGetDescendantsU3Ed__39__ctor_m7B171EFABDBA3789E5B32F52B269CDF3B26E38F7,
	U3CGetDescendantsU3Ed__39_System_IDisposable_Dispose_m7EB41E295C3DF7867F7F2DB34D307F6D44B6A214,
	U3CGetDescendantsU3Ed__39_MoveNext_m96FEE57B6E8F95ED61F8CFF31FC40B3855FC6E11,
	U3CGetDescendantsU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_Xml_Linq_XElementU3E_get_Current_mA50996D4BE37D57C02010059B1975E83986879BF,
	U3CGetDescendantsU3Ed__39_System_Collections_IEnumerator_Reset_mEBDF66638C0F1C1360C90B6711174D356240104B,
	U3CGetDescendantsU3Ed__39_System_Collections_IEnumerator_get_Current_mAABB550D2670DB92F0F8DB7E47F9A63DF654383F,
	U3CGetDescendantsU3Ed__39_System_Collections_Generic_IEnumerableU3CSystem_Xml_Linq_XElementU3E_GetEnumerator_m9E2BF33FBBABF2CE488337FDA1F9E72E87AFBF6D,
	U3CGetDescendantsU3Ed__39_System_Collections_IEnumerable_GetEnumerator_mC48E7E7056DF5805830B14874BBBA07C4CAB3814,
	NamespaceCache_Get_m617B1D8CD4BF120BB3ABDC1C2FA5D204E1157FF1_AdjustorThunk,
	XElement_get_EmptySequence_m7FF58487144D2E1343139552401AA9D93D84126D,
	XElement__ctor_m6598392EE71FFB985FD2649DA0AF79A6B04754B9,
	XElement__ctor_m1B9977917FB1B8E3FEB1D95F9A7FA08575AFB54C,
	XElement_get_Name_m46016ACB0276F3197414EC13D354B5C50E5F1ECD,
	XElement_get_NodeType_mD94D10A57FA920767D963C0878F73BC9E8CD9A75,
	XElement_get_Value_m58EC8F265EC7B8921070A30D281CA3E24AF5D4FF,
	XElement_GetPrefixOfNamespace_m416B5CBFDDFAF2E72B2244F31891BB3A74253BA7,
	XElement_WriteTo_mA05B3D7F76873055C3A2307C2E2FCC57719767CA,
	XElement_AppendAttributeSkipNotify_m2B3076226DD2C736CA637F610FE3617277FF0051,
	XElement_CloneNode_mDE6871E3AAFED49A68406A018E620C468118A727,
	XElement_GetNamespaceOfPrefixInScope_mECA610CCD1AFA0B0EBB853E042310E4AE1932306,
	XElement_SetEndElementLineInfo_mE21F92F942D54250C37C9D1257F61DBF6EA43024,
	XElement_ValidateNode_mCADFA1A4571AB6521A113F0BDD8044205710B43C,
	ElementWriter__ctor_mF5EC39D2FACA052CD0400649F6C97439C50F2CCD_AdjustorThunk,
	ElementWriter_WriteElement_mE7B0DBF3466AD8E41EE575B19E1BC46A8B20B265_AdjustorThunk,
	ElementWriter_GetPrefixOfNamespace_mFBF3B564CA259C011D62B50AE4D1F82350D3DDAE_AdjustorThunk,
	ElementWriter_PushAncestors_mC23EA98721039488FB1740BD9BBC776F9467992B_AdjustorThunk,
	ElementWriter_PushElement_mDD333F8219B6F32C63B3E4983C587EDAA2858816_AdjustorThunk,
	ElementWriter_WriteEndElement_m9E53D14DECD22552E12997DD6A524F1F9D21A062_AdjustorThunk,
	ElementWriter_WriteFullEndElement_mD95A4447FC6330D7021BAD141388136B05433C60_AdjustorThunk,
	ElementWriter_WriteStartElement_m2538ED7269F8AFB5DB20DD85F150251A384709E0_AdjustorThunk,
	NamespaceResolver_PushScope_m470CD4A84E54AE34219C93A3DBCC27782A5BB0CD_AdjustorThunk,
	NamespaceResolver_PopScope_m53B07DE341947EA45B67FA9FB53910905840BB33_AdjustorThunk,
	NamespaceResolver_Add_mC34C03B18174C62DB1EB1D2EA486E92FCFC359FC_AdjustorThunk,
	NamespaceResolver_AddFirst_mFA5E0D330A5BBE30FAD2FA8EEA5799A0971D65B1_AdjustorThunk,
	NamespaceResolver_GetPrefixOfNamespace_mBDDDF4A31794C36CD5CEF25471A04DEC2D28DF9E_AdjustorThunk,
	NamespaceDeclaration__ctor_mBAE0A487DEA2888E7C675AE71FD28DABD12E38FC,
	XDocument__ctor_mA19AE8E641537630474156158B0AA413B178F9D0,
	XDocument__ctor_m0A2141DE41096648DF643513EFA087D32D1BC749,
	XDocument_set_Declaration_mCBC64164EEE19F5D963FC89B4E2618E234CF8565,
	XDocument_get_NodeType_m885F2923AE2D44B3D3A5A9FCC07F1B45EF815882,
	XDocument_get_Root_mB8FBE6A2416946D861E32B34F5A756A91345BF00,
	XDocument_Load_mB18063229902BEC3D71470E0573652781BFE0B3D,
	XDocument_Parse_mC150339001FE189E56A93C67404286967826B5FD,
	XDocument_Parse_mDCD08F7B4433BB9928C368C76974B757FA1822D9,
	XDocument_WriteTo_mDE40AA006D14CE1FCB22AD36E98F3B59720827FF,
	XDocument_CloneNode_m9F5A7F59C781C4C0A27B780F93F053E80547EE54,
	NULL,
	XDocument_IsWhitespace_m1D143440031BB2894BB0E08232050B18AD397F17,
	XDocument_ValidateNode_m0DA6C56577B09A88672FD004139153F33E0163B5,
	XDocument_ValidateDocument_m98AFEA48AAA944A5949ABBCFF9A81C95B2EB7DC7,
	XDocument_ValidateString_m787A90AEA54D2AB8E35E3481DD2FC14EA8314FF6,
	XComment__ctor_mB8CAC011B4C5C0FD8E9FD9CF7E865E03C981938E,
	XComment__ctor_m28A1215BF1FEC8A9542F1FF9FEBC78DAF01C1DE3,
	XComment_get_NodeType_mB3E4E4292E74B655FA7BA48358433C189ECAEBA2,
	XComment_WriteTo_m74138EDFEAA8BCA9701CADB65E817C081D3138B3,
	XComment_CloneNode_m5045061010FD2F3E6EE637D565DAFEADAFBCB70D,
	XProcessingInstruction__ctor_m68DB736A1B5340D09C86ACD0FF17507DB308978F,
	XProcessingInstruction__ctor_mD355BE3DCE287836A5A6DFED8AD4FFC80B1014A8,
	XProcessingInstruction_get_NodeType_m3FEAC4998A2E2153032723D19F5F1A6E3F899EA1,
	XProcessingInstruction_WriteTo_mB36F7772FD68FF208BFBB5562914F6B408332C52,
	XProcessingInstruction_CloneNode_m894C4E2233276435EC110D384AED395D481A7F45,
	XProcessingInstruction_ValidateName_mA2196FD0B33D05D93C3C024C43C7E1CE1B0B5DCA,
	XDeclaration__ctor_mDF8296DA144E6F8B168ABAB8ECD364BF98FE2201,
	XDeclaration__ctor_m4FCAAD471F5EA6411F5D4528EE5DDEA7F0057AF8,
	XDeclaration_get_Standalone_mBA2716CE655FDFF0797D3CDEEBCBF60F5B756ED8,
	XDeclaration_ToString_m9CF422CAB0DFC6EC2AF13443759AFC751D3C3176,
	XDocumentType__ctor_m96057B4CAF2FA8E385A916DAC72F76788EACCA8E,
	XDocumentType__ctor_m271051C72D313E83008CA7C812C01A220F70D0AF,
	XDocumentType__ctor_m6E667F5616C60D7E44A69D5ADD72713D88E7779C,
	XDocumentType_get_NodeType_m33096AE14FDE212B323B4040C58C9426AC43D4DC,
	XDocumentType_WriteTo_mAD3CE6CA16EB048E5830AE074511A5BD9754208D,
	XDocumentType_CloneNode_m1D5F9DE76CB2C67E2485856AF850983C5F7BF7E3,
	XAttribute__ctor_m60BAA1AC6ED0AEC8FAE4822DE951FDBA3088C726,
	XAttribute__ctor_mE44888FB074027ACDEC63862A55E68DAA4734D93,
	XAttribute_get_IsNamespaceDeclaration_mF2F6A018FD0377B321764CC967936EA1DE15D43D,
	XAttribute_get_Name_m44CB8DEBEF318BF2D1C5B6B9213DD49551780D16,
	XAttribute_get_NodeType_m6F2A94333292F14870CBD65E7E4F5203F9961D30,
	XAttribute_get_Value_m91DDE7E38DA35BF3E74E462C647D63125ECA7C4A,
	XAttribute_ToString_m8F8DF424AA30D926EDD35D77F53FB71F2D7DEBD2,
	XAttribute_GetPrefixOfNamespace_mD44D3EACD0DE9F08B9AE138BE3F135287386E7AB,
	XAttribute_ValidateAttribute_m03CE15615703DC8C44E9BA57F30409884F9B2E87,
	Res_GetString_m394A7CC04CBBC770E0476D906DC3D22D6711F3AD,
	Res_GetString_m011D9682DD3F461A6D93997DC39FE4D6355E82C0,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_m8B7D591B7AC5FDB664F0C6F1161A4720EA315488,
	ThrowStub_ThrowNotSupportedException_m2B95525055210633B02DAAC664B40119828A33F8,
};
static const int32_t s_InvokerIndices[184] = 
{
	27,
	14,
	14,
	14,
	14,
	0,
	0,
	9,
	10,
	135,
	135,
	9,
	111,
	23,
	111,
	564,
	111,
	26,
	14,
	28,
	14,
	4,
	4,
	4,
	0,
	9,
	10,
	135,
	135,
	54,
	193,
	0,
	0,
	682,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	14,
	10,
	26,
	28,
	-1,
	89,
	10,
	10,
	89,
	26,
	129,
	10,
	26,
	129,
	129,
	23,
	14,
	26,
	26,
	14,
	43,
	34,
	26,
	26,
	10,
	14,
	26,
	26,
	14,
	26,
	26,
	10,
	26,
	14,
	23,
	26,
	28,
	28,
	26,
	26,
	26,
	26,
	23,
	1362,
	148,
	0,
	26,
	130,
	27,
	26,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	14,
	28,
	4,
	26,
	26,
	14,
	10,
	14,
	28,
	26,
	26,
	14,
	105,
	129,
	27,
	26,
	26,
	148,
	26,
	26,
	23,
	23,
	26,
	23,
	23,
	27,
	27,
	148,
	23,
	23,
	26,
	26,
	10,
	14,
	119,
	0,
	119,
	26,
	14,
	-1,
	114,
	27,
	35,
	26,
	26,
	26,
	10,
	26,
	14,
	27,
	26,
	10,
	26,
	14,
	154,
	26,
	26,
	14,
	14,
	428,
	26,
	791,
	10,
	26,
	14,
	27,
	26,
	89,
	14,
	10,
	14,
	14,
	28,
	137,
	0,
	1,
	94,
	3,
};
static const Il2CppTokenRangePair s_rgctxIndices[4] = 
{
	{ 0x02000005, { 0, 5 } },
	{ 0x02000007, { 5, 8 } },
	{ 0x06000035, { 13, 1 } },
	{ 0x06000092, { 14, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[15] = 
{
	{ (Il2CppRGCTXDataType)2, 23548 },
	{ (Il2CppRGCTXDataType)3, 17254 },
	{ (Il2CppRGCTXDataType)3, 17255 },
	{ (Il2CppRGCTXDataType)3, 17256 },
	{ (Il2CppRGCTXDataType)3, 17257 },
	{ (Il2CppRGCTXDataType)2, 23549 },
	{ (Il2CppRGCTXDataType)3, 17258 },
	{ (Il2CppRGCTXDataType)2, 19027 },
	{ (Il2CppRGCTXDataType)3, 17259 },
	{ (Il2CppRGCTXDataType)3, 17260 },
	{ (Il2CppRGCTXDataType)3, 17261 },
	{ (Il2CppRGCTXDataType)2, 19027 },
	{ (Il2CppRGCTXDataType)3, 17262 },
	{ (Il2CppRGCTXDataType)2, 19036 },
	{ (Il2CppRGCTXDataType)2, 19081 },
};
extern const Il2CppCodeGenModule g_System_Xml_LinqCodeGenModule;
const Il2CppCodeGenModule g_System_Xml_LinqCodeGenModule = 
{
	"System.Xml.Linq.dll",
	184,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	4,
	s_rgctxIndices,
	15,
	s_rgctxValues,
	NULL,
};
