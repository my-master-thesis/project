using System;
using System.Xml.Serialization;
namespace Service.Models
{
    [Serializable]
    [XmlTypeAttribute(AnonymousType = true)]
    [XmlRootAttribute(Namespace = "Item", IsNullable = false)]
    public class ItemData
    {
        [XmlAttribute("requested_quantity")]
        public int requested_quantity;
    
        [XmlAttribute("available_on_stock")]
        public int available_on_stock;
    
        [XmlAttribute("next_delivery_date")]
        public string next_delivery_date;
        
        [XmlAttribute("grossprice")]
        public double grossprice;
    
        [XmlAttribute("netprice")]
        public double netprice;
    
        [XmlAttribute("currency")]
        public string currency;
        
        [XmlAttribute("blaetterkatalog")]
        public string blaetterkatalog;
    }
}