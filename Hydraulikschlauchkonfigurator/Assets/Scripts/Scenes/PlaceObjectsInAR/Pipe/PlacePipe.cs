using System;
using System.Collections.Generic;
using System.Linq;
using Helper;
using InputHandler;
using Loading;
using Service.Models;
using Service.Singletons;
using SplineMesh;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace Scenes.PlaceObjectsInAR.Pipe
{
    [RequireComponent(typeof(ARRaycastManager))]
    public class PlacePipe : MonoBehaviour
    {
        [SerializeField] private GameObject measurementPointPrefab;
        [SerializeField] private GameObject penPoint;
        [SerializeField] private TMP_Text distanceText;
        [SerializeField] private TMP_Text depthLabel;
        [SerializeField] private TMP_Text waypointLabel;
        [SerializeField] private Button addButton;
        [SerializeField] private Button removeLastButton;
        [SerializeField] private Button finishButton;
        [SerializeField] private GameObject pipe;
        
        private ARRaycastManager _arRaycastManager;
        private ARPointCloudManager _arPointCloudManager;
        private GameObject _currentPoint;
        private float _distance;
        private PipeData _selectedPipe = new PipeData();
        
        private const string NextScene = "ChooseArmaturesScreen";
        
        private readonly Create3DPipe _create3DPipe = new Create3DPipe();
        private readonly Vector2 _screenCenter = new Vector3(Screen.width / 2, Screen.height / 2);
        
        public static List<GameObject> _wayPoints = new List<GameObject>();
        private static readonly List<ARRaycastHit> Hits = new List<ARRaycastHit>();


        private void Start()
        {
            ResetScene();
        }

        private void Update()
        {
            if (_currentPoint == null) return;
            if (ButtonSwitcher.FeaturePointBased)
            {
                _arPointCloudManager.enabled = true;
                Raycasting();
            }
            else
            {
                _arPointCloudManager.enabled = false;
                addButton.interactable = true;
                _currentPoint.SetActive(false);
                depthLabel.enabled = false;
            }
        }

        private void Raycasting()
        {
            if (_arRaycastManager.Raycast(_screenCenter, Hits, TrackableType.FeaturePoint))
            {
                depthLabel.enabled = true;
                _currentPoint.SetActive(true);
                addButton.interactable = addButton.transform.position.y > 0;
                var hitPose = Hits[0].pose;
                _currentPoint.transform.position = hitPose.position;
                depthLabel.text = $"Abstand: {(Math.Round(Hits[0].distance * 100))} cm";
            }
            else
            {
                addButton.interactable = false;
                _currentPoint.SetActive(false);
                depthLabel.text = "";
            }
        }

        public void Add()
        {
            if (!addButton.IsInteractable()) return;
            GameObject wayPoint;
            if (ButtonSwitcher.FeaturePointBased)
                wayPoint = Instantiate(measurementPointPrefab, _currentPoint.transform.position,
                    _currentPoint.transform.rotation);
            else
                wayPoint = Instantiate(measurementPointPrefab, penPoint.transform.position,
                    penPoint.transform.rotation);
            wayPoint.SetActive(true);
            _wayPoints.Add(wayPoint);
            waypointLabel.text = _wayPoints.Count.ToString();
            DrawPipe();
            Handheld.Vibrate();
        }

        public void RemoveLast()
        {
            if (_wayPoints.Count <= 0) return;
            Destroy(_wayPoints.Last());
            _wayPoints.Remove(_wayPoints.Last());
            waypointLabel.text = _wayPoints.Count.ToString();
            DrawPipe();
        }

        private void DrawPipe()
        {
            if (_wayPoints.Count > 1)
            {
                var waypoints = new List<Vector3>();
                waypoints.AddRange(_wayPoints.Select(i => i.transform.position));
                _create3DPipe.WayPoints = waypoints;
                _create3DPipe.DrawPipeFromWaypoints(pipe.GetComponent<Spline>());
                pipe.GetComponent<Spline>().RefreshCurves();
                pipe.GetComponent<SplineSmoother>().SmoothAll();
                pipe.SetActive(true);
                _distance = pipe.GetComponent<Spline>().Length;
            }
            else
            {
                pipe.SetActive(false);
            }

            UpdateTextField();
            UpdateButtons();
        }

        private void UpdateButtons()
        {
            removeLastButton.interactable = _wayPoints.Count > 0;
            finishButton.interactable = _wayPoints.Count > 1 && _distance > 0;
        }

        private void UpdateTextField()
        {
            var distanceRounded = (float) Math.Round(_distance * 1f) / 1f;
            distanceText.text = distanceRounded + "mm";
        }

        private void ResetScene()
        {
            _selectedPipe = GlobalDataController.Instance.pipe;
            _currentPoint = Instantiate(measurementPointPrefab);
            _arRaycastManager = GetComponent<ARRaycastManager>();
            _arPointCloudManager = GetComponent<ARPointCloudManager>();
            _distance = 0;
            addButton.interactable = false;
            measurementPointPrefab.transform.localScale = new Vector3(_selectedPipe.dia * 0.003f,
                _selectedPipe.dia * 0.003f, _selectedPipe.dia * 0.003f);
            pipe.GetComponent<SplineMeshTiling>().scale = new Vector3(_selectedPipe.dia * 0.0015f,
                _selectedPipe.dia * 0.0015f, _selectedPipe.dia * 0.0015f);
            _wayPoints.Clear();
        }

        public void Finish()
        {
            if (!(_distance > 0)) return;
            Destroy(_currentPoint);
            GlobalDataController.Instance.length = Convert.ToInt32(_distance);
            SceneLoader.LoadSceneAdaptive(SceneLoader.FindScene(NextScene));
        }

    }
}