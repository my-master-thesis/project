﻿using Loading;
using TMPro;
using UnityEngine;

namespace Scenes
{
    public class Error : MonoBehaviour
    {
        [SerializeField] private TMP_Text redirectText;
        
        private float _timeLeft = 3.0f;
        
        private void Update()
        {
            _timeLeft -= Time.deltaTime;
            if (_timeLeft > 2)
            {
                redirectText.text = "Sie werden automatisch weitergeleitet.";
            } else if (_timeLeft > 1)
            {
                redirectText.text = "Sie werden automatisch weitergeleitet..";
            }
            else if(_timeLeft > 0)
            {
                redirectText.text = "Sie werden automatisch weitergeleitet...";
            }
            else
            {
                SceneLoader.Load(SceneLoader.Scene.StartScreen);
            }
        }
    }

}
